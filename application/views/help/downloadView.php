<h2>Downloads</h2>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    Aqui você encontrará todas as informações sobre os downloads que você pode realizar no site.
</p>
<h4>Programa Desktop do Prodown</h4>
<div class ="row-fluid">
    <div class="span8">
        <p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
            No site <a href="<?=BASE_URL .'site/'?>" class='btn btn-link'> Prodown</a>, a aba de "Programa desktop" tem três opções de download, conforme a sua necessidade.
        </p>
        <p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
            Clique na versão do programa do sistema que deseja utilizar.
        </p> 
    </div>
    <div class="span4">
        <a href="#" class="thumbnail" style='margin-bottom: 40px'>
            <img src="<?=IMG.'ajuda/ajuda_download_desktop.png'?>">
        </a>
    </div>    
</div>
<h4>Artigos relacionadas aos resultados dos testes</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    No site <a href="<?=BASE_URL .'site/'?>" class='btn-link'>Prodown</a>, a aba de "Publicações" tem vários artigos relacionados aos testes realizados com os alunos especiais. Esses artigos poderão ser usados como referência para outros trabalhos academicos ou como consulta para quem quer se aprofundar mais sobre o assunto.
</p>
<a href="#" class="thumbnail" style='margin-bottom: 20px'>
    <img src="<?=IMG.'ajuda/ajuda_download_publicacoes.png'?>">
</a>
<div class="alert alert-info" style='margin-bottom: 40px'>
    <strong><i class="icon-info-sign"></i>Info</strong> Ao lado de cada uma das publicações, você fará o download.
</div>
<h4>Ficha de avalição do Prodown</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    A ficha de avaliação serve como de coletar as informações, antes de inseri-las no programa desktop.
</p>
<p style='text-indent: 20px;margin-bottom: 40px'>
    A ficha está disponivel para baixar<a href="<?=BASE_URL. 'doc/Ficha-Avaliacao-PRODOWN.doc'?>" class="btn btn-link"> aqui.</a>
</p>
<h4>Turmas/aluno que o professor possue</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    No sistema web, o professor cadastrado poderá fazer download das suas turmas e de seus alunos.
</p>
<a href="#" class="thumbnail" style='margin-bottom: 20px'>
    <img src="<?=IMG.'ajuda/ajuda_download_recuperarTurma.png'?>">
</a>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    Segue os eguintes passos:
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    1) Selecione a instituição da turmas/alunos que deseja recuperar;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    2) Selecione a versão do documento que deseja gerar;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    3) Clique no botão "Gerar";
</p>
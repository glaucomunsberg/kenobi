<?php

if (!defined('BASEPATH'))

	exit('No direct script access allowed');


class Exportacao extends CI_Controller{

	function __construct() {
		parent::__construct();
		$this->load->model('sistema/loginmodel', 'loginModel');
		$this->load->model('exportacaomodel','exportacaoModel');
                $this->load->model('instituicaomodel', 'instituicaoModel');
                $this->load->model('turmasmodel', 'turmasModel');
		$this->load->model('turmaalunosmodel', 'turmaAlunosModel');
                $this->load->model('testesmodel', 'testesModel');
                $this->load->model('quintilmodel', 'quintilModel');
		$this->loginModel->logged();
	}

        public function calc_idade( $data_nasc ){

            $data_nasc = explode("/", $data_nasc);
            $data = date("d-m-Y");
            $data = explode("-", $data);
            $anos = $data[2] - $data_nasc[2];

            if ( $data_nasc[1] >= $data[1] ){
                if ( $data_nasc[0] <= $data[0] ){
                    return $anos;
                    break;
                }
                else{
                    return $anos-1;
                    break;
                } 
            }
            else{
                return $anos;
            } 
        } 
        
        public function calc_idade2($data_nasc, $data){

            $data_nasc = explode("-", $data_nasc);
            $data = explode("-", $data);
            $anos = $data[0] - $data_nasc[0];
            
            log_message('debug', implode(' ',$data_nasc));
            log_message('debug', implode(' ', $data));
            
            if ( $data_nasc[1] >= $data[1] ){
                if ( $data_nasc[2] <= $data[2] ){
                    return $anos;
                    break;
                }
                else{
                    return $anos-1;
                    break;
                } 
            }
            else{
                return $anos;
            } 
        } 
        
        public function exportarArquivo() {
            $formato = $_POST['formato'];
            $cnpj = $_POST['cnpj'];
            $cpf = $_POST['cpf'];
            
            log_message('debug', $formato);
            log_message('debug', $cnpj);
            log_message('debug', $cpf);
            
            if($formato == "CODY"){
                try{                
                    $data = $this->instituicaoModel->getInstituicao($cnpj);
                    $data['turmas'] = $this->turmasModel->getTurmaByProfessorInstituicao($cnpj, $cpf);
                    $i = 0;
                    foreach ($data['turmas'] as $turma) {
                        $data['turmas'][$i++]['alunos'] = $this->turmaAlunosModel->getAlunosByTurma($cnpj, $cpf, $turma['nome_turma'], $turma['ano']);
                    }
                    $url_file = $this->exportacaoModel->exportar($data, $formato);
                    print json_encode(array('status'=>'sucess','mensagem'=>$url_file));
                }catch(Exception $e){
                    print json_encode(array('status'=>'error','mensagem'=>"Algo de errado aconteceu no nosso servidor, tente novamente"));
                }
            }
            elseif($formato == "CVS"){
                try{
                    $data = $this->testesModel->getTestesEGeneroByEducadorInstituicao($cpf, $cnpj);
                    $i = 0;
                    foreach ($data as $teste){
                        $data[$i++]['idade'] = $this->calc_idade2($teste['dt_nascimento'], $teste['dt_ocorrencia']);
                        log_message('debug', implode(' ', $teste));
                    }
                    $url_file = $this->exportacaoModel->exportar($data, $formato);
                    print json_encode(array('status'=>'sucess', 'mensagem'=>$url_file));
                }catch(Exception $e){
                    print json_encode(array('status'=>'error','mensagem'=>"Algo de errado aconteceu no nosso servidor, tente novamente"));
                }
            }
            elseif($formato == "PDF"){
                try{
                    $turmas = $this->turmasModel->getTurmaByProfessorInstituicao($cnpj, $cpf);
                    $i = 0;
                    $data = array();
                    foreach ($turmas as $turma) {
                        $alunos = $this->turmaAlunosModel->getAlunosByTurma($cnpj,$cpf,$turma['nome_turma'],$turma['ano']);
                        foreach ($alunos as $aluno){
                            $idade = $this->calc_idade($aluno['dt_nascimento']);
                            $testes = $this->testesModel->getTesteByAluno($cnpj, $cpf, $turma['nome_turma'], $aluno['nome'], $aluno['nome_mae'], $aluno['dt_nascimento']);
                            foreach ($testes as $teste){
                                $abdominal = $this->quintilModel->getQuintilAbdominaisByGenero($aluno['genero'], $idade,$teste["num_abdominais"]);
                                $agilidade = $this->quintilModel->getQuintilAgilidadeByGenero($aluno['genero'], $idade, $teste["quadrado"]);
                                $forca_inferiro = $this->quintilModel->getQuintilForcaExplosivaInferiorByGenero($aluno['genero'], $idade, $teste["salto_em_distancia"]);
                                $forca_superior = $this->quintilModel->getQuintilForcaExplosivaSuperiorByGenero($aluno['genero'], $idade, $teste["arremesso_medicineball"]);
                                $velocidade = $this->quintilModel->getQuintilVelocidadeDeslocamentoByGenero($aluno['genero'], $idade, $teste["corrida_vinte_metros"]);
                                $flexibilidade = $this->quintilModel->getQuintilAgilidadeByGenero($aluno['genero'], $idade, $teste['sentar_alcancar']);
                                $data[$i++] = array('nome' => $aluno['nome'], 'dt_ocorrencia' => $teste['dt_ocorrencia'],'turma' => $turma['nome_turma'], 'abdominal' => $abdominal, 'agilidade' => $agilidade, 'flexibilidade' => $flexibilidade, 'forca_explosiva_inferir' => $forca_inferiro, 'forca_explosiva_superior' => $forca_superior, 'velocidade' => $velocidade);
                            }
                        }
                    }
                    
                    $url_file = $this->exportacaoModel->exportar($data, $formato);
                    print json_encode(array('status'=>'sucess', 'mensagem'=>$url_file));
                }catch(Exception $e){
                    $url_file = $this->exportacaoModel->exportar($data, $formato);
                    print json_encode(array('status'=>'error','mensagem'=>"Algo de errado aconteceu no nosso servidor, tente novamente"));
                }
            }
            else{
                print json_encode(array('status'=>'error','mensagem'=>"Algo de errado aconteceu no nosso servidor, tente novamente"));
            }
        }
}


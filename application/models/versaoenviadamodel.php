<?php

class VersaoEnviadaModel extends CI_Model {
    
    function getHashs(){
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('versoesArquivoEnviado');
        $this->db->trans_complete();
        
        return $this->db->get()->result();
    }
    
    function getHash($cpf,$hash){
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('versoesArquivoEnviado');
            $this->db->where('cpf', $cpf);
            $this->db->where('file_hash', $hash);
        $this->db->trans_complete();
        
        return $this->db->get()->result();
    }
    
    function inserirHash($cpf,$hash,$fileId,$versaoDoPrograma){
        $this->db->trans_start();
            $this->db->set('cpf', $cpf);
            $this->db->set('file_hash', $hash);
            $this->db->set('file_id', $fileId);
            $this->db->set('versao_programa', $versaoDoPrograma);
            $this->db->insert('versoesArquivoEnviado');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }
        else{
            return true;
        }
    }
    
    function existeHash($hash,$cpf){
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('versoesArquivoEnviado');
            $this->db->where('cpf', $cpf);
            $this->db->where('file_hash', $hash);
        $this->db->trans_complete();
        
        if($this->db->get()->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }
    
    function removerHash($cpf,$hash){
        $this->db->trans_start();
            $this->db->where('cpf', $cpf);
            $this->db->where('file_hash', $hash);
            $this->db->delete('versoesArquivoEnviado');
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $retorno = false;
        }else{
            $retorno = true;
        }
    }
}

?>

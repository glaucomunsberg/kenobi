<?=headerGlobalView();?>
<div id="total" class="container">
    <div id="menuGeral" data-spy="affix-top" data-offset-top="0"  class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse" >
                    <ul class="nav">
                        <li class=""><a href="<?=BASE_URL?>" ><i class="icon-home"></i>Início</a></li>
                        <li class=""><a href="<?=BASE_URL?>developers" >Developers</a></li>
                        <li class=""><a href="#windows" >Windows</a></li>
                        <li class=""><a href="#linux" >Linux</a></li>
                        <li class=""><a href="#ubuntu" >Linux</a></li>
                        <li class=""><a href="#macosx" >OS X</a></li>
                        <li class=""><a href="<?=BASE_URL.'ajuda'?>"  target="_blank">Central de Ajuda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 50px;text-align: center">
        <ul class="thumbnails">
            <li class="span12">
                <div class="thumbnail">
                    <img src="<?= IMG . 'cody/codybackground.png' ?>" alt="">
                </div>
            </li>
        </ul>
        <p>
            <h1><strong>Conheça o Cody</strong></h1>
        </p>
        <p>
            <h6>Programa Desktop de Formação </h6>
        </p>
    </div>
    <div class="row">
        <div class="span12">
            <p style="text-indent: 40px; text-align: justify; margin-top: 10px;margin-bottom: 25px">
                Cody é a nossa solução para a formatação de informações para o sistema de informações online Kenobi. Nosso sistema Cody foi desenvolvido na linguagem de programação <strong>Java</strong> e usa o formato <strong>XML</strong> para trocar informações entre ele o sistema web Kenobi.
                Atualmente o sistema está disponível para 3 sistemas operacionais e sendo compatibilizado para outros. Veja abaixo como funciona e o que é necessário para a instalação para cada uma das plataformas.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="page-header">
            <h1>Informações<small> técnicas do programa</small></h1>
        </div>
        <div class='span12'>
            <table class="table table-striped">
                <thead>
                </thead>
                <tbody>
                    <tr>
                        <td><b>Nome Técnico</b></td>
                        <td>Programa de Digitalização</td>
                    </tr>
                    <tr>
                        <td><b>Apelido</b></td>
                        <td>Cody</td>
                    </tr>
                    <tr>
                        <td><b>Codenome</b></td>
                        <td>Programa Desktop</td>
                    </tr>
                    <tr>
                        <td><b>Versão Atual</b></td>
                        <td>1.0c</td>
                    </tr>
                    <tr>
                        <td><b>Consume Memória</b></td>
                        <td> =>50Mb</td>
                    </tr>
                    <tr>
                        <td><b>Linguagem de Programação</b></td>
                        <td> Java 7</td>
                    </tr>
                    <tr>
                        <td><b>Licença</b></td>
                        <td>GPL 2.0 <small>GNU General Public License</small></td>
                    </tr>
                    <tr>
                        <td><b>Versionamento</b></td>
                        <td><a href="https://bitbucket.org/glaucomunsberg/cody/" class='btn btn-link'>Bitbucket</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="page-header">
            <h1>Versões<small> para Sistemas Operacionais</small></h1>
        </div>
    </div>
    <section id="windows"  style="margin-top: 50px;">
        <div class="row">
            <div class="span2">
                <img src="<?= IMG . 'soWindows.png' ?>" alt="">
            </div>
            <div class="span10">
                <h3>Windows</h3>
                <p>
                    É necessário ter o <strong>Windows XP SP3</strong> ou uma versão superior deste sistema operacional;
                </p>
                <p>
                    Esteja instalado previamente o <strong>Java 7 ou versão superior</strong>;
                </p>
                <h5>Instalação e Execução</h5>
                <p>
                    A instação no windowns não querer ajudestes ou passos além dos tradicionais
                </p>
                <p><a href="<?= BASE_URL . 'static/installers/setupCody1.0c.exe' ?>" class="btn btn-primary"><i class="icon-download"></i> Baixar</a></p>
            </div>
        </div>
    </section>
    <section id="linux" style="margin-top: 50px;">
        <div class="row">
            <div class="span2">
                <img src="<?= IMG . 'soLinux.png' ?>" alt="">
            </div>
            <div class="span10">
                <h3>Linux</h3>
                <p>
                    Qualquer distribuição <strong>Linux</strong> pode rodar esta versão, note que há a necessidade de no mínimo 64 MB de memória RAM livre para que o programa execute de forma satisfatória;
                </p>
                <p>
                    Esteja instalado previamente o <strong>Java 7</strong> ou <strong>OpenJDK</strong>;
                </p>
                <h5>Instalação e Execução</h5>
                <p>
                    Para a execução descompacte a pasta Cody e execute o bash Cody.sh através duplo click ou pelo terminal com o comando abaixo:
                </p>
                <div class="well">
                  $ ./Cody.sh
                </div>
                <p><a href="<?= BASE_URL . 'static/installers/cody1.0.tar.gz' ?>" class="btn btn-primary"><i class="icon-download"></i> Baixar</a></a></p>
            </div>
        </div>
    </section>
    <section id="ubuntu" style="margin-top: 50px;">
        <div class="row">
            <div class="span2">
                <img src="<?= IMG . 'soUbuntu.png' ?>" alt="">
            </div>
            <div class="span10">
                <h3>Ubuntu</h3>
                <p>
                    Ubuntu igual a <strong>10.04 ou superior</strong> e instalado previamente o <strong>Java 7</strong> ou <strong>OpenJDK</strong>;
                </p>
                <h5>Instalação e Execução</h5>
                <p>A instalação pode ser feita com duplo click sobre o .deb, caso haja o centrol de aplicativos Ubuntu, ou através do terminal pelo comando abaixo:</p>
                <div class="well">
                  $ sudo dpkg -i Cody1.0c.deb
                </div>
                <p class="alert alert-danger">
                    <strong> <i class="icon-warning-sign"></i> Beta</a></strong> esta é uma versão instável e não indicado para quem deseja trabalhar.  
                </p>
                <p><a href="<?=BASE_URL . 'static/installers/cody1.0c.deb' ?>" class="btn btn-primary"><i class="icon-download"></i> Baixar</a></a></p>
            </div>
        </div>
    </section>
    <section id="macosx" style="margin-top: 50px;">
        <div class="row">
            <div class="span2">
                <img src="<?= IMG . 'soOSX.png' ?>" alt="">
            </div>
            <div class="span10">
                <h3>MAC OS X</h3>
                <p>
                    É necessário ter o <strong>MAC OS X 10.6.8</strong> ou uma versão superior deste sistema operacional;
                </p>
                <p>
                    Esteja instalado previamente o <strong>Java 6 ou versão superior</strong>;
                </p>
                <h5>Instalação e Execução</h5>
                <p>
                    A instação no OS X não querer ajudestes ou passos além dos tradicionais
                </p>
                <p><a href="<?= BASE_URL . 'static/installers/prodown_1.0c.pkg' ?>" class="btn btn-primary"><i class="icon-download"></i> Baixar</a></p>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="page-header">
            <h1>Desenvolvimento<small> do programa</small></h1>
        </div>
        <div class='span12'>
            <p>
                O programa foi desenvolvido pela <a class='btn btn-link' href="<?=BASE_URL.'equipe'?>">equipe skywallker</a> entre 2013 e o ano de 2014. Atualmente conta-se com membros da equipe original como colaboradores.
            </p>
        </div>
    </div>
</div>
<script>
     $('#total').css('height',$(document).height()-208);
     $('#membros div').mouseenter(function() {
            $(this).find('div').show();
        });
        $('#membros div').mouseleave(function() {

            $(this).find('div').hide();
        });
</script>
<?=footerView(); ?>
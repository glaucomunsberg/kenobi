DROP TABLE IF EXISTS instituicoes;

CREATE TABLE instituicoes (
       cnpj VARCHAR(14) NOT NULL
     , nome VARCHAR(255) NOT NULL
     , instituicao_tipo CHAR(1) NOT NULL
     , endereco VARCHAR(255)
     , cidade_id INT(11)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (cnpj)
     , INDEX (cidade_id)
     , CONSTRAINT FK_instituicoes_1 FOREIGN KEY (cidade_id)
                 REFERENCES cidades (id)
);


DROP TABLE IF EXISTS usuarios;
DROP TABLE IF EXISTS testes;
DROP TABLE IF EXISTS versoesArquivoEnviado;
DROP TABLE IF EXISTS turmaAlunos;
DROP TABLE IF EXISTS turmas;
DROP TABLE IF EXISTS educadoresInstituicoes;
DROP TABLE IF EXISTS educadores;
DROP TABLE IF EXISTS cidades;
DROP TABLE IF EXISTS estados;
DROP TABLE IF EXISTS ci_sessions;
DROP TABLE IF EXISTS alunos;
DROP TABLE IF EXISTS instituicoes;
DROP TABLE IF EXISTS files_upload;
DROP TABLE IF EXISTS mensagens;
DROP TABLE IF EXISTS configuracao_perfil;
set character_set_database = latin1;
CREATE TABLE configuracao_perfil (
       nome_perfil VARCHAR(255) NOT NULL
     , sistema_online BOOLEAN DEFAULT 1
     , linguagem_padrao VARCHAR(10)
     , email VARCHAR(255)
     , senha VARCHAR(30)
     , email_administrador VARCHAR(255)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (nome_perfil)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;;

CREATE TABLE files_upload (
       id BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT
     , filename VARCHAR(255)
     , titulo VARCHAR(255)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;;

CREATE TABLE alunos (
       nome VARCHAR(255) NOT NULL DEFAULT 'NOT NULL'
     , nome_mae VARCHAR(255) NOT NULL
     , dt_nascimento DATE NOT NULL
     , genero CHAR(1)
     , nome_pai VARCHAR(255)
     , cpf VARCHAR(11)
     , rg VARCHAR(25)
     , endereco VARCHAR(300)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (nome, nome_mae, dt_nascimento)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE ci_sessions (
       session_id VARCHAR(40) NOT NULL
     , ip_address VARCHAR(45) NOT NULL
     , user_agent VARCHAR(120) NOT NULL
     , last_activity INT(10) UNSIGNED
     , user_data TEXT
     , PRIMARY KEY (session_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE estados (
       id INT(11) NOT NULL AUTO_INCREMENT
     , uf VARCHAR(2)
     , estado VARCHAR(150)
     , PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE cidades (
       id INT(11) NOT NULL AUTO_INCREMENT
     , estado_id INT(11)
     , uf VARCHAR(5) NOT NULL
     , cidade VARCHAR(150)
     , PRIMARY KEY (id)
     , INDEX (estado_id)
     , CONSTRAINT FK_cidades_1 FOREIGN KEY (estado_id)
                  REFERENCES estados (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE instituicoes (
       cnpj VARCHAR(14) NOT NULL
     , nome VARCHAR(255) NOT NULL
     , instituicao_tipo CHAR(1) NOT NULL
     , endereco VARCHAR(255)
     , cidade_id INT(11)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (cnpj)
     , INDEX (cidade_id)
     , CONSTRAINT FK_instituicoes_1 FOREIGN KEY (cidade_id)
                  REFERENCES cidades (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;;

CREATE TABLE educadores (
       cpf VARCHAR(11) NOT NULL
     , nome VARCHAR(300) DEFAULT 'NOT NULL'
     , sobrenome VARCHAR(255)
     , rg VARCHAR(25)
     , genero CHAR(1)
     , dt_nascimento DATE
     , endereco VARCHAR(300)
     , cidade_id INT(11)
     , email VARCHAR(255)
     , justificativa_pedido TEXT
     , usuario_ativo BOOLEAN DEFAULT FALSE
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (cpf)
     , INDEX (cidade_id)
     , CONSTRAINT FK_educadores_1 FOREIGN KEY (cidade_id)
                  REFERENCES cidades (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;;

CREATE TABLE educadoresInstituicoes (
       cnpj VARCHAR(14) NOT NULL
     , cpf VARCHAR(11) NOT NULL
     , relacao_ativa BOOLEAN DEFAULT FALSE
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (cnpj, cpf)
     , INDEX (cpf)
     , CONSTRAINT FK_educadores_instituicoes_1 FOREIGN KEY (cpf)
                  REFERENCES educadores (cpf)
     , INDEX (cnpj)
     , CONSTRAINT FK_educadores_instituicoes_2 FOREIGN KEY (cnpj)
                  REFERENCES instituicoes (cnpj)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE turmas (
       cnpj VARCHAR(14) NOT NULL
     , cpf VARCHAR(11) NOT NULL
     , nome_turma VARCHAR(255) NOT NULL
     , ano YEAR NOT NULL
     , turma_ativa BOOLEAN DEFAULT 1
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (cnpj, cpf, nome_turma, ano)
     , INDEX (cnpj, cpf)
     , CONSTRAINT FK_turmas_2 FOREIGN KEY (cnpj, cpf)
                  REFERENCES educadoresInstituicoes (cnpj, cpf)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE turmaAlunos (
       cnpj VARCHAR(14) NOT NULL
     , cpf VARCHAR(11) NOT NULL
     , nome_turma VARCHAR(255) NOT NULL
     , ano YEAR NOT NULL
     , nome VARCHAR(255) NOT NULL
     , nome_mae VARCHAR(255) NOT NULL
     , dt_nascimento DATE NOT NULL
     , esta_ativo BOOLEAN DEFAULT 1
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (cnpj, cpf, nome_turma, ano, nome, nome_mae, dt_nascimento)
     , INDEX (cnpj, cpf, nome_turma, ano)
     , CONSTRAINT FK_turmasAlunos_1 FOREIGN KEY (cnpj, cpf, nome_turma, ano)
                  REFERENCES turmas (cnpj, cpf, nome_turma, ano)
     , INDEX (nome, nome_mae, dt_nascimento)
     , CONSTRAINT FK_turmasAlunos_2 FOREIGN KEY (nome, nome_mae, dt_nascimento)
                  REFERENCES alunos (nome, nome_mae, dt_nascimento)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE testes (
       cnpj VARCHAR(14) NOT NULL
     , cpf VARCHAR(11) NOT NULL
     , nome_turma VARCHAR(255) NOT NULL
     , ano YEAR NOT NULL
     , nome VARCHAR(255) NOT NULL
     , nome_mae VARCHAR(255) NOT NULL
     , dt_nascimento DATE NOT NULL
     , dt_ocorrencia DATE NOT NULL
     , arremesso_medicineball FLOAT UNSIGNED
     , corrida_vinte_metros FLOAT UNSIGNED
     , salto_em_distancia FLOAT UNSIGNED
     , quadrado FLOAT UNSIGNED
     , minutos_esportivo FLOAT UNSIGNED
     , minutos_esportivos_nove BOOLEAN DEFAULT 0
     , num_abdominais INT UNSIGNED
     , imc INT UNSIGNED
     , sentar_alcancar FLOAT UNSIGNED
     , sentar_alcancar_com_banco BOOLEAN DEFAULT 0
     , minutos_saude FLOAT UNSIGNED
     , minutos_saude_nove BOOLEAN DEFAULT 0
     , massa_corporal FLOAT UNSIGNED
     , estatura FLOAT UNSIGNED
     , envergadura FLOAT UNSIGNED
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (cnpj, cpf, nome_turma, nome, nome_mae, dt_nascimento, dt_ocorrencia, ano)
     , INDEX (cnpj, cpf, nome_turma, ano, nome, nome_mae, dt_nascimento)
     , CONSTRAINT FK_testes_1 FOREIGN KEY (cnpj, cpf, nome_turma, ano, nome, nome_mae, dt_nascimento)
                  REFERENCES turmaAlunos (cnpj, cpf, nome_turma, ano, nome, nome_mae, dt_nascimento)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usuarios (
       cpf VARCHAR(11) NOT NULL
     , password VARCHAR(50)
     , tipo_usuario CHAR(1) DEFAULT 'E'
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (cpf)
     , INDEX (cpf)
     , CONSTRAINT FK_usuarios_1 FOREIGN KEY (cpf)
                  REFERENCES educadores (cpf)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;;

CREATE TABLE versoesArquivoEnviado (
	 file_hash VARCHAR(32) NOT NULL
     , cpf VARCHAR(11) NOT NULL
     , file_id BIGINT(20) UNSIGNED NOT NULL
     , versao_programa VARCHAR(20)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (file_hash, cpf)
     , INDEX (cpf)
     , CONSTRAINT FK_versoes_arquivo_enviado_1 FOREIGN KEY (cpf)
                  REFERENCES educadores (cpf)
     , CONSTRAINT FK_versoesArquivoEnviado_2 FOREIGN KEY (file_id)
                  REFERENCES files_upload (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;;

CREATE TABLE mensagens (
       origem_cpf VARCHAR(11) NOT NULL
     , destino_cpf VARCHAR(11) NOT NULL
     , mensagem VARCHAR(300) NOT NULL
     , origem_tipo VARCHAR(30)
     , destino_tipo VARCHAR(30)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (origem_cpf, destino_cpf, mensagem)
     , INDEX (origem_cpf)
     , CONSTRAINT FK_mensagens_1 FOREIGN KEY (origem_cpf)
                  REFERENCES educadores (cpf)
     , INDEX (destino_cpf)
     , CONSTRAINT FK_mensagens_2 FOREIGN KEY (destino_cpf)
                  REFERENCES educadores (cpf)
) ENGINE = MyISAM;

<?php

class CidadesModel extends CI_Model{
    
    function getCidades(){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('cidades');
        $this->db->trans_complete();
        
        return $this->db->get()->result();
        
    }
    
    function getCidade($uf , $cidade){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('cidades');
            $this->db->where('uf',$uf);
            $this->db->where('cidade',$cidade);
        $this->db->trans_complete();
        return $this->db->get()->result();
    }
    function getCidadesbyEstado($uf){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('cidades');
            $this->db->where('uf',$uf);
        $this->db->trans_complete();
        
        return $this->db->get()->result();
        
    }
    
    function getTodasCidades(){
        $array= array();
        $this->db->trans_start();
            $this->db->select('c.id, CONCAT(e.estado," - ", c.cidade) as descricao',false);
            $this->db->from('cidades as c');
            $this->db->join('estados as e','c.estado_id = e.id');
            $array = $this->db->get()->result_array();
        $this->db->trans_complete();
        return $array;
    }
    
}
?>

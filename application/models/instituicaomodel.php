<?php

class InstituicaoModel extends CI_Model{
    
    function getInstituicoes(){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('instituicoes');
        $this->db->trans_complete();
        
        return $this->db->get()->result();
        
    }
    
    function getInstituicao($cnpj){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('instituicoes');
            $this->db->where('cnpj',$cnpj);
        $this->db->trans_complete();
        return $this->db->get()->row_array();
    }
    
    function inserirInstituicao($parametros){
        $cnpj = str_replace(".", "", $parametros['cnpj']);
        $cnpj = str_replace("-", "", $cnpj);
        $cnpj = str_replace("/", "", $cnpj);
        $this->db->trans_start();
            $this->db->set('cnpj', $cnpj);
            if( strtolower($parametros['tipo']) == 'especial' || strtolower($parametros['tipo']) == 'e'){
                $this->db->set('instituicao_tipo', 'E');
            }else{
                $this->db->set('instituicao_tipo', 'N');
            }
            
            if($parametros['nomeInstituicao'] != ''){
                $this->db->set('nome', $parametros['nomeInstituicao']);
            }else{
                $this->db->set('nome', $parametros['nome']);
            }
            $this->db->set('endereco', $parametros['enderecoInstituicao']);
            $this->db->set('cidade_id', $parametros['cidadeInstituicaoId']);
            $this->db->insert('instituicoes');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            return true;
        }
    }
    
    function atualizarInstituicao($parametros){
        
        $this->db->trans_start();
            $this->db->set('instituicao_tipo', $parametros['tipo']);
            $this->db->set('nome', $parametros['nome']);
            $this->db->where('cnpj', $parametros['cnpj']);
            $this->db->update('instituicoes');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            return true;
        }
    }
    
    function existeInstituicao($cnpj){
        $cnpj = str_replace(".", "", $cnpj);
        $cnpj = str_replace("-", "", $cnpj);
        $cnpj = str_replace("/", "", $cnpj);
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('instituicoes');
            $this->db->where('cnpj',$cnpj);
        $this->db->trans_complete();
        
        if($this->db->get()->num_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }
    
    function getInstituicoesByEducador($cpf){
         $this->db->trans_start();
            $this->db->select('i.cnpj, i.nome,i.instituicao_tipo, i.dt_cadastro');
            $this->db->from('educadoresInstituicoes as ei');
            $this->db->join('instituicoes as i','i.cnpj = ei.cnpj');
            $this->db->where('ei.cpf',$cpf);
        $this->db->trans_complete();
        return $this->db->get()->result();
    }
    
    function getInstituicoesByEducadorArray($cpf){
         $this->db->trans_start();
            $this->db->select('i.cnpj, i.nome,i.instituicao_tipo, i.dt_cadastro');
            $this->db->from('educadoresInstituicoes as ei');
            $this->db->join('instituicoes as i','i.cnpj = ei.cnpj');
            $this->db->where('ei.cpf',$cpf);
        $this->db->trans_complete();
        return $this->db->get()->result_array();
    }
    
        
    function existeEducadorInstituicao($cnpj,$cpf){
        
        $cnpj = str_replace(".", "", $cnpj);
        $cnpj = str_replace("-", "", $cnpj);
        $cnpj = str_replace("/", "", $cnpj);
        
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace("-", "", $cpf);
        $cpf = str_replace("/", "", $cpf);
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('educadoresInstituicoes');
            $this->db->where('cnpj',$cnpj);
            $this->db->where('cpf',$cpf);
        $this->db->trans_complete();
        
        if($this->db->get()->num_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }
}
?>

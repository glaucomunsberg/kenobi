<h2>Como enviar sua avaliação</h2>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    Enviar suas avaliações é a melhor forma de obter metainformações da sua coleta e também ajuda a nossa pesquisa. Colabore e mantenha 
    sempre no sistema web suas informações!
</p>
<div class="alert alert-info" style='margin-bottom: 40px'>
    <strong><i class="icon-info-sign"></i>Info</strong> Para mais informações sobre como se cadastrar consulte
    <a class="btn btn-link" href="<?=BASE_URL .'site/help#cadSistemaView.php'?>">Como se cadastrar no sistema</a>.
</div>
<h4>Você já possui cadastro e não sabe como enviar?</h4>
<div class ="row-fluid">
    <div class="span8">
        <p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
            Para enviar os testes você deve fazer o login no sistema. 
            Para fazer o login você deve inserir seu usuário e senha.
        </p>
    </div>
    <div class="span4">
        <a href="#" class="thumbnail" style='margin-bottom: 40px'>
            <img src="<?=IMG.'ajuda/cadastrar.png'?>" alt="">
        </a>
     </div>
</div>
<h4>Enviando os testes</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    Para enviar você deve ir no menu "Enviar Avaliações". Após entrar nessa opção selecione o arquivo a ser envidado e clique em "Enviar". O arquivo 
    a ser enviado deve ter sido criado pelo programa desktop do projeto e ter a extensão ".cody".
    Não são aceitos outros tipos de arquivos para envio das avaliações!
</p>
<a href="#" class="thumbnail" style='margin-bottom: 20px'>
    <img src="<?=IMG.'ajuda/enviar_testes.png'?>" alt="">
</a> 
<p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    Pronto! Seu arquivo foi enviado!
</p>
<h4>Você perdeu seus arquivos de teste?</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Saiba que é possível recuperá-los! Basta você ir no menu "Recuperar Turmas".
</p>
<a href="#" class="thumbnail" style='margin-bottom: 20px'>
    <img src="<?=IMG.'ajuda/exportar.png'?>" alt="">
</a> 
<p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    Você escolhe a instituição que você pertence, a turma desejada e o formato
    do arquivo para exportar, clique em "Gerar" e um arquivo com sua turma será gerado
    para seu backup. Porém, os testes desses alunos não seram enviados por motivos de segurança
    do sistema!
</p>

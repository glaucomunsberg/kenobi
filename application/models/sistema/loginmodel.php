<?php
class LoginModel extends CI_Model {

    # Valida o usuário
    function validate() {
        $this->db->select('e.cpf, e.nome, e.email, u.tipo_usuario');
        $this->db->from('usuarios as u');
        $this->db->join('educadores as e','e.cpf = u.cpf');
        $pos = strrpos($this->input->post('cpf'), "@");
        if ($pos === false) { 
            $this->db->where('u.cpf', $this->input->post('cpf'));
        }else{
            $this->db->where('e.email', $this->input->post('cpf'));
        }
        $this->db->where('u.password', md5($this->input->post('password')));
        $this->db->where('e.usuario_ativo', 1);

        $resultado = $this->db->get(); 

        if ($resultado->num_rows == 1) { 
            return true; // RETORNA VERDADEIRO
        }else{
            return false;
        }
    }
    
    function atualizarSenha($cpf,$novaSenha){
        $this->db->trans_start();
            
            $this->db->set('password', md5($novaSenha));
            $this->db->where('cpf', $cpf);
            $this->db->update('usuarios');
            
        $this->db->trans_complete();    
        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            return true;
        }
    }
    
    
    function atualizarUsuario($parametros){
        $this->db->trans_start();
            $parts = explode('/', $parametros['u_dt_nascimento']);
            $date  = "$parts[2]-$parts[1]-$parts[0]";
            $this->db->set('dt_nascimento', $date);
            $this->db->set('nome', $parametros['u_nome']);
            $this->db->set('sobrenome', $parametros['u_sobrenome']);
            $this->db->set('email', $parametros['u_email']);
            $this->db->where('cpf', $this->session->userdata('cpf'));
            $this->db->update('educadores');
            
        $this->db->trans_complete();    
        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            if( $parametros['u_senha'] == ''){
                return true;
            }else{
                $this->db->trans_start();
                    $this->db->set('password', md5($parametros['u_senha']));
                    $this->db->where('cpf', $this->session->userdata('cpf'));
                    $this->db->update('usuarios');
                $this->db->trans_complete();
                if($this->db->trans_status() === FALSE){
                    return false;
                }else{
                    return true;
                }
            }
            
        }
    }
    
    function getTotalUsuarios(){
        $retorno =  $this->db->get('usuarios');
        return $retorno->num_rows();
    }
    
    function listaUsuarios($parametros,$get){
        $data = array();

        $data['TotalRecordCount'] = $this->getTotalUsuarios();

        $this->db->trans_start();
            $this->db->select('e.cpf, nome, usuario_ativo, e.dt_cadastro',false);
            $this->db->from('usuarios as u');
            $this->db->join('educadores as e','e.cpf = u.cpf');
            $this->db->order_by('usuario_ativo','ASC');
            if(!empty($parametros['usuario_nome'])){
                $this->db->like('upper(nome)', strtoupper($parametros['usuario_nome']) );
            }

            if( !empty($get['jtSorting'])){
                $pieces = explode(" ", @$get['jtSorting']);
                $this->db->order_by($pieces[0],$pieces[1]);
            }

            if( @$get['jtStartIndex'] != ''  && @$get['jtPageSize'] != '' ){
                $this->db->limit($get['jtStartIndex']+','+$get['jtPageSize']);
            }

            $data['Records'] = $this->db->get()->result();
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }

        return $data;
    }
    
    function atualizarAtividoUsuario($parametros){
        $this->db->trans_start();
            $this->db->set('usuario_ativo', $parametros['usuario_ativo']);
            $this->db->where('cpf', $parametros['cpf']);
            $this->db->update('educadores');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }
        return $data;
    }
    /**
     * Retorna informações do usuário logado!
     * @return type
     */
    function getUsuarioLogged(){
        $this->db->select('e.cpf, u.tipo_usuario,e.nome,e.sobrenome,e.email, e.dt_nascimento');
        $this->db->from('educadores as e');
        $this->db->join('usuarios as u','u.cpf = e.cpf');
        $this->db->where('e.cpf', $this->session->userdata('cpf'));
        $this->db->limit(1);
        return $this->db->get()->row();
    }
    
    function isUsuario($parametros){
        $this->db->select('*');
        $this->db->from('usuarios as u');
        $this->db->join('educadores as e','e.cpf = u.cpf');
        $this->db->where('e.cpf', $parametros['cpf']);
        $this->db->like('e.email',$parametros['email']);
        $resultado = $this->db->get(); 
        if ($resultado->num_rows == 1) {
            return true;
        }else{
            return false;
        }
    }
    
    
    /**
     * Método que verifica se o usuário está logado
     */
    function logged() {
        $logged = $this->session->userdata('logged');

        if (!isset($logged) || $logged != true) {
            ?>
            <!doctype html>
            <html lang="pt-BR" xmlns="http://www.w3.org/1999/xhtml">
              <head>
                <link rel="shortcut icon" href="<?=IMG?>/favicon.ico">
                <script src='<?=JS."jquery.js"?>'></script>
                <script src='<?=JS."bootstrap.min.js"?>'></script>  
                <link href= '<?=CSS?>bootstrap.min.css' rel="stylesheet">
                <meta charset="UTF-8">
                <meta name="description" content="<?=lang('cabecalhoTitle');?>">
                <title><?=lang('cabecalhoTitle');?></title>
              </head>
              <body>
                    <div class="container">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="hero-unit">
                                    <h1>Ooops! <small>permissão negada</small></h1>
                                    <h1> </h1>
                                    <p>Você não tem permissão ou não está logado para acessar essa página, por favor volte a login </p>
                                    <p>
                                        <a class="btn btn-large btn-primary btn pull-right" href="<?=BASE_URL.'login/'?>"><i class="icon-lock"></i>Área de Login</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </body>
            </html>
            <?php
            
            die();
        }
    }
    
    /**
     * verifica se é um administador que está conectado
     *  caso não esteja a página morre.
     */
    function adminLogged() {
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('cpf', $this->session->userdata('cpf'));
        $this->db->where("tipo_usuario = 'A'");

        $resultado = $this->db->get(); 

        if ($resultado->num_rows == 1) {
            log_message('INFO',$resultado->num_rows);
            return true; // RETORNA VERDADEIRO
        }else{
            ?>
            <!doctype html>
            <html lang="pt-BR" xmlns="http://www.w3.org/1999/xhtml">
              <head>
                <link rel="shortcut icon" href="<?=IMG?>/favicon.ico">
                <script src='<?=JS."jquery.js"?>'></script>
                <script src='<?=JS."bootstrap.min.js"?>'></script>  
                <link href= '<?=CSS?>bootstrap.min.css' rel="stylesheet">
                <meta charset="UTF-8">
                <meta name="description" content="<?=lang('cabecalhoTitle');?>">
                <title><?=lang('cabecalhoTitle');?></title>
              </head>
              <body>
                    <div class="container">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="hero-unit">
                                    <h1>Ooops! <small>permissão negada</small></h1>
                                    <h1> </h1>
                                    <p>Você não tem permissão de Administrador para acessar esse página, por favor volte a login </p>
                                    <p>
                                        <a class="btn btn-large btn-primary btn pull-right" href="<?=BASE_URL.'login/'?>"><i class="icon-lock"></i>Área de Login</a>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </body>
            </html>
            <?php
            
            die();
        }
    }
}
?>
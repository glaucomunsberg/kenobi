<?=headerGlobalView();?>
<div class="container">
    <div class="row-fluid">
        <div class="page-header">
            <h1><?=lang('proDown')?> <small><?=lang('kenobiSistema')?></small></h1>
        </div>
    </div>
    
    <div class="row-fluid">
        <div class="span3">
            <div class="well" style="padding: 8px 0;">
                <ul class="nav nav-list">
                   <li class="disabled"><a><i class="icon-book"></i> <?=lang('menuPrivadoVerResultado')?></a></li>
                  <li><a href="<?=BASE_URL.'sistema/areaRestrita#resultados'?>" ><i class="icon-chevron-left"></i>Voltar</a></li>
                </ul>
              </div>
        </div>
        <div id="tabs" class="span5">
            <h2><?=lang('menuPrivadoResultadoTurma')?><small><?=' '.@$turma['nome_turma'].' na instituição '.@$turma['instituicao_nome']?></small></h2>
        </div>
        <div class="span4">
            <p>
            Deseja ver resultados de outras turmas?</p>
            <p>
             <div class="btn-group">
          <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Turmas vinculadas<span class="caret"></span></button>
           <ul class="dropdown-menu">
              <?php
              if( empty($turmas)){
                  echo '<li><a href="#">Você não tem turmas cadastradas</a></li>';
              }else{
                foreach ($turmas as $turma){
                    echo '<li><a href="#" onClick="resultadosTurma(\''.$turma->cnpj.'\',\''.$turma->cpf.'\',\''.$turma->nome_turma.'\',\''.$turma->ano.'\')">'.$turma->nome_instituicao.' - '.$turma->nome_turma.' de '.$turma->ano.'</a></li>';
                }  
              }
              ?>
          </ul>
        </div>
        </div>
    </div>
    <?php
        
        $cont = 0;
        if(!empty($testes)){
            foreach ($testes as $teste){
                ?>
                    <div class="row-fluid">
                        <div class="span12">
                            <div>
                                <h3>Teste realizado dia <?php echo $teste['ocorrencia']?></h3>
                            </div>
                            <div id="teste_<?php echo $cont?>">
                            </div>
                        </div>
                    </div>
                <?php
                $cont++;
            }
        }else{
        ?>    
        
            <div class="row-fluid" >
                    <div class="span4">
                    </div>
                         <div class="span8">
                        <div>
                            <h3>Esta turma não possui testes</h3>
                        </div>
                         </div>
                </div>
    <?php
        }
    ?>
    <div class="row-fluid">
                    <div class="span12">
                       
                    </div>
                </div>
</div>
<form action="<?=BASE_URL?>resultados/resultadoPorTurma/" method="post" id="byTurma">
           <input type="hidden" name="cpf" value="soemthing" id="cpf" />
           <input type="hidden" name="cnpj" value="soemthing" id="cnpj" />
           <input type="hidden" name="nome_turma" value="soemthing" id="nome_turma" />
           <input type="hidden" name="turma_ano" value="soemthing" id="turma_ano" />
        </form>
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<script>
     function resultadosTurma($cnpj,$cpf,$turmaNome,$turmaAno){
        $('#cnpj').val($cnpj);
        $('#cpf').val($cpf);
        $('#nome_turma').val($turmaNome);
        $('#turma_ano').val($turmaAno);
        document.getElementById('byTurma').submit();
    }
    
     google.load('visualization', '1', {packages:['table']});
     google.setOnLoadCallback(drawTable);
      function drawTable() {
          
        <?php
            $cont = 0;
            foreach($testes as $teste){
                ?>
                        var data_<?php echo $cont?> = new google.visualization.DataTable();
                        data_<?php echo $cont?>.addColumn('string', 'Nome');
                        data_<?php echo $cont?>.addColumn('string', 'Gênero');
                        data_<?php echo $cont?>.addColumn('string', 'IMC');
                        data_<?php echo $cont?>.addColumn('string', 'Abdominal');
                        data_<?php echo $cont?>.addColumn('string', 'Corrida 20m');
                        data_<?php echo $cont?>.addColumn('string', 'Medicineball');
                        data_<?php echo $cont?>.addColumn('string', 'Salto Horizontal');
                        data_<?php echo $cont?>.addColumn('string', 'Quadrado');
                       
                        
                        data_<?php echo $cont?>.addRows([
                            
                            <?php
                                $linhasTeste = $teste['testes'];
                                if(!empty($linhasTeste)){
                                foreach ($linhasTeste as $linhaTeste){
                                    echo "['".$linhaTeste['nome']."','".$linhaTeste['genero']."','".$linhaTeste['imc'].
                                            "','".$linhaTeste['quitilabdminal']. "','".$linhaTeste['corrida_vinte_metros'].
                                            "','".$linhaTeste['medicinebal']."','".$linhaTeste['salto_horizontal']."','".$linhaTeste['quadrado']."'],";
                                }
                                }else{
                                    echo"['a','a','a','a','a','a','a','a'],";
                                }
                            ?>
                        ]);

                        var table_<?php echo $cont?> = new google.visualization.Table(document.getElementById('teste_<? echo $cont?>'));
                        table_<?php echo $cont?>.draw(data_<?php echo $cont?>, {showRowNumber: true});
                <?php
                $cont++;
            }
            
        
        ?>
        
      }
</script>

<?=footerView(); ?>

<?php

$lang['kenobiVersao'] = 'Kenobi 1.0';
$lang['kenobiSistema'] = 'Sistema de Gestão de Informações';
$lang['kenobiSobreProjeto'] = 'Sobre o Projeto';

$lang['footerOSitema'] = 'O sistema ';
$lang['footerCriado'] = ' foi criado pela equipe  ';

$lang['enviarArquivoDef'] = ' de testes. Você fez novos testes? Que tal fazer o upload?';
$lang['enviarArquivoExplanacao'] = 'A qualquer momento você pode enviar os testes que você tem feito com suas turmas. Com isso você poderá ter acesso aos resultados através de Gráficos e até mesmo exportar para os formatos PDF e CVS. Tudo o você precisa é exportar o arquivo  com terminação ".cody" do seu aplicativo desktop. ';
$lang['enviarArquivoComoFazer'] = 'Agora que você tem o arquivo em mãos, selecione ele no seu computador e clique no botão "submit" ';

$lang['menuPrivadoDownloadDef'] = ' do programa para formatar seus dados';

$lang['uploadFilesSucessUpload'] = 'Arquivo subiu com sucesso';
$lang['uploadFilesSomethingWrong'] = 'Alguma coisa está errado quando você tenta subir o arquivo, por favor tente novamente';
$lang['uploadFilesError'] = 'Erro';

$lang['menuPrivadoDownloadDef'] = ' do programa desktop';
$lang['menuPrivadoGraficosDef'] = ' veja alguns dados que conseguimos calcular para você :)';

$lang['exportarArquivoDef']= ' de turmas e alunos';
$lang['exportarArquivoExplanacao']= 'Perdeu ou não acha seu arquivo da instituição? Aqui a qualquer momento você pode recuperar as informações de cada instituição, turmas e as informações completas de cada alunos. Com isso você poderá abrir o no seu programa desktop para fazer novos testes e criar outras tantas turmas e alunos.';
$lang['exportarArquivoComoFazer']= 'Escolha uma instiuição e o formato. Lembre-se: O arquivo apenas será gerado com as turmas a qual você ainda se encontra vinculado e alunos vinculadas a essas turmas';
$lang[''] = '';

/**
 * jTable Mensagens 
 */
$lang['jTableserverCommunicationError'] = 'Houve um erro ao se comunicar com o servidor.';
$lang['jTableloadingMessage'] = 'Carregando...';
$lang['jTablenoDataAvailable'] = 'Não há entradas!';
$lang['jTableareYouSure'] = 'Você tem certeza?';
$lang['jTableaddNewRecord'] = '+ Adicionar';
$lang['jTableeditRecord'] = 'Editar';
$lang['jTabledeleteConfirmation'] = 'Esse registro será deletado.Você tem certeza sobre isso?';
$lang['jTablesave'] = 'Salvar';
$lang['jTablesaving'] = 'Salvando';
$lang['jTablecancel'] = 'Cancelar';
$lang['jTabledeleteText'] = 'Deletar';
$lang['jTabledeleting'] = 'Deletando';
$lang['jTableerror'] = 'Erro';
$lang['jTableclose'] = 'Fechar';
$lang['jTablecannotLoadOptionsFor'] = 'Não é possível carregar as opções para o campo {0}';
$lang['jTablepagingInfo'] = 'Mostrando {0} a {1} de {2} registros';
$lang['jTablecanNotDeletedRecords'] = 'Não pode ser deletado {0} de {1} registros!';
$lang['jTabledeleteProggress'] = 'Deletar {0} de {1} registros, processando...';
?>

<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Configuracao extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('sistema/configuracaomodel', 'configuracaoModel');
        $this->load->model('sistema/loginmodel', 'loginModel');
        $this->loginModel->logged();
    }
    
    
    public function listaPerfil(){
        print json_encode($this->configuracaoModel->listaPerfeis($_POST,$_GET));
    }
    
    public function atualizarPerfil(){
         print json_encode($this->configuracaoModel->atualizarPrefil($_POST));
    }
}
?>
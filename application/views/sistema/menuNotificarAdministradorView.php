<h2>Mensageiro<small> do sistema.</small></h2>
<p>
    O mensageiro é a forma mais fácil de você entrar em contato com o administrador do sistema. Aproveite e tire suas dúvidas com ele por este canal
</p>
<div class="row">
    <div class="span12"  style="margin-left: 40px">
        <?=jTableStart('mensageiro','Mensagens','mensageiro/listaMensagens','mensageiro/criarMensagem','','',array('selecting','multiselect','selectingCheckboxes'))?>
            <?=jPanelAddID(true,false,true)?>
            <?=jPanelAddCampo('origem_tipo', 'Origem', '', '12%',true,false,false)?>
            <?=jPanelAddCampo('destino_tipo', 'Destino', '', '12%',true,false,false)?>
            <?=jPanelAddCampo('mensagem','Mensagem','textarea','46%',true)?>
            <?=jPanelAddData('dt_cadastro', 'Dt. Cadastro', '15%', 'dd-mm-yy',true,false,false)?>
        <?=jTableEnd()?>
    </div>
</div>
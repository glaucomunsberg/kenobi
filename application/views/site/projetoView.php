<div class="page-header">
    <h1>ProDown<small> avaliação da aptidão física de crianças e jovens com Down</small></h1>
</div>
<p style="text-align: justify; text-indent: 20px">
   Pró-Down é uma associação sem fins lucrativos para atendimentos especializados de crianças com necessidades especiais, tendo um Centro de Equoterapia e profissionais da área de Psicologia, Fisioterapia, Educação Especial e Pedagogia.
</p>
<p style="text-align: justify; text-indent: 20px">
   Este programa sugere tabelas normativas no que se refere ao desenvolvimento de crianças e jovens brasileiros com Síndrome de Down (SD).
   A atuação do PRODOWN configura-se em delinear o perfil das crianças e jovens brasileiras com SD, no crescimento e desenvolvimento somatomotor e 
   aptidão física relacionada à saúde e ao desempenho motor.
</p>
<p style="text-align: justify; text-indent: 20px">
   Permite ainda, organizar um banco de dados da população brasileira com SD, que ofereça a possibilidade 
   de desenvolver estudos epidemiológicos, referentes ao estilo de vida, das relações entre a atividade física, exercício físico e doenças associadas, 
   bem como o perfil da aptidão física.
</p>
<div class="alert alert-success">
    <p>
        Seja um amigo da Prodown, doe valores de R$ 10,00 a R$ 200,00. No banco <strong>Banco Sicredi</strong> na agência <strong>0512/06</strong> e conta nº <strong>27737-1</strong>
    </p>
</div>
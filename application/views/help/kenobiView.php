<?=headerGlobalView();?>
<div id="total" class="container">
    <div id="menuGeral" data-spy="affix-top" data-offset-top="0"  class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse" >
                    <ul class="nav">
                        <li class=""><a href="<?=BASE_URL?>" ><i class="icon-home"></i>Início</a></li>
                        <li class=""><a href="<?=BASE_URL?>developers" >Developers</a></li>
                        <li class=""><a href="<?=BASE_URL.'ajuda'?>"  target="_blank">Central de Ajuda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 50px;text-align: center">
        <ul class="thumbnails">
            <li class="span12">
                <div class="thumbnail">
                    <img src="<?= IMG . 'kenobi.png' ?>" alt="">
                </div>
            </li>
        </ul>
        <p>
            <h1><strong>Conheça o Kenobi</strong></h1>
        </p>
        <p>
            <h6>Sistema de Gestão de Informações </h6>
        </p>
    </div>
    <div class="row">
        <div class="span12">
            <p style="text-indent: 40px; text-align: justify; margin-top: 10px;margin-bottom: 25px">
                Kenobi é o Sistema de Gestão de Informações e coração do projeto Prodown. Ele foi escrito em <strong>PHP</strong> e <strong>MySQL</strong> 
                entre outras liguagens de programação pelo framework Codeigniter para antender as necessidades da Comunidade Prodown. Ele serve como ambiente de armazenamento, gerencimamento,
                e extração de informações.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="page-header">
            <h1>Informações<small> técnicas do Sistema</small></h1>
        </div>
        <div class='span12'>
            <table class="table table-striped">
                <thead>
                </thead>
                <tbody>
                    <tr>
                        <td><b>Nome Técnico</b></td>
                        <td>Sistema de Gestão de Informações</td>
                    </tr>
                    <tr>
                        <td><b>Apelido</b></td>
                        <td>Kenobi</td>
                    </tr>
                    <tr>
                        <td><b>Codenome</b></td>
                        <td>Sistema Prodown</td>
                    </tr>
                    <tr>
                        <td><b>Versão Atual</b></td>
                        <td>1.0</td>
                    </tr>
                    <tr>
                        <td><b>Linguagem de Programação</b></td>
                        <td> PHP, MySQL entre outras</td>
                    </tr>
                    <tr>
                        <td><b>Licença</b></td>
                        <td>GPL 2.0 <small>GNU General Public License</small></td>
                    </tr>
                    <tr>
                        <td><b>Versionamento</b></td>
                        <td><a href="https://bitbucket.org/glaucomunsberg/kenobi/" class='btn btn-link'>Bitbucket</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="page-header">
            <h1>Colaborar<small> com o desenvolvimento</small></h1>
        </div>
    </div>
    <div class="row">
        <div class="span2">
            <img src="<?= IMG . 'codigo.png' ?>" alt="">
        </div>
        <div class="span10">
            <h3>PHP/MySQL</h3>
            <p>
                É necessário que o servidor web esteja configurado com <strong>php5</strong>, <strong>mysql5</strong> e o <strong>modo rewrite</strong>(mod_rewrite) habilitado;
            </p>
            <p>
                Certifique-se que o banco de dados está correto.
            </p>
            <p><a href="https://bitbucket.org/glaucomunsberg/kenobi/" class="btn btn-primary"><i class="icon-download"></i> Acessar</a></p>
        </div>
    </div>
    <div class="row">
        <div class="page-header">
            <h1>Desenvolvimento<small> do programa</small></h1>
        </div>
        <div class='span12'>
            <p>
                O programa foi desenvolvido pela <a class='btn btn-link' href="<?=BASE_URL.'equipe'?>">equipe skywallker</a> entre 2013 e o ano de 2014. Atualmente conta-se com membros da equipe original como colaboradores.
            </p>
        </div>
    </div>
</div>
<script>
     $('#total').css('height',$(document).height()-208);
     $('#membros div').mouseenter(function() {
            $(this).find('div').show();
        });
        $('#membros div').mouseleave(function() {

            $(this).find('div').hide();
        });
</script>
<?=footerView(); ?>
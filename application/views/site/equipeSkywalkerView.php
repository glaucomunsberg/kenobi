<?=headerGlobalView();?>
<style type="text/css">
    .transparecia{
    filter:opacity(alpha=70); /* para o IE */
    -moz-opacity:0.70; /* para o FireFox */
    opacity:0.70; /* para o outros Navegadores */
    background-color: #fff;
}
.caixaFotoMembroEquipe{
    margin-top:5px;margin-left:1px ;height: 150px; width: 150px; cursor:pointer; background-size:contain ;background-repeat: no-repeat;margin-right:1px; float:left;
    -webkit-filter: grayscale(100%);
    filter: gray; /* IE6-9 */
}

.caixaDescricaoMembroEquipe{
    vertical-align: middle !important;display: none; padding-top: 40px; padding-bottom: 5px;text-align: center;height: 110px;width: 150px;cursor:pointer;color: black
}
</style>
<div id="total" class="container">
    <div id="menuGeral" data-spy="affix-top" data-offset-top="0"  class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse" >
                    <ul class="nav">
                        <li class=""><a href="<?=BASE_URL?>" ><i class="icon-home"></i>Início</a></li>
                        <li class="disabled"><a href="#" >Equipe Skywalker</a></li>
                        <li class=""><a href="<?=BASE_URL.'ajuda'?>"  target="_blank">Central de Ajuda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 50px">
        <h1>Equipe Skywalker</h1>
        <p style="text-indent: 40px; text-align: justify; margin-top: 10px;margin-bottom: 25px">
            A equipe Skywalker foi formada por alunos graduandos do curso de Bacharelado em Ciência da Computação da Universidade UFPel orientados pelos Professores Paulo Ferreira e Lisane Brisolar da mesma instituição. E com todo a atenção e respeito aos portadores da Sindrome de Down se dedicaram para tornar esse sistema viável e útil para comunidade Prodown. 
        </p>
    </div>
    <div>
        <div id="membros" class="span12">
            <div class="caixaFotoMembroEquipe" style="background-image: url('<?= IMG . '/profiles/aline.jpg' ?>');">
                <div class="transparecia caixaDescricaoMembroEquipe">
                    <b>Aline Tonini</b><br>Computação - UFPel
                </div>
            </div>
            <div class="caixaFotoMembroEquipe" style="background-image: url('<?= IMG . '/profiles/daniel.jpg' ?>');">
                <div class="transparecia caixaDescricaoMembroEquipe">
                    <b>Daniel Retzlaff</b><br>Computação - UFPel
                </div>
            </div>
            <div class="caixaFotoMembroEquipe" style="background-image: url('<?= IMG . '/profiles/eliane.jpg' ?>');">
                <div class="transparecia caixaDescricaoMembroEquipe">
                    <b>Eliane Siegert</b><br>Computação - UFPel
                </div>
            </div>
            <div class="caixaFotoMembroEquipe" style="background-image: url('<?= IMG . '/profiles/giovane.jpg' ?>');">
                <div class="transparecia caixaDescricaoMembroEquipe">
                    <b>Giovane Torres</b><br>Computação - UFPel
                </div>
            </div>
            <div class="caixaFotoMembroEquipe" style="background-image: url('<?= IMG . '/profiles/glauco.jpg' ?>');">
                <div class="transparecia caixaDescricaoMembroEquipe">
                    <b>Glauco Munsberg</b><br>Computação - UFPel<br><b>(Team Leader)</b>
                </div>
            </div>
            <div class="caixaFotoMembroEquipe" style="background-image: url('<?= IMG . '/profiles/guilherme.jpg' ?>');">
                <div class="transparecia caixaDescricaoMembroEquipe">
                    <b>Guilherme Cousin</b><br>Computação - UFPel
                </div>
            </div>
            <div class="caixaFotoMembroEquipe" style="background-image: url('<?= IMG . '/profiles/murilo.jpg' ?>');">
                <div class="transparecia caixaDescricaoMembroEquipe">
                    <b>Murilo Schmalfuss</b><br>Computação - UFPel
                </div>
            </div>
            <div class="caixaFotoMembroEquipe" style="background-image: url('<?= IMG . '/profiles/valesca.jpg' ?>');">
                <div class="transparecia caixaDescricaoMembroEquipe">
                    <b>Valesca Nunes</b><br>Computação - UFPel
                </div>
            </div>
        </div>
    </div>
</div>
<script>
     $('#total').css('height',$(document).height()-208);
     $('#membros div').mouseenter(function() {
            $(this).find('div').show();
        });
        $('#membros div').mouseleave(function() {

            $(this).find('div').hide();
        });
</script>
<?=footerView(); ?>

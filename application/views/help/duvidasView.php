<h3>Dúvidas Frequentes</h3>

<h4>Os testes podem ser aplicados em crianças sem Síndrome de Down?</h4>
<p style="margin-left: 20px; margin-bottom: 20px">
    Não, estes testes são específicos para crianças portadoras de Síndrome de Down.
</p>

<h4>Minha escola é de ensino regular e possui alunos com Síndorme de Down.
    Posso aplicar os testes nesses alunos?</h4>
<p style="margin-left: 20px; margin-bottom: 20px"> 
    Sim, você pode aplicar os testes do PRODOWN nesses alunos. O sistema permite o cadastramento
    de escolas de ensino regular que possuem alunos especiais.
</p>

<h4>Não possuo acesso a Internet na minha escola, como posso enviar os testes?</h4>
<p style="margin-left: 20px; margin-bottom: 20px"> 
    Você pode ter o programa de digitação em um computador sem acesso a Internet
    e salvar o arquivo dos testes e envia-lo de outro computador com acesso a rede.
</p>
<h4>Onde posso encontrar referências para embasamento científico?</h4>
<p style="margin-left: 20px; margin-bottom: 20px">
    No site do PRODOWN estão as referências bibliográficas do projeto.
</p>

<h4>Como faço para me cadastrar e colaborar com o projeto?</h4>
<p style="margin-left: 20px; margin-bottom: 20px"> 
    Inicialmente, entre em contato com o administrador do sistema ou entre no site do PRODOWN(www.prodown.ufrgs.br) para obter mais conhecimento do projeto. Após baixe o programa de digitação que está no link <a> "programa de digitação"</a>. 
    Neste mesmo link existe um arquivo chamado de guia de utilização que poderá facilitar o uso do programa de digitação. Agora você está apto a iniciar sua coleta de dados. 
    Ao término das digitações das duas avaliações, submeta o seu arquivo no site do PRODOWN, desta forma você poderá visualizar e imprimir os relatórios dos seus alunos.  
</p> 

<h4>É preciso realizar toda a bateria de testes para poder enviar?</h4>
<p style="margin-left: 20px; margin-bottom: 20px">
    Você poderá enviar os dados que coletou, mesmo sem ter aplicado toda a bateria de testes. O importante
    é que os dados obrigatórios (com *) estejam preenchidos, porém indica-se que todos dos testes
    sejam realizados para melhor obtenção de estatísticas.
</p>

<h4>Posso usar o programa de digitação sem enviar os dados dos testes?</h4>
<p style="margin-left: 20px; margin-bottom: 20px">
    A opção por usar o programa sem o envio dos dados é sua, contudo só receberás os relatórios de desempenho se submeteres suas avaliações no site.
</p>
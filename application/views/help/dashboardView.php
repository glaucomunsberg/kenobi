<?=headerGlobalView();?>
<div id="total" class="container">
    <div id="menuGeral" data-spy="affix-top" data-offset-top="0"  class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse" >
                    <ul class="nav">
                        <li class=""><a href="<?=BASE_URL?>" ><i class="icon-home"></i>Início</a></li>
                        <li class="disabled"><a href="#" >Central de Ajuda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-bottom: 25px;margin-top: 50px">
        <div class="span12">
            <h1>Central de Ajuda<small> do Sistema de Gestão Prodown</small></h1>
        </div>
    </div>
    <div class="row" style="margin-top: 10px">
        <div class="span12">
            <div class="tabbable tabs-left">
                <ul id="menuHelp"class="nav nav-tabs">
                    <li id="lihome" class="active"><a href="#home" data-toggle="tab">Início</a></li>
                    <li id="liprodonw" class=""><a href="#prodown" data-toggle="tab">Como aplicar a Avaliação</a></li>
                    <li id="lidesktop" class=""><a href="#desktop" data-toggle="tab">Como usar o Programa Desktop</a></li>
                    <li id="liweb" class=""><a href="#web" data-toggle="tab">Como usar o Sistema Web</a></li>
                    <li id="liavaliacao" class=""><a href="#avaliacao" data-toggle="tab">Como enviar a Avaliação</a></li>
                    <li id="lisistema" class=""><a href="#sistema" data-toggle="tab">Como se Cadastrar no Sistema</a></li>
                    <li id="liduvidas" class=""><a href="#duvidas" data-toggle="tab">Dúvidas Frequentes</a></li>
                    <li id="linormas" class=""><a href="#normas" data-toggle="tab">Normas de Uso</a></li>
                    <li id="lidownload" class=""><a href="#download" data-toggle="tab">Downloads</a></li>
                    <li id="livideos" class=""><a href="#videos" data-toggle="tab">Vídeos</a></li>
                </ul>
                <div id="menuHelpTab" class="tab-content">
                    <div class="tab-pane active" id="home">
                        <?=$this->load->view('help/homeView')?>
                    </div>
                    <div class="tab-pane" id="prodown">
                        <?=$this->load->view('help/aplicarProdownView')?>
                    </div>
                    <div class="tab-pane" id="desktop">
                        <?=$this->load->view('help/progDesktopView')?>
                    </div>
                    <div class="tab-pane" id="web">
                        <?=$this->load->view('help/comoUsarSistWebView')?>
                    </div>
                    <div class="tab-pane" id="avaliacao">
                        <?=$this->load->view('help/enviaAvaliacaoView')?>
                    </div>
                    <div class="tab-pane" id="sistema">
                        <?=$this->load->view('help/cadSistemaView')?>
                    </div>
                    <div class="tab-pane" id="duvidas">
                        <?=$this->load->view('help/duvidasView')?>
                    </div>
                    <div class="tab-pane" id="normas">
                        <?=$this->load->view('help/normasView')?>
                    </div>
                     <div class="tab-pane" id="videos">
                        <?=$this->load->view('help/videosView')?>
                    </div>
                    <div class="tab-pane" id="download">
                        <?=$this->load->view('help/downloadView')?>
                    </div>
                </div>
            </div>
        </div>       
    </div>
</div>
<script>
     $('#total').css('height',$(document).height()-180);
     if(window.location.hash) {
          var hash = window.location.hash.substring(1); //Retorna a primeira substring
          $('#menuHelpTab div').each(function(){
             $(this).removeClass('active');
             $('#'+hash).addClass('active');
          });
          $('#menuHelp li').each(function(){
             $(this).removeClass('active');
             $('#li'+hash).addClass('active');
          });
      }
      $('#menuHelp li').click(function(){
          window.location.hash = $(this).get(0).id.slice(2) ;
      })
</script>
<?=footerView(); ?>

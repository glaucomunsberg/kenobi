<?php

class EducadorModel extends CI_Model {
    
    function getEducadores(){
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('educadores');
        $this->db->trans_complete();
        
        return $this->db->get()->result();
    }
    
    function getTotalEducadores(){
        $retorno =  $this->db->get('educadoresInstituicoes');
        return $retorno->num_rows();
    }
    
    function getTodosEducadoresParaLista(){
        $array= array();
        $this->db->trans_start();
            $this->db->select('CONCAT(e.nome," ", e.sobrenome) as descricao, e.cpf as id',false);
            $this->db->from('educadores as e');
            $array = $this->db->get()->result_array();
        $this->db->trans_complete();
        return $array;
    }
    
    
    function listaEducadores($parametros,$get){
        $data = array();

        $data['TotalRecordCount'] = $this->getTotalEducadores();

        $this->db->trans_start();
            $this->db->select('e.nome as edu_nome, i.nome as inst_nome, ei.relacao_ativa, ei.dt_cadastro,ei.cpf,ei.cnpj',false);
            $this->db->from('educadoresInstituicoes as ei');
            $this->db->join('educadores as e','ei.cpf = e.cpf');
            $this->db->join('instituicoes as i','ei.cnpj = i.cnpj');
            $this->db->order_by('relacao_ativa','ASC');
            if(!empty($parametros['nome_educador'])){
                $this->db->like('upper(e.nome)', strtoupper($parametros['nome_educador']) );
            }
            if(!empty($parametros['nome_instituicao'])){
                $this->db->like('upper(i.nome)', strtoupper($parametros['nome_instituicao']) );
            }

            if( !empty($get['jtSorting'])){
                $pieces = explode(" ", @$get['jtSorting']);
                $this->db->order_by($pieces[0],$pieces[1]);
            }

            if( @$get['jtStartIndex'] != ''  && @$get['jtPageSize'] != '' ){
                $this->db->limit($get['jtStartIndex']+','+$get['jtPageSize']);
            }

            $data['Records'] = $this->db->get()->result();
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }

        return $data;
    }
    
    function getEducador($cpf){
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace("-", "", $cpf);
        $cpf = str_replace("/", "", $cpf);
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('educadores');
            $this->db->where('cpf', $cpf);
        $this->db->trans_complete();
        
        return $this->db->get()->result();
    }
    
    function getEducadorByEmail($email,$password){
        $this->db->trans_start();
            $this->db->select('e.cpf, e.nome');
            $this->db->from('educadores as e');
            $this->db->join('usuarios as u','u.cpf = e.cpf');
            $this->db->like('email', $email);
            $this->db->where('u.password', md5($password));
        $this->db->trans_complete();
        return $this->db->get()->row();
    }
    
    
    function inserirEducador($parametros){
        $cpf = str_replace(".", "", $parametros['cpf']);
        $cpf = str_replace("-", "", $cpf);
        $cpf = str_replace("/", "", $cpf);
        $this->db->trans_start();
            $this->db->set('cpf', $cpf);
            $this->db->set('nome', $parametros['nome']);
            $this->db->set('sobrenome', $parametros['sobrenome']);
            $this->db->set('rg', $parametros['rg']);
            if( strtolower($parametros['genero']) == 'feminino' || strtolower($parametros['genero']) == 'f'){
                $this->db->set('genero', 'F');
            }else{
                $this->db->set('genero', 'M');
            }
            
            $parts = explode('/', $parametros['dt_nascimento']);
            $date  = "$parts[2]-$parts[0]-$parts[1]";
            $this->db->set('dt_nascimento', $date);
            if( @$parametros['endereco'] != ''){
                $this->db->set('endereco', $parametros['endereco']);
            }else{
                $this->db->set('endereco', $parametros['enderecoEducador']);
            }
            if(@$parametros['cidade_id'] != ''){
                $this->db->set('cidade_id', $parametros['cidade_id']);
            }else{
                $this->db->set('cidade_id', $parametros['cidadeEducadorId']);
            }
            $this->db->set('email', $parametros['email']);
            $this->db->set('justificativa_pedido', $parametros['observacao']);
            $this->db->set('usuario_ativo', 0);
            $this->db->insert('educadores');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }
        else{
            return true;
        }
    }
    
    function inserirEducadorInstituicao($cpf,$cnpj,$relacao){
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace("-", "", $cpf);
        $cpf = str_replace("/", "", $cpf);
        $cnpj = str_replace(".", "", $cnpj);
        $cnpj = str_replace("-", "", $cnpj);
        $cnpj = str_replace("/", "", $cnpj);
        $this->db->trans_start();
            $this->db->set('cpf', $cpf);
            $this->db->set('cnpj', $cnpj);
            $this->db->set('relacao_ativa', $relacao);
            $this->db->insert('educadoresInstituicoes');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }
        else{
            return true;
        }
    }
    
    function atualizaEducador($parametros){
        $this->db->trans_start();
            $this->db->set('nome', $parametros['nome']);
            $this->db->set('sobrenome', $parametros['sobrenome']);
            $this->db->set('rg', $parametros['rg']);
            $this->db->set('genero', $parametros['genero']);
            $this->db->set('dt_nascimento', $parametros['dt_nascimento']);
            $this->db->set('endereco', $parametros['endereco']);
            $this->db->set('cidade_uf', $parametros['cidade_uf']);
            $this->db->set('email', $parametros['email']);
            $this->db->set('justificativa_pedido', $parametros['justificativa_pedido']);
            $this->db->set('usuario_ativo', $parametros['usuario_ativo']);
            $this->db->set('dt_cadastro', $parametros['dt_cadastro']);
            $this->db->where('cpf', $parametros['cpf']);
            $this->db->insert('educadores');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }
        else{
            return true;
        }
    }
    
    function existeEducador($cpf){
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace("-", "", $cpf);
        $cpf = str_replace("/", "", $cpf);
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('educadores');
            $this->db->where('cpf', $cpf);
        $this->db->trans_complete();
        
        if($this->db->get()->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }
    
    function existeEducadorByEmail($email){
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('educadores');
            $this->db->where('email', $email);
        $this->db->trans_complete();
        
        if($this->db->get()->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }
    
    
    function existeRelacaoEducadorInstituicao($cpf,$cnpj){
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace("-", "", $cpf);
        $cpf = str_replace("/", "", $cpf);
        
        $cnpj = str_replace(".", "", $cnpj);
        $cnpj = str_replace("-", "", $cnpj);
        $cnpj = str_replace("/", "", $cnpj);
        $this->db->trans_start();
            $this->db->where('cpf', $cpf);
            $this->db->where('cnpj', $cnpj);
            $this->db->from('educadoresInstituicoes');
        $this->db->trans_complete();
        
        if($this->db->get()->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }
    
    function atualizarRelacao($parametros){
        $this->db->trans_start();
            $this->db->where('cpf', $parametros['cpf']);
            $this->db->where('cnpj', $parametros['cnpj']);
            $this->db->set('relacao_ativa', $parametros['relacao_ativa']);
            $this->db->update('educadoresInstituicoes');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }
        return $data;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

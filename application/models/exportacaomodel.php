<?php

require(dirname(__FILE__).'/../../application/libraries/fpdf17/fpdftable.php');

class ExportacaoModel extends CI_Model{
	
	private $url;
	
	function exportar($param, $formato){
		switch($formato){
			case "CODY":
				$retorno = $this->exportarCody($param);
				break;
			case "CVS":
				$retorno = $this->exportarCsv($param);
				break;
			default:
				$retorno = $this->exportarPdf($param);
		}
		return $retorno;
	}
	
	function exportarCody($param){	
		$xml = new DOMDocument("1.0");
		
		/* Nome do arquivo exportado é o nome da instituição + a extensão '.cody'        */
		$this->url = __DIR__."/../../archives/".$param['nome'].".cody";
	
		/* Cria a tag instituição do XML                                                 */
		$instituicao = $xml->createElement("instituicao");
		$xml->appendChild($instituicao);
		
		/* Salva o nome da instituição e o seu CNPJ.                                     */
		$xml = $this->salvaXml($xml,$instituicao,"nome",$param['nome']);
		$xml = $this->salvaXml($xml,$instituicao,"cnpj",$param['cnpj']);
		
		/* No banco de dados, o tipo da instituição é salvo somente em um char (E ou R). *
		 * No Cody, isto é salvo como uma string (Ensino Especial e Ensino Regular).     *
		 * Esta parte do código transforma o caracter na string correspondente.          */
		if($param['instituicao_tipo'] == 'E'){
			$tipo = "Ensino Especial";
		}
		else{
			$tipo = "Ensino Regular";
		}
		
		/* Salva o tipo da instituição devidamente transformada.                         */
		$xml = $this->salvaXml($xml,$instituicao,"tipo",$tipo);
		
		/* Verifica se existe um array de turmas, e em caso positivo, faz a exportação   *
		 * de todas as turmas que estão contidas dentro do array.                        */
		if(is_array($param['turmas']))
		{
			foreach ($param['turmas'] as $turma){
				/* Cria as tags XML para cada turma, para exportar cada uma */
				$turma_xml = $xml->createElement("turma");
				$instituicao->appendChild($turma_xml);
				$xml = $this->exportarTurma($xml,$turma_xml,$turma);
			}
		}

		/* Salva o arquivo, e baseado neste arquivo é gerada uma hash, e então o         *
		 * arquivo é resalvo para ser possível inserir a hash no mesmo.                  */
		$xml->save($this->url);
		$xml = $this->salvaXml($xml,$instituicao,"hash",
		md5_file(dirname(__FILE__)."/../../archives/".$param['nome'].".cody"));
		$xml->save($this->url);
		
		/* Retorna o arquivo salvo para a controller                                     */
		return ARQUIVO.$param['nome'].".cody";
	}

	/* Método para exportar uma turma da instituição.                                    */	
	function exportarTurma($xml,$turma,$param){

		/* Salva o nome e o ano da turma.                                                */
		$xml = $this->salvaXml($xml,$turma,"nome",$param['nome_turma']);
		$xml = $this->salvaXml($xml,$turma,"ano",$param['ano']);
		
		/* Verifica se existe um array de alunos, e em caso positivo, faz a exportação   *
		 * de todos os alunosque estão contidas dentro do array.                         */		
		if(is_array($param['alunos']))
		{
			foreach ($param['alunos'] as $aluno){
				/* Cria as tags XML para cada aluno, para exportar cada um. */
				$aluno_xml = $xml->createElement("aluno");
				$turma->appendChild($aluno_xml);
				$xml = $this->exportarAluno($xml,$aluno_xml,$aluno);
			}		
		}

		return $xml;
	}
	
	/* Método para exportar um aluno da turma.                                           */
	function exportarAluno($xml,$aluno,$param){
		
		/* Salva o nome, a data de nascimento e o nome da mãe do aluno. Não é feito      *         
		 * tratamento algum, pois estes dados são obrigatórios.                          */
		$xml = $this->salvaXml($xml,$aluno,"nome",$param['nome']);	
		$xml = $this->salvaXml($xml,$aluno,"dataNascimento",$param['dt_nascimento']);		
		$xml = $this->salvaXml($xml,$aluno,"nomeMae",$param['nome_mae']);
		
		/* Salva o nome do pai do aluno, se o mesmo existe.                              */
		if($param['nome_pai'] != null){
			$xml = $this->salvaXml($xml,$aluno,"nomePai",$param['nome_pai']);
		}

		/* Salva o endereço do aluno, se o mesmo existe.                                 */		
		if($param['endereco'] != null){
			$xml = $this->salvaXml($xml,$aluno,"endereco",$param['endereco']);
		}		

		/* Salva o RG do aluno, se o mesmo existe.                                       */	
		if($param['rg'] != null){
			$xml = $this->salvaXml($xml,$aluno,"registroGeral",$param['rg']);
		}

		/* Salva o CPF do aluno, se o mesmo existe.                                      */		
		if($param['cpf'] != null){
			$xml = $this->salvaXml($xml,$aluno,"cadPessoaFisica",$param['cpf']);
		}
		
		/* No banco de dados, o gênero do aluno é salvo somente em um char (M, F).       *
		 * No Cody, isto é salvo como uma string (Masculino, Feminino).                  *
		 * Esta parte do código transforma o caracter na string correspondente.          */			
		if($param['genero'] == 'M'){
			$genero = "Masculino";
		}
		else{
			$genero = "Feminino";
		}
		
		/* Salva o gênero do aluno, devidamente transformado.                            */
		$xml = $this->salvaXml($xml,$aluno,"genero",$genero);
		return $xml;	
	}
	
	/* Método para salvar uma string para o arquivo XML.                                 *
	 * $xml = Arquivo XML onde será salvo;                                               *
	 * $raiz = Tag XML onde deve ser inserido a informação;                              *
	 * $nomeElemento = Nome da tag XML onde será salva a string;                         *
	 * $param = String que será inserida.                                                */
	function salvaXml($xml,$raiz,$nomeElemento,$param){
		$elemento = $xml->createElement($nomeElemento);
		$raiz->appendChild($elemento);
		$texto = $xml->createTextNode($param);
		$elemento->appendChild($texto);
		
		return $xml;			
	}

	function exportarCsv($param){
		
		/* Abre um arquivo na pasta padrão do projeto, com o nome testes.csv.            */	
		$fp = fopen(dirname(__FILE__)."/../../archives/testes.csv", 'w');
		
		if(is_array($param)){	

			/* Cabeçalho para cada item dos testes (que serão postos no csv)             */
			$header = array('Gênero', 'Idade', 'Arremesso de medicineball', 
			'Corrida de vinte metros', 'Salto em distância', 'Quadrado', 
			'6 minutos (esportivo)','9 minutos (esportivo)', 'Número de Abdominais', 
			'IMC', 'Sentar-e-alcançar (sem banco)', 'Sentar-e-alcançar (com banco)', 
			'6 minutos (saúde)', '9 minutos (saúde)', 'Massa corporal', 
			'Estatura', 'Envergadura');
			
			/* Insere o cabeçalho no arquivo csv                                         */
			fputcsv($fp, $header);
			foreach($param as $teste){
				
				/* Remove os atributos desnecessários para a exportação do csv.          */
				unset($teste['dt_nascimento']);
				
				/* Gambiarra p/ colocar a idade na 2ª coluna do csv, ao invés da última. */
				$teste['dt_ocorrencia'] = $teste['idade'];
				unset($teste['idade']);
				
				/* Transformação da letra do gênero.                                     *
				 * M -> Masculino, F -> Feminino                                         */
				if($teste['genero'] = 'M'){
					$teste['genero'] = "Masculino";
				}else{
					$teste['genero'] = "Feminino";
				}
				
				/* Com o campo "minutos_esportivos_nove" é um booleano pra saber se      *
				 * foram 9 minutos, põe o valor dos "minutos_esportivo" no lugar certo.  */
				if($teste['minutos_esportivos_nove'] == 0){
					$teste['minutos_esportivos_nove'] = '-';
				}
				else{
					$teste['minutos_esportivos_nove'] = $teste['minutos_esportivo'];
					$teste['minutos_esportivo'] = '-';
				}

				/* Com o campo "minutos_saude_nove" é um booleano pra saber se           *
				 * foram 9 minutos, põe o valor dos "minutos_saude" no lugar certo.      */				
				if($teste['minutos_saude_nove'] == 0){
					$teste['minutos_saude_nove'] = '-';
				}
				else{
					$teste['minutos_saude_nove'] = $teste['minutos_saude'];
					$teste['minutos_saude'] = '-';
				}

				/* Com o campo "sentar_alcancar_com_banco" é um booleano pra saber se    *
				 * teve banco no teste, põe o valor do teste de sentar-e-alcançar no     *
				 * índice certo do array.												 */				
				if($teste['sentar_alcancar_com_banco'] == 0){
					$teste['sentar_alcancar_com_banco'] = '-';
				}
				else{
					$teste['sentar_alcancar_com_banco'] = $teste['sentar_alcancar'];
					$teste['sentar_alcancar'] = '-';
				}
				
				/* Coloca no CVS os dados remanescentes da exclusão.                     */				
				fputcsv($fp, $teste);
			}
		}
		/* Fecha e salva o arquivo csv, retornando o próprio.                            */
		fclose($fp);
    	return ARQUIVO."testes.csv";
	}
	
	function exportarPdf($param){
		/* Abre um arquivo na pasta padrão do projeto, com o nome "resultados.pdf"      */
		$destino =  dirname(__FILE__)."/../../archives/resultados.pdf";
		
		/* Novo arquivo PDF.                                                            */
		$pdf = new tFPDF('P', 'mm', 'A4');
		
		/* Nova página em formato de retrato, e define tamanho das tabelas.				*/		
		$pdf->AddPage('L');
		$pdf->SetWidths(array(52, 40, 20, 23, 27, 27, 30, 30, 28));
		
		/* Cabeçalho do PDF, que só vai na primeira página, devidamente formatado.      */
		$pdf->SetFont('Times', 'b', 24);
		$pdf->Cell(40,10,'ProDown');
		$pdf->SetTextColor(128,128,128);
		$pdf->SetFont('Times', '', 16);
		$pdf->Cell(0, 10,utf8_decode('Sistema de Gestão de Informações'));
		
		/* Prepara a formatação das tabelas                                             */
		$pdf->SetTextColor(0,0,0);
		$pdf->ln();
		$pdf->ln();
		$pdf = $this->geraCabecalho($pdf);
		
		/* Coloca os resultados obtidos relacionados aos quintis.                       */
		if(is_array($param)){
			$i = 0;
			foreach($param as $teste){			
				$pdf->Row(array(utf8_decode($teste['nome']), 
				utf8_decode($teste['turma']), $teste['dt_ocorrencia'], 
				$teste['abdominal'], $teste['agilidade'], 
				$teste['flexibilidade'], $teste['forca_explosiva_inferir'], 
				$teste['forca_explosiva_superior'], $teste['velocidade']));
			}			
		}
		
		/* Quebra de linha com inserção da data e hora em que o PDF foi gerado         */						
		$pdf->ln();
		$pdf->Cell(0, 10,utf8_decode(date("d/m/Y - H:i:s")));
		
		/* Retorna o PDF pra controller, e ela que se vire.                            */
		$pdf->output($destino);
		return ARQUIVO."resultados.pdf";
	}
	
	function geraCabecalho($param){
		$param->SetFont('Times','b',11);
		$param->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
		$param->Row(array("Nome do Aluno", "Turma", "Data do Teste", "Abdominal", 
		"Agilidade", "Flexibilidade", utf8_decode("Força Explosiva Inferior"), 
		utf8_decode("Força Explosiva Superior"), "Velocidade"));
		$param->SetFont('Times','',10);
		$param->SetAligns(array('L', 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L'));		
		return $param;
	}		
}
?>

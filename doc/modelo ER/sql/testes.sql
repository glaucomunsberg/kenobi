DROP TABLE IF EXISTS DEFAULT_SCHEMA.testes;

CREATE TABLE DEFAULT_SCHEMA.testes (
       cnpj VARCHAR(14) NOT NULL
     , cpf VARCHAR(11) NOT NULL
     , nome_turma VARCHAR(255) NOT NULL
     , ano YEAR NOT NULL
     , nome VARCHAR(255) NOT NULL
     , nome_mae VARCHAR(255) NOT NULL
     , dt_nascimento DATE NOT NULL
     , dt_ocorrencia DATE NOT NULL
     , arremesso_medicineball FLOAT UNSIGNED
     , corrida_vinte_metros FLOAT UNSIGNED
     , salto_em_distancia FLOAT UNSIGNED
     , quadrado FLOAT UNSIGNED
     , minutos_esportivo FLOAT UNSIGNED
     , minutos_esportivos_nove BOOLEAN DEFAULT 0
     , num_abdominais INT UNSIGNED
     , imc INT UNSIGNED
     , sentar_alcancar FLOAT UNSIGNED
     , senta_alcancar_com_banco BOOLEAN DEFAULT 0
     , minutos_saude FLOAT UNSIGNED
     , minutos_saude_nove BOOLEAN DEFAULT 0
     , massa_corporal FLOAT UNSIGNED
     , estatura FLOAT UNSIGNED
     , envergadura FLOAT UNSIGNED
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (cnpj, cpf, nome_turma, nome, nome_mae, dt_nascimento, dt_ocorrencia, ano)
     , INDEX (cnpj, cpf, nome_turma, ano, nome, nome_mae, dt_nascimento)
     , CONSTRAINT FK_testes_1 FOREIGN KEY (cnpj, cpf, nome_turma, ano, nome, nome_mae, dt_nascimento)
                  REFERENCES DEFAULT_SCHEMA.turmaAlunos (cnpj, cpf, nome_turma, ano, nome, nome_mae, dt_nascimento)
);


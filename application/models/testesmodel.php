<?php

class TestesModel extends CI_Model{
    
    function getTestes(){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('testes');
        $this->db->trans_complete();
        
        return $this->db->get()->result_array();
        
    }
    
    function getTestesEGeneroByEducadorInstituicao($cpf, $cnpj){
        $this->db->trans_start();
            $this->db->select('a.genero, a.dt_nascimento, t.dt_ocorrencia, t.arremesso_medicineball, t.corrida_vinte_metros, t.salto_em_distancia, t.quadrado, t.minutos_esportivo, t.minutos_esportivos_nove, t.num_abdominais, t.imc, t.sentar_alcancar, t.sentar_alcancar_com_banco, t.minutos_saude, t.minutos_saude_nove, t.massa_corporal, t.estatura, t.envergadura');
            $this->db->from('testes as t');
            $this->db->join('alunos as a', 't.nome = a.nome and t.nome_mae = a.nome_mae and t.dt_nascimento = a.dt_nascimento');
            $this->db->where('t.cpf', $cpf);
            $this->db->where('t.cnpj', $cnpj);
        $this->db->trans_complete();
        
        return $this->db->get()->result_array();
    }
    
    function getTestesByEducadorInstituicao($cpf, $cnpj){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('testes');
            $this->db->where('cpf', $cpf);
            $this->db->where('cnpj', $cnpj);
        $this->db->trans_complete();
        
        return $this->db->get()->result_array();
        
    }
    
    function getTeste($cnpj,$cpf, $turma_nome, $nome, $nome_mae, $dt_nascimento, $dt_ocorrencia){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('testes');
            $this->db->where('cnpj',$cnpj);
            $this->db->where('cpf',$cpf);
            $this->db->where('turma_nome',$turma_nome);
            $this->db->where('nome',$nome);
            $this->db->where('nome_mae',$nome_mae);
            $this->db->where('dt_nascimento',$dt_nascimento);
            $this->db->where('dt_ocorrencia',$dt_ocorrencia);
        $this->db->trans_complete();
        return $this->db->get()->result();
    }
  
    function getTesteByAluno($cnpj,$cpf, $turma_nome, $nome, $nome_mae, $dt_nascimento){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('testes');
            $this->db->where('cnpj',$cnpj);
            $this->db->where('cpf',$cpf);
            $this->db->where('nome_turma',$turma_nome);
            $this->db->where('nome',$nome);
            $this->db->where('nome_mae',$nome_mae);
            $parts = explode('/', $dt_nascimento);
            $date  = "$parts[2]-$parts[1]-$parts[0]";
            $this->db->where('dt_nascimento',$date);
        $this->db->trans_complete();
        return $this->db->get()->result_array();
    }
    
            function getMediaTestesByAluno($cnpj,$cpf, $turma_nome, $nome, $nome_mae, $dt_nascimento){
        
        $this->db->trans_start();
            $this->db->select("( sum(num_abdominais)/count(num_abdominais) ) abdominais, ( sum(quadrado)/count(quadrado) ) as agilidade, ( sum(arremesso_medicineball)/count(arremesso_medicineball) ) as forca_explosiva_superior, ( sum(salto_em_distancia)/count(salto_em_distancia) ) as forca_explosiva_inferior, ( sum(corrida_vinte_metros)/count(corrida_vinte_metros) ) as velocidade");
            $this->db->from('testes');
            $this->db->where('cnpj',$cnpj);
            $this->db->where('cpf',$cpf);
            $this->db->where('nome_turma',$turma_nome);
            $this->db->where('nome',$nome);
            $this->db->where('nome_mae',$nome_mae);
            $parts = explode('/', $dt_nascimento);
            $date  = "$parts[2]-$parts[1]-$parts[0]";
            $this->db->where('dt_nascimento',$date);
        $this->db->trans_complete();
        return $this->db->get()->result_array();
    }
 
    function inserirTeste($cnpj,$cpf,$turma,$aluno,$parametros){
        
        $this->db->trans_start();
            $this->db->set('cnpj',$cnpj);
            $this->db->set('cpf',$cpf);
            $this->db->set('nome_turma',$turma['nome_turma']);
            $this->db->set('nome',$aluno['nome']);
            $this->db->set('nome_mae',$aluno['nome_mae']);
            $parts = explode('/', $aluno['dt_nascimento']);
            $date  = "$parts[2]-$parts[1]-$parts[0]";
            $this->db->set('dt_nascimento', $date);
            $parts2 = explode('/', $parametros['dt_ocorrencia']);
            $date2  = "$parts2[2]-$parts2[1]-$parts2[0]";
            $this->db->set('dt_ocorrencia',$date2);
            $this->db->set('ano', $turma['ano']);
            $this->db->set('arremesso_medicineball', $parametros['arremesso_medicineball']);
            $this->db->set('corrida_vinte_metros', $parametros['corrida_vinte_metros']);
            $this->db->set('salto_em_distancia', $parametros['salto_em_distancia']);
            $this->db->set('quadrado', $parametros['quadrado']);
            $this->db->set('minutos_esportivos_nove', $parametros['nove_minutos_esportivo']);
            $this->db->set('minutos_esportivo', $parametros['seis_minutos_esportivo']);
            $this->db->set('num_abdominais', $parametros['num_abdominais']);
            $this->db->set('imc', $parametros['imc']);
            $this->db->set('sentar_alcancar', '0');
            $this->db->set('sentar_alcancar_com_banco', '0');
            $this->db->set('minutos_saude_nove', $parametros['nove_minutos_saude']);
            $this->db->set('minutos_saude', $parametros['seis_minutos_saude']);
            $this->db->set('massa_corporal', $parametros['massa_corporal']);
            $this->db->set('estatura', $parametros['estatura']);
            $this->db->set('envergadura', $parametros['envergadura']);
            $this->db->insert('testes');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            return true;
        }
    }
    
    function atualizarTestes($parametros){
        
        $this->db->trans_start();
            $this->db->set('ano', $parametros['ano']);
            $this->db->set('arremesso_medicineball', $parametros['arremesso_medicineball']);
            $this->db->set('corrida_vinte_metros', $parametros['corrida_vinte_metros']);
            $this->db->set('salto_em_distancia', $parametros['salto_em_distancia']);
            $this->db->set('quadrado', $parametros['quadrado']);
            $this->db->set('nove_minutos_esportivo', $parametros['nove_minutos_esportivo']);
            $this->db->set('sei_minutos_esportivo', $parametros['sei_minutos_esportivo']);
            $this->db->set('num_abdominais', $parametros['num_abdominais']);
            $this->db->set('imc', $parametros['imc']);
            $this->db->set('sentar_alcancar_sem_banco', $parametros['sentar_alcancar_sem_banco']);
            $this->db->set('sentar_alcancar_com_banco', $parametros['sentar_alcancar_com_banco']);
            $this->db->set('nove_minutos_saude', $parametros['nove_minutos_saude']);
            $this->db->set('sei_minutos_saude', $parametros['sei_minutos_saude']);
            $this->db->set('massa_corporal', $parametros['massa_corporal']);
            $this->db->set('estatura', $parametros['estatura']);
            $this->db->set('envergadura', $parametros['envergadura']);
            $this->db->where('cnpj', $parametros['cnpj']);
            $this->db->where('cpf',$parametros['cpf']);
            $this->db->where('turma_nome',$parametros['turma_nome']);
            $this->db->where('nome',$parametros['nome']);
            $this->db->where('nome_mae',$parametros['nome_mae']);
            $this->db->where('dt_nascimento',$parametros['dt_nascimento']);
            $this->db->where('dt_ocorrencia',$parametros['dt_ocorrencia']);
            $this->db->update('testes');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            return true;
        }
    }
    
    function getTesteByTurma($cnpj,$cpf, $turma_nome, $turma_ano){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('testes');
            $this->db->where('cnpj',$cnpj);
            $this->db->where('cpf',$cpf);
            $this->db->where('turma_nome',$turma_nome);
            $this->db->where('ano',$turma_ano);
        $this->db->trans_complete();
        return $this->db->get()->result_array();
    }
    
    function getMediaTurmasByEducador($cpf){
        $this->db->trans_start();
            $this->db->select("t.nome_turma, t.ano as ano_turma, ( sum(corrida_vinte_metros)/count(corrida_vinte_metros) ) + ( sum(minutos_esportivo)/count(minutos_esportivo) ) + ( sum(salto_em_distancia)/count(salto_em_distancia) ) + ( sum(quadrado)/count(quadrado) ) / 5 as media_turma");
            $this->db->from('testes as t');
            $this->db->join('alunos as a','t.nome = a.nome and  t.nome_mae = t.nome_mae and t.dt_nascimento = t.dt_nascimento');
            $this->db->join('turmas as tu', 'tu.nome_turma = t.nome_turma and tu.ano = t.ano and t.cpf = tu.cpf and t.cnpj = tu.cnpj');
            $this->db->group_by('t.nome_turma, t.ano, t.cnpj, t.cpf');
            $this->db->order_by('media_turma','ASC');
            
            if($cpf != ''){
                $this->db->where('t.cpf',$cpf);
                $this->db->where('tu.turma_ativa',1);
            }
        $this->db->trans_complete();
        
        return $this->db->get()->result_array();
    }
    
    function getMelhorCorredorPorGenero($cpf,$genero){
         $this->db->trans_start();
            $this->db->select('t.nome as aluno_nome, sum(corrida_vinte_metros)/count(corrida_vinte_metros) as corrida_vinte');
            $this->db->from('testes as t');
            $this->db->join('alunos as a','t.nome = a.nome and  t.nome_mae = t.nome_mae and t.dt_nascimento = t.dt_nascimento');
            $this->db->join('turmas as tu', 'tu.nome_turma = t.nome_turma and tu.ano = t.ano and t.cpf = tu.cpf and t.cnpj = tu.cnpj');
            $this->db->where('a.genero',$genero);
            $this->db->group_by('t.nome, t.nome_mae, t.dt_nascimento');
            $this->db->order_by('corrida_vinte','ASC');
            
            if($cpf != ''){
                $this->db->where('t.cpf',$cpf);
                $this->db->where('tu.turma_ativa',1);
            }
            
            $this->db->limit(1);
        $this->db->trans_complete();
        
        return $this->db->get()->row_array();
    }
   
    function getMelhorAlunoDesempenhoGeral($cpf){
        $this->db->trans_start();
            $this->db->select('t.nome as aluno_nome, ( sum(corrida_vinte_metros)/count(corrida_vinte_metros) ) + ( sum(minutos_esportivo)/count(minutos_esportivo) ) + ( sum(salto_em_distancia)/count(salto_em_distancia) ) + ( sum(quadrado)/count(quadrado) ) / 5 as aluno_media');
            $this->db->from('testes as t');
            $this->db->join('alunos as a','t.nome = a.nome and  t.nome_mae = t.nome_mae and t.dt_nascimento = t.dt_nascimento');
            $this->db->join('turmas as tu', 'tu.nome_turma = t.nome_turma and tu.ano = t.ano and t.cpf = tu.cpf and t.cnpj = tu.cnpj');
            $this->db->group_by('t.nome, t.nome_mae, t.dt_nascimento');
            $this->db->order_by('aluno_media','ASC');
            
            if($cpf != ''){
                $this->db->where('t.cpf',$cpf);
                $this->db->where('tu.turma_ativa',1);
            }
                
            $this->db->limit(1);
        $this->db->trans_complete();
        
        return $this->db->get()->row_array();
    }
    
    function existeTeste($cnpj,$cpf, $turma_nome,$ano, $nome, $nome_mae, $dt_nascimento, $dt_ocorrencia){
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('testes');            
            $this->db->where('cnpj', $cnpj);
            $this->db->where('cpf',$cpf);
            $this->db->where('nome_turma',$turma_nome);  
            $this->db->where('ano',$ano);
            $this->db->where('nome',$nome);
            $this->db->where('nome_mae',$nome_mae);
            $this->db->where('dt_nascimento',$dt_nascimento);
            $this->db->where('dt_ocorrencia',$dt_ocorrencia);
        $this->db->trans_complete();
        
        if($this->db->get()->num_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }
    
    function getAvaliacoesByTurma($cnpj,$cpf,$turma_nome, $turma_ano){
        $this->db->trans_start();
            $this->db->select('t.cnpj, t.cpf, t.nome_turma, t.ano, t.dt_ocorrencia');
            $this->db->from('testes as t');
            $this->db->join('turmas as tu', 't.nome_turma = tu.nome_turma and t.ano = tu.ano and t.cpf = tu.cpf and t.cnpj = tu.cnpj');
            $this->db->where('tu.turma_ativa', 1);
            $this->db->where('tu.cnpj', $cnpj);
            $this->db->where('tu.cpf', $cpf);
            $this->db->where('tu.nome_turma', $turma_nome);
            $this->db->where('tu.ano', $turma_ano);
            $this->db->group_by('t.cnpj, t.cpf, t.nome_turma, t.ano, t.dt_ocorrencia');
        $this->db->trans_complete();

       return $this->db->get()->result_array();
    }
    function getTestesAlunosByAvaliacao($cnpj,$cpf,$turma_nome, $turma_ano,$dt_ocorrencia){
        $this->db->trans_start();
                $this->db->select('a.nome, a.genero, a.dt_nascimento, t.arremesso_medicineball, t.corrida_vinte_metros, t.salto_em_distancia, t.quadrado, t.minutos_esportivo, t.minutos_esportivos_nove, t.num_abdominais, t.imc, t.sentar_alcancar, t.sentar_alcancar_com_banco, t.minutos_saude, t.minutos_saude_nove, t.massa_corporal, t.estatura, t.envergadura');
                $this->db->from('testes as t');
                $this->db->join('turmas as tu', 't.nome_turma = tu.nome_turma and t.ano = tu.ano and t.cpf = tu.cpf and t.cnpj = tu.cnpj');
                $this->db->join('alunos as a', 't.nome = a.nome and t.nome_mae = a.nome_mae and t.dt_nascimento = a.dt_nascimento');
                $this->db->where('tu.turma_ativa', 1);
                $this->db->where('tu.cnpj', $cnpj);
                $this->db->where('tu.cpf', $cpf);
                $this->db->where('tu.nome_turma', $turma_nome);
                $this->db->where('tu.ano', $turma_ano);
                $this->db->where('t.dt_ocorrencia', $dt_ocorrencia);
                $this->db->group_by('t.cnpj, t.cpf, t.nome_turma, t.ano, t.dt_ocorrencia,t.nome');
            $this->db->trans_complete();
            return $this->db->get()->result_array();
    }
    function getTestesByIdade($cpf, $idade){
        
             $this->db->trans_start();
                $this->db->select('t.nome_turma, ( sum(corrida_vinte_metros)/count(corrida_vinte_metros) ) + ( sum(minutos_esportivo)/count(minutos_esportivo) ) + ( sum(salto_em_distancia)/count(salto_em_distancia) ) + ( sum(quadrado)/count(quadrado) ) / 5 as media_turma');
                $this->db->from('testes as t');
                $this->db->join('alunos as a', 't.nome = a.nome and t.nome_mae = a.nome_mae and t.dt_nascimento = a.dt_nascimento and t.cpf = '.$cpf);
                $this->db->join('turmas as tu', 't.nome_turma = tu.nome_turma and t.ano = tu.ano and t.cpf = tu.cpf and t.cnpj = tu.cnpj');
                $this->db->where('tu.turma_ativa', 1);
                $this->db->group_by('a.nome, a.nome_mae, a.dt_nascimento');
                $this->db->where("DATE_FORMAT(CURRENT_DATE(), '%y') = (STR_TO_DATE(alunos.dt_nascimento,'%y')+ ".$idade ); 
            $this->db->trans_complete();
             
           return $this->db->get()->result_array();
        
    }
    
     function getTestesByIdadeGenero($cpf, $idade, $genero){
        
             $this->db->trans_start();
                $this->db->select('t.nome_turma, ( sum(corrida_vinte_metros)/count(corrida_vinte_metros) ) + ( sum(minutos_esportivo)/count(minutos_esportivo) ) + ( sum(salto_em_distancia)/count(salto_em_distancia) ) + ( sum(quadrado)/count(quadrado) ) / 5 as media_turma');
                $this->db->from('testes as t');
                $this->db->join('alunos as a', 't.nome = a.nome and t.nome_mae = a.nome_mae and t.dt_nascimento = a.dt_nascimento and t.cpf = '.$cpf);
                $this->db->join('turmas as tu', 't.nome_turma = tu.nome_turma and t.ano = tu.ano and t.cpf = tu.cpf and t.cnpj = tu.cnpj');
                $this->db->where('tu.turma_ativa', 1);
                $this->db->group_by('a.nome, a.nome_mae, a.dt_nascimento');
                $this->db->where("DATE_FORMAT(CURRENT_DATE(), '%y') = (STR_TO_DATE(alunos.dt_nascimento,'%y')+ ".$idade ); 
                $this->db->where('a.genero',$genero);
            $this->db->trans_complete();
             
           return $this->db->get()->result_array();
    }
    function getTestesByAluno($cpf, $nome, $nome_mae, $dt_nascimento){
      $this->db->trans_start();
         $this->db->select('t.cpf, t.dt_ocorrencia, t.nome, t.nome_mae, t.dt_nascimento, t.arremesso_medicineball, t.corrida_vinte_metros, t.salto_em_distancia, t.quadrado, t.minutos_esportivo, t.minutos_esportivos_nove, t.num_abdominais, t.imc, t.sentar_alcancar, t.sentar_alcancar_com_banco, t.minutos_saude, t.minutos_saude_nove, t.massa_corporal, t.estatura, t.envergadura');
         $this->db->from('testes as t');
         $this->db->where('t.cpf', $cpf);
         $this->db->where('t.nome', $nome);
         $this->db->where('t.nome_mae', $nome_mae);
         $this->db->where('t.dt_nascimento', $dt_nascimento);
         $this->db->group_by('t.cpf, t.nome_turma, t.ano, t.dt_ocorrencia');
      $this->db->trans_complete();
      return $this->db->get()->result_array();
    }
    function getQuintilByAluno($cnpj,$cpf,$nome_turma,$ano,$nome,$nome_mae,$dt_nascimento){
        
        
    }
            
     function getResultadoAbdominal($idade, $genero, $nroAbdominais){
                 	
                if($genero == 'F'  ){ //testes para feminino
                    switch($idade){	
                    case(10):   //isso sera feito para cada idade
                        if($nroAbdominais <= 5  ){ 
                                return "Fraco";
                                break;
                        }   
                        if($nroAbdominais > 5 && $nroAbdominais <= 9  ){ 
                                return "Razoavel";
                                break;
                        }    
                       if($nroAbdominais > 9 && $nroAbdominais <= 13 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 13  && $nroAbdominais <= 15){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 16  ){ 
                                return "Otimo";
                                break;
                        } 
                    case(11):   //isso sera feito para cada idade
                        if($nroAbdominais <= 6  ){ 
                                return"Fraco";
                                break;
                        }   
                        if($nroAbdominais > 6 && $nroAbdominais <= 9  ){ 
                                return "Razoavel";
                                break;
                        }    
                       if($nroAbdominais > 9 && $nroAbdominais <= 13 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 13  && $nroAbdominais <= 15){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 16  ){ 
                                return "Otimo";
                                break;
                        } 
                    case(12):   //isso sera feito para cada idade
                        if($nroAbdominais <= 6  ){ 
                                return "Fraco";
                                exit;
                        }   
                        if($nroAbdominais > 7 && $nroAbdominais <= 9  ){ 
                                return"Razoavel";
                                break;
                        }    
                       if($nroAbdominais > 9 && $nroAbdominais <= 13 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 13  && $nroAbdominais <= 16){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 17  ){ 
                                return "Otimo";
                                exit;
                        } 
                    case(13):   //isso sera feito para cada idade
                        if($nroAbdominais <= 6  ){ 
                                return "Fraco";
                                break;
                        }   
                        if($nroAbdominais > 6 && $nroAbdominais <= 9  ){ 
                                return "Razoavel";
                                break;
                        }    
                       if($nroAbdominais > 10 && $nroAbdominais <= 13 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 13  && $nroAbdominais <= 16){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 17  ){ 
                                return "Otimo";
                                break;
                        } 
                    case(14):   //isso sera feito para cada idade
                        if($nroAbdominais <= 7  ){ 
                                return "Fraco";
                                break;
                        }   
                        if($nroAbdominais > 7 && $nroAbdominais <= 10  ){ 
                                return "Razoavel";
                                break;
                        }    
                       if($nroAbdominais >10 && $nroAbdominais <= 13 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 13  && $nroAbdominais <= 16){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 17  ){ 
                                return "Otimo";
                                break;
                        } 
                    case(15):   //isso sera feito para cada idade
                        if($nroAbdominais <= 8  ){ 
                                return "Fraco";
                                break;
                        }   
                        if($nroAbdominais > 8 && $nroAbdominais <= 11  ){ 
                                return "Razoavel";
                                break;
                        }    
                       if($nroAbdominais > 11 && $nroAbdominais <= 14 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 14  && $nroAbdominais <= 17){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 18  ){ 
                                return "Otimo";
                                break;
                        } 
                    case(16):   //isso sera feito para cada idade
                        if($nroAbdominais <= 9  ){ 
                                return "Fraco";
                                break;
                        }   
                        if($nroAbdominais > 9 && $nroAbdominais <= 12  ){ 
                                return "Razoavel";
                                break;
                        }    
                       if($nroAbdominais > 12 && $nroAbdominais <= 15 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 15  && $nroAbdominais <= 19){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 16  ){ 
                                return "Otimo";
                                break;
                        } 
                    case(17):   //isso sera feito para cada idade
                        if($nroAbdominais <= 10  ){ 
                                return "Fraco";
                                break;
                        }   
                        if($nroAbdominais > 11 && $nroAbdominais <= 12  ){ 
                                return "Razoavel";
                                break;
                        }    
                       if($nroAbdominais > 12 && $nroAbdominais <= 14 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 14  && $nroAbdominais <= 17){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 18  ){ 
                                return "Otimo";
                                break;
                        } 
                    case(18):   //isso sera feito para cada idade
                        if($nroAbdominais <= 9  ){ 
                                return "Fraco";
                                break;
                        }   
                        if($nroAbdominais > 9 && $nroAbdominais <= 12  ){ 
                                return "Razoavel";
                                break;
                        }    
                       if($nroAbdominais > 13 && $nroAbdominais <= 15 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 15  && $nroAbdominais <= 17){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 18  ){ 
                                return "Otimo";
                                break;
                        } 
                    case(19):   //isso sera feito para cada idade
                        if($nroAbdominais <= 9  ){ 
                                return "Fraco";
                                break;
                        }   
                        if($nroAbdominais > 10 && $nroAbdominais <= 12  ){ 
                                return "Razoavel";
                                break;
                        }    
                       if($nroAbdominais > 12 && $nroAbdominais <= 14 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 14  && $nroAbdominais <= 17){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 18 ){ 
                                return "Otimo";
                                break;
                        } 
                    case(20):   //isso sera feito para cada idade
                        if($nroAbdominais <= 9  ){ 
                                return "Fraco";
                                break;
                        }   
                        if($nroAbdominais > 9 && $nroAbdominais <= 11  ){ 
                                return "Razoavel";
                                break;
                        }    
                       if($nroAbdominais > 11 && $nroAbdominais <= 14 ){ 
                                return "Bom";
                                break;
                        }    
                       if($nroAbdominais > 14  && $nroAbdominais <= 16){ 
                                return "Muito Bom";
                                break;
                        }    
                       if($nroAbdominais >= 17  ){ 
                                return "Otimo";
                                break;
                        } 
                    }  
            }  
                           
    }
    
    function getTotalTestesByEducador($cpf){
        if($cpf != '')
            $this->db->where('cpf',$cpf);
        $retorno =  $this->db->get('testes');
        return $retorno->num_rows();
    }
    
    function listaTestesByEducador($parametros,$get){
        $data = array();
        function getTotalTestesByEducador($cpf){
            if($cpf != '')
                $this->db->where('cpf',$cpf);
            $retorno =  $this->db->get('testes');
            return $retorno->num_rows();
        }
        $data['TotalRecordCount'] = $this->getTotalTestesByEducador(@$parametros['cpf']);

        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('testes as t');
            $this->db->order_by('nome','ASC');            
            if(!empty($parametros['cpf'])){
                $this->db->where('cpf', strtoupper($parametros['cpf']) );
            }
            
            if(!empty($parametros['turma_nome'])){
                $this->db->like('upper(nome_turma)', strtoupper($parametros['turma_nome']) );
            }
            
            if(!empty($parametros['aluno_nome'])){
                $this->db->like('upper(nome)', strtoupper($parametros['aluno_nome']) );
            }
            
            if(!empty($parametros['cpf'])){
                $this->db->where('cpf', strtoupper($parametros['cpf']) );
            }

            if( !empty($get['jtSorting'])){
                $pieces = explode(" ", @$get['jtSorting']);
                $this->db->order_by($pieces[0],$pieces[1]);
            }

            if( @$get['jtStartIndex'] != ''  && @$get['jtPageSize'] != '' ){
                $this->db->limit($get['jtStartIndex']+','+$get['jtPageSize']);
            }

            $data['Records'] = $this->db->get()->result();
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }

        return $data;
    }
    
    function atualizarTesteByAdmin($parametros){
        $this->db->trans_start();
            
            $this->db->set('arremesso_medicineball', $parametros['arremesso_medicineball']);
            $this->db->set('corrida_vinte_metros', $parametros['corrida_vinte_metros']);
            $this->db->set('salto_em_distancia', $parametros['salto_em_distancia']);
            $this->db->set('quadrado', $parametros['quadrado']);
            $this->db->set('minutos_esportivo', $parametros['minutos_esportivo']);
            $this->db->set('minutos_esportivos_nove', $parametros['minutos_esportivos_nove']);
            $this->db->set('num_abdominais', $parametros['num_abdominais']);
            $this->db->set('imc', $parametros['imc']);
            $this->db->set('sentar_alcancar', $parametros['sentar_alcancar']);
            $this->db->set('sentar_alcancar_com_banco', $parametros['sentar_alcancar_com_banco']);
            $this->db->set('minutos_saude', $parametros['minutos_saude']);
            $this->db->set('minutos_saude_nove', $parametros['minutos_saude_nove']);
            $this->db->set('massa_corporal', $parametros['massa_corporal']);
            $this->db->set('envergadura', $parametros['envergadura']);
            
            $this->db->where('cpf', $parametros['cpf']);
            $this->db->where('cnpj', $parametros['cnpj']);
            $this->db->where('nome_turma', $parametros['nome_turma']);
            $this->db->where('ano', $parametros['ano']);
            $this->db->where('nome_mae', $parametros['nome_mae']);
            $this->db->where('dt_nascimento', $parametros['dt_nascimento']);
            $this->db->where('dt_ocorrencia', $parametros['dt_nascimento']);
            $this->db->update('testes');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }
        return $data;
    }
    
    
    
    
    }

    
?>

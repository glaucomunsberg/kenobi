<h2><?=lang('menuPrivadoGraficos')?><small><?=lang('menuPrivadoGraficosDef')?></small></h2>
<div class='row' style="margin-left: 40px">
    <div class="span6">
        <p style="text-align: justify">
            Selecione uma das suas <strong>turmas</strong> para obter estatísticas de cada uma delas individualmente</p>
        <p>
        <div class="btn-group">
          <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Turmas vinculadas<span class="caret"></span></button>
          <ul class="dropdown-menu">
              <?php
              if( empty($turmas)){
                  echo '<li><a href="#">Você não tem turmas cadastradas</a></li>';
              }else{
                foreach ($turmas as $turma){
                    echo '<li><a href="#" onClick="resultadosTurma(\''.$turma->cnpj.'\',\''.$turma->cpf.'\',\''.$turma->nome_turma.'\',\''.$turma->ano.'\')">'.$turma->nome_instituicao.' - '.$turma->nome_turma.' de '.$turma->ano.'</a></li>';
                }  
              }
              ?>
          </ul>
        </div>
    </div>
    <div class="span6">
        <p style="text-align: justify">
        Selecione um <strong>aluno</strong> para obter estatísticas individualmente</p>
        <p>
        <div class="btn-group">
          <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Alunos Vinculados<span class="caret"></span></button>
          <ul class="dropdown-menu">
              <?php
              if(empty($alunos) || $alunos == ''){
                  echo '<li><a href="#">Você não tem Alunos cadastrados</a></li>';
              }else{
                foreach ($alunos as $aluno){
                    echo '<li><a href="#" onClick="resultadosAluno(\''.$aluno->cnpj.'\',\''.$aluno->cpf.'\',\''.$aluno->nome.'\',\''.$aluno->nome_mae.'\',\''.$aluno->dt_nascimento.'\')">'.$aluno->nome.'</a></li>';
                }  
              }
              ?>
          </ul>
        </div>
    </div>
</div>
<br/>
<hr>
<br/>
<h4>Geral</h4>
<div class='row'>
    <div class='span6'>
        <div id="alunos_genero"></div>
    </div>
    <div class='span6' style="padding-left: 45px">
        <p style="padding-top: 25px">
            <strong style="font-size: 11px">Melhores Desempenhos Individual</strong>
        </p>
        <p style="padding-top: 5px">
            <strong>Melhor Desempenho:</strong> <abbr title="Uma média geral de apenas <?=@$melhor_desempenho_geral['aluno_media']?>"><?=@$melhor_desempenho_geral['aluno_nome']?></abbr>
        </p>
        <p>
            <strong>Melhor Corredora:</strong> <abbr title="Média de <?=@$melhor_corredor_feminino['corrida_vinte']?>"><?=@$melhor_corredor_feminino['aluno_nome']?></abbr>
        </p>
        <p>
            <strong>Melhor Corredor:</strong> <abbr title="Média de <?=@$melhor_corredor_masculino['corrida_vinte']?>"><?=@$melhor_corredor_masculino['aluno_nome']?></abbr>
        </p>
        
    </div>
</div>
<div class="row">
    <div class="span12">
        <div id="turmas_desempenho"></div>
    </div>
</div>
<div class="row" style="display: none">
    <div class="span12">
        <form action="<?=BASE_URL?>resultados/resultadoPorTurma/" method="post" id="byTurma">
           <input type="hidden" name="cpf" value="soemthing" id="cpf" />
           <input type="hidden" name="cnpj" value="soemthing" id="cnpj" />
           <input type="hidden" name="nome_turma" value="soemthing" id="nome_turma" />
           <input type="hidden" name="turma_ano" value="soemthing" id="turma_ano" />
        </form>
        <form action="<?=BASE_URL?>resultados/resultadoPorAluno/" method="post" id="byAluno">
           <input type="hidden" name="cpf" value="soemthing" id="cpf_a" />
           <input type="hidden" name="cnpj" value="soemthing" id="cnpj_a" />
           <input type="hidden" name="nome" value="soemthing" id="nome" />
           <input type="hidden" name="nome_mae" value="soemthing" id="nome_mae" />
           <input type="hidden" name="dt_nascimento" value="soemthing" id="dt_nascimento" />
        </form>
    </div>
</div>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    
    function resultadosTurma($cnpj,$cpf,$turmaNome,$turmaAno){
        $('#cnpj').val($cnpj);
        $('#cpf').val($cpf);
        $('#nome_turma').val($turmaNome);
        $('#turma_ano').val($turmaAno);
        document.getElementById('byTurma').submit();
    }
    
    function resultadosAluno($cnpj,$cpf,$nome,$nome_mae,$dt_nascimento){
        $('#cnpj_a').val($cnpj);
        $('#cpf_a').val($cpf);
        $('#nome').val($nome);
        $('#nome_mae').val($nome_mae);
        $('#dt_nascimento').val($dt_nascimento);
        document.getElementById('byAluno').submit();
    }

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(desenhaPorGenero);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function desenhaPorGenero() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Meninos', <?=@$num_alunos_masculino?>],
          ['Meninas', <?=@$num_alunos_femininos?>],
        ]);

        // Set chart options
        var options = {'title':'Alunos por Gênero',
                       'width':400,
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('alunos_genero'));
        chart.draw(data, options);
        
        
        
        
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Turmas');
        data.addColumn('number', 'Média Geral');
        data.addRows([
            <?php
                foreach ($media_por_turmas as $media_por_turma){
                    echo "['".$media_por_turma['nome_turma'].' / '.$media_por_turma['ano_turma']."',".$media_por_turma['media_turma']."],";
                }
            
            ?>
        ]);
        var options = {
          title: 'Média por turma',
          width:'100%',
          height:'100%'
          //vAxis: {title: 'Year',  titleTextStyle: {color: 'red'}}
        };

        barsVisualization = new google.visualization.ColumnChart(document.getElementById('turmas_desempenho'));
        barsVisualization.draw(data, options);

        // Add our over/out handlers.
        google.visualization.events.addListener(barsVisualization, 'onmouseover', barMouseOver);
        google.visualization.events.addListener(barsVisualization, 'onmouseout', barMouseOut);
      }

      function barMouseOver(e) {
        barsVisualization.setSelection([e]);
      }

      function barMouseOut(e) {
        barsVisualization.setSelection([{'row': null, 'column': null}]);
      }
        
</script>
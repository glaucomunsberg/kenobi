DROP TABLE IF EXISTS files_upload;

CREATE TABLE files_upload (
       id BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT
     , filename VARCHAR(255)
     , titulo VARCHAR(255)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (id)
);


<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	function __construct() {
            parent::__construct();
            $this->load->model('educadormodel', 'educadorModel');
            $this->load->model('instituicaomodel', 'instituicaoModel');
            $this->load->model('sistema/emailModel','emailModel');
            $this->load->model('sistema/usuariosModel','usuariosModel');
            $this->load->model('sistema/configuracaoModel','configuracaoModel');
        }
        
	public function index()
	{
            if($this->configuracaoModel->getSistemaOnline() == '1'){
                $this->load->view('site/dashboardView');
            }else{
                $this->load->view('site/manutencaoView');
            }
            
	}
        
        public function existeEducador(){
            print json_encode($this->educadorModel->existeEducador($_POST['cpf']));
        }
        
        public function existeInstituicao(){
            print json_encode($this->instituicaoModel->existeInstituicao($_POST['cnpj']));
        }
        
        public function existeEducadorEmail(){
            print json_encode($this->educadorModel->existeEducadorByEmail($_POST['email']));
        }
        
        public function cadastrar(){
            $instituicao = $educador = false;
            if($_POST['recaptcha_challenge_field'] == true){
                if(!$this->instituicaoModel->existeInstituicao($_POST['cnpj'])){
                    $instituicao = $this->instituicaoModel->inserirInstituicao($_POST);
                }else{
                    $instituicao = true;
                }
                
                if($instituicao){
                    if(!$this->educadorModel->existeEducador($_POST['cpf'])){
                        $instituicao = $this->educadorModel->inserirEducador($_POST);
                    }else{
                        $educador = true;
                    } 
                }else{
                    print json_encode( array('titulo'=>'Oops!', 'mensagem'=>'É constragedor, mas houve um erro ao cadastrar a instituição :/ Revise os dados ou tente novamente mais tarde.') );
                }
                
                if(!$this->educadorModel->existeRelacaoEducadorInstituicao($_POST['cpf'],$_POST['cnpj'])){
                    if($this->educadorModel->inserirEducadorInstituicao($_POST['cpf'],$_POST['cnpj'],0)){
                        
                        if($this->usuariosModel->inserirUsuario($_POST)){
                            
                            $educador = $this->educadorModel->getEducador($_POST['cpf']);
                            $educador = $educador[0];

                            $prodown_email      = $this->configuracaoModel->getEmail();
                            $prodown_senha      = $this->configuracaoModel->getSenha();
                            $prodown_email_admin= $this->configuracaoModel->getEmailAdministrador();
                            log_message('error',$educador->email);
                            $this->emailModel->enviarEmailBoasVindas($prodown_email,$prodown_senha,$educador->nome,$educador->email);
                            print json_encode( array('titulo'=>'Sucesso', 'mensagem'=>'Inserido com sucesso! Em breve você receberá um email de confirmação de inscrição, em seguida deve-se aguardar a liberação de sua conta pelo administrador.') );
                        }else{
                            print json_encode( array('titulo'=>'Error', 'mensagem'=>'Não foi possível criar um usuário. Tente novamente') );
                        }
                           
                    }else{
                        print json_encode( array('titulo'=>'Erro', 'mensagem'=>'Não conseguimos vincular você a instituição. Tente novamente mais tarde') );
                    }
                }else{
                    $educador = $this->educadorModel->getEducador($_POST['cpf']);
                    $educador = $educador[0];

                    $prodown_email      = $this->configuracaoModel->getEmail();
                    $prodown_senha      = $this->configuracaoModel->getSenha();
                    $prodown_email_admin= $this->configuracaoModel->getEmailAdministrador();
                    $titulo = 'Novo Cadastro';
                    $conteudo = "Olá Administrador do Sistema\n
                                    O educador ".$_POST['nome']." com o CPF ".$_POST['cpf']." solicitou liberação. :)";
                    $this->emailModel->notificaAdministradorDoSistema($prodown_email,$prodown_senha,$prodown_email_admin,$titulo,$conteudo);
                    print json_encode( array('titulo'=>'Sucesso', 'mensagem'=>'Você já está vinculado a essa instituição, caso não consiga acessar ao sistema entre em contato com o administrador') );
                }
            }else{
                print json_encode( array('titulo'=>'Oops!', 'mensagem'=>'Captcha está incorreto tente novamente') );
            }
        }
        
        public function recuperar(){
            $this->load->view('site/recuperarSenhaView');
        }
        
        public function help(){
            $this->load->view('help/dashboardView');
        }
        
        public function equipe(){
            $this->load->view('site/equipeSkywalkerView');
        }
        
        public function developers(){
            $this->load->view('help/developerView');
        }
        
        public function cody(){
            $this->load->view('help/codyView');
        }
        
        public function kenobi(){
            $this->load->view('help/kenobiView');
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
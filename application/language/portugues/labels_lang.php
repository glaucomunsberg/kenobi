<?php
$lang['kenobi'] = 'Kenobi';
$lang['skyWalker'] = 'SkyWalker';

$lang['tituloPagina'] = 'Prodown - SkyWalker';
$lang['proDown'] = 'ProDown';
$lang['proDownBemVindo'] = 'Bem-vindo';

$lang['menuPrivadoInicio'] = 'Início';
$lang['menuPrivadoEducador'] = 'EDUCADOR';
$lang['menuPrivadoEnviarAvaliacoes'] = 'Enviar Avaliações';
$lang['menuPrivadoEnviar'] = 'Carregador';
$lang['menuPrivadoVerResultado'] = 'Ver Resultados';
$lang['menuPrivadoResultadoTurma'] = 'Resultados';
$lang['menuPrivadoProgramaDesktop'] = 'Programa Desktop';
$lang['menuPrivadoNotificarAdmin'] = 'Notificar Administrador';
$lang['menuPrivadoNotificarEducador'] = 'Notificar Educador';
$lang['menuPrivadoAdministrador'] = 'ADMINISTRADOR';
$lang['menuPrivadoGerenciarUser'] = 'Gerenciar Usuário';
$lang['menuPrivadoModificarInf'] = 'Gerenciar Informações';
$lang['menuPrivadoSair'] = 'Sair';
$lang['menuPrivadoAjuda'] = 'Ajuda';
$lang['menuPrivadoDownload'] = 'Download';
$lang['menuPrivadoGraficos'] = 'Gráficos';
$lang['menuPrivadoExportar'] = 'Recuperar Turmas';
$lang['menuPrivadoConfig'] = 'Configurador';
$lang['menuPrivadoConta'] = 'Conta';

$lang['exportarArquivo']= 'Recuperador';
$lang['exportarFormato'] = 'Instituição e Formatos';
?>

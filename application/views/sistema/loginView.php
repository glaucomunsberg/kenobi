<?=headerGlobalView();?>
<div id="total" class="container">
    <div id="menuGeral" data-spy="affix-top" data-offset-top="0"  class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse" >
                    <ul class="nav">
                        <li class=""><a href="<?=BASE_URL?>" ><i class="icon-home"></i>Início</a></li>
                        <li class="disabled"><a href="#" >Sistema de Login</a></li>
                        <li class=""><a target="_blank" href="<?=BASE_URL.'site/help'?>" >Central de Ajuda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 50px">
        <h1><?=lang('proDown')?> <small>Sistema de Logar</small></h1>
    </div>
    <div class="row">
        <div class="span12">
            <p>Olá Colaborador, para efetuar o login é necessário seu Email ou CPF. Se você ainda não tem conta ou está tendo dificuldades para acessar a conta entre em contato com o administrador do sistema.</p>
                    
        </div>
    </div>
    <div class="row">
        <hr class="soften">
    </div>
    <div class="row">
        <div class="span6">
            <img src="<?=IMG.'login/login_down.jpg'?>" class="img-polaroid">
        </div>
        <div class="span6" style="margin-top:90px">
            <div class="control-group <?php if( !empty($error)){ echo "error";} ?>">
                <?php
                    echo form_open(BASE_URL . 'sistema/login/');
                ?>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-envelope"></i></span></span>
                    <input class="span3" id="cpf" name="cpf" type="text" placeholder="cpf ou email">
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-lock"></i></span>
                    <input class="span3" id="password" name="password" type="password" placeholder="senha">
                </div>
                <div>
                    <?php if( !empty($error)){ echo $error;} ?>
                </div>
            </div>
            <div style="margin-bottom: 5px">
                <?php echo form_submit('submit', 'Entrar','class="btn btn-success" style="width: 88px"');?>
                <?php 
                    if(empty($error)){
                        ?>
                            <a class="btn btn-link" href="<?=BASE_URL.'cadastro'?>"><strong>Inscreva-se já!</strong></a>
                        <?php
                    }
                ?>
                
                <?php 
                    if(!empty($error)){
                        ?>
                            <a class='btn btn-link' style="color: red" href="<?=BASE_URL.'recuperar'?>"><strong><i>Recuperar Senha?</i></strong></a>
                        <?php
                    }
                ?>
                <?php form_close(); ?>
            </div>

        </div> 
    </div>
    <div class="row">
        <div class="span12">
            <p>
                <h2>Perdeu sua senha? <small>nos podemos te ajudar!</small></h2>
                Entre em contato com o administrador do sistema para que ele gere uma nova senha.
            </p>
        </div>
    </div>
</div>
<script>
     $('#total').css('height',$(document).height()-208);
</script>
<?=footerView(); ?>

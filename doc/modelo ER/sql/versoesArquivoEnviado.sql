DROP TABLE IF EXISTS DEFAULT_SCHEMA.versoesArquivoEnviado;

CREATE TABLE DEFAULT_SCHEMA.versoesArquivoEnviado (
       file_hash VARCHAR(32) NOT NULL
     , cpf VARCHAR(11) NOT NULL
     , file_id BIGINT(20) UNSIGNED NOT NULL
     , versao_programa VARCHAR(20)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (file_hash, cpf)
     , INDEX (cpf)
     , CONSTRAINT FK_versoes_arquivo_enviado_1 FOREIGN KEY (cpf)
                  REFERENCES DEFAULT_SCHEMA.educadores (cpf)
     , INDEX (file_id)
     , CONSTRAINT FK_versoesArquivoEnviado_2 FOREIGN KEY (file_id)
                  REFERENCES DEFAULT_SCHEMA.files_upload (id)
);


<?php
class TurmaAlunosModel extends CI_Model{
    
 #retorna se o aluno pertence a determinada turma
function alunoPertenceTurma($cnpj,$cpf,$nome_turma,$ano,$nome_aluno,$nome_mae,$dt_nascimento){
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('turmaAlunos');
            $this->db->where('cnpj',$cnpj);
            $this->db->where('cpf',$cpf);
            $this->db->where('ano',$ano);
            $this->db->where('nome_turma',$nome_turma);
            $this->db->where('nome',$nome_aluno);
            $this->db->where('nome_mae',$nome_mae);
            $this->db->where('dt_nascimento',$dt_nascimento);
               
        $this->db->trans_complete();
        
        if($this->db->get()->num_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }      

   #atualiza o aluno em de tal turma
function atualizaAlunoTurma($cnpj,$cpf,$par_turma, $par_aluno){
        $this->db->trans_start();
            $this->db->set('esta_ativo',1);
            $this->db->where('cnpj',$cnpj);
            $this->db->where('cpf',$cpf);
            $this->db->where('ano',$par_turma['ano']);
            $this->db->where('nome_turma',$par_turma['nome_turma']);
            $this->db->where('nome',$par_aluno['nome']);
            $this->db->where('nome_mae',$par_aluno['nome_mae']);
            $this->db->where('dt_nascimento',$par_aluno['dt_nascimento']);
            $this->db->update('turmaAlunos');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            return true;
        }
    }   
    
  #insere novo aluno na turma  
function insereAlunoTurma($cnpj,$cpf,$par_turma, $par_aluno){
        $this->db->trans_start();
            $this->db->set('cnpj',$cnpj);
            $this->db->set('cpf',$cpf);
            $this->db->set('ano',$par_turma['ano']);
            $this->db->set('nome_turma',$par_turma['nome_turma']);
            $this->db->set('nome',$par_aluno['nome']);
            $this->db->set('nome_mae',$par_aluno['nome_mae']);
            $parts = explode('/', $par_aluno['dt_nascimento']);
            $date  = "$parts[2]-$parts[1]-$parts[0]";
            $this->db->set('dt_nascimento', $date);
            $this->db->set('esta_ativo',1);
            $this->db->set('dt_cadastro', date('ymd'));
            $this->db->insert('turmaAlunos');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            return true;
        }
    }  

   #retorna alunos da turma especifica
 function getAlunosByTurma($cnpj,$cpf,$turmaNome,$turmaAno){
 	$this->db->trans_start();
 		$this->db->select("a.nome, a.nome_mae, DATE_FORMAT(a.dt_nascimento,'%d/%m/%Y') as dt_nascimento, a.genero, a.nome_pai, a.cpf, a.rg, a.endereco", false);
 		$this->db->from('turmaAlunos as ta');
                $this->db->join('alunos as a','a.nome = ta.nome and a.nome_mae = ta.nome_mae and a.dt_nascimento = ta.dt_nascimento');
 		$this->db->where('ta.cnpj',$cnpj);
                $this->db->where('ta.cpf',$cpf);
                $this->db->where('ta.nome_turma',$turmaNome);
                $this->db->where('ta.ano',$turmaAno);
                $this->db->where('ta.esta_ativo',1);
 	$this->db->trans_complete();
 	
 	return $this->db->get()->result_array();
 	
    }
    
  function getNumAlunoGeneroByEducador($cpf,$genero){
 	$this->db->trans_start();
 		$this->db->select('a.nome, a.nome_mae, a.dt_nascimento, a.genero, a.nome_pai, a.cpf, a.rg, a.endereco');
 		$this->db->from('turmaAlunos as ta');
                $this->db->join('alunos as a','a.nome = ta.nome and a.nome_mae = ta.nome_mae and a.dt_nascimento = ta.dt_nascimento');
                $this->db->where('ta.cpf',$cpf);
                $this->db->where('a.genero',$genero);
                $this->db->where('ta.esta_ativo',1);
 	$this->db->trans_complete();
 	
 	return $this->db->get()->num_rows();
 	
    }
 	
 }


?>

<?php

/*ImportacaoModel : Refere-se ao modelo de importação dos dados que estão no arquivo.cody enviado.
 * */
 
class ImportacaoModel extends CI_Model{
	private $doc;
	function ImportacaoModel(){
	}

	//Função que carrega o arquivo, passado por url, na variavel doc para o uso geral da classe, retorna true em caso do url ser um xml, e false c.c.
	function abrirArquivo($url){
		if($url == NULL){
			
			return false;
		}
		
		$this->doc = simplexml_load_file($url);
		if($this->doc){
			return true;
		}
		
		return false;
	}
	
	//Função que pega a instituição do arquivo xml, retorna um array com os dados da instituição encontrada.
	function getInstituicao(){
		$inst = $this->doc;
		$instituicao = array("nome" => (string)$inst->nome, "cnpj" => (string)$inst->cnpj, 
		"instituicao_tipo" => (string)$inst->tipo, "hash" => (string)$inst->hash, "versao" => (string)$inst->versao);
		
		return $instituicao;
	}
	
	//Função que pega todas as turmas do XML, retorna um array com as informações das turmas encontradas.
	function getTurmas(){
		$turmas = $this->doc;
		$i = 0;
		foreach($turmas->xpath('//turma') as $turma){
			$turmaDados = array("nome_turma" => (string)$turma->nome, "ano" => (string)$turma->ano);
			$dadosDeTodasTurmas[$i] = $turmaDados;
			$i++;
		}
		return $dadosDeTodasTurmas;
	}
	
	//Função que pega todos os alunos do XML, retorna um array com a informação de todos os alunos encontrados.
	function getTodosAlunos(){
		$alunos = $this->doc;
		$i = 0;
		foreach($alunos->xpath('//aluno') as $aluno){
			
			$alunoDados = array("nome" => (string)$aluno->nome, "dt_nascimento" => (string)$aluno->dataNascimento, 
			"nome_mae" => (string)$aluno->nomeMae, "nome_pai" => (string)$aluno->nomePai, "endereco" => (string)$aluno->endereco, 
			"rg" => (string)$aluno->registroGeral, "cpf" => (string)$aluno->cadPessoaFisica, 
			"genero" => (string)$aluno->genero);
			
			$dadosDeTodosAlunos[$i] = $alunoDados;
			
			$i++;
		}
		return $dadosDeTodosAlunos;
	}
	
	/*Função que pega todos alunos da turma passada por parametro. 
	 * Retorna um array com os dados dos alunos daquela turma ou NULL caso a turma não exista no xml.
	 */
	
	function getAlunosTurma($turma){
		$turmas = $this->doc;
		$dadosDosAlunosDaTurma = '';

		foreach($turmas->xpath('//turma') as $turmaXML){
			//Condição para encontrar a turma passada por parâmetro
			if((strcmp( (string)$turmaXML->nome, $turma["nome_turma"]) == 0)  && (strcmp( (string)$turmaXML->ano,$turma["ano"])== 0)){
				$i = 0;
				
				//Pega todos os dados dos alunos da Turma indicada e retorna em um array.
				foreach($turmaXML->aluno as $aluno){
					$alunoDados = array("nome" => (string)$aluno->nome, "dt_nascimento" => (string)$aluno->dataNascimento, 
					"nome_mae" => (string)$aluno->nomeMae, "nome_pai" => (string)$aluno->nomePai, "endereco" => (string)$aluno->endereco, 
					"rg" => (string)$aluno->registroGeral, "cpf" => (string)$aluno->cadPessoaFisica, 
					"genero" => (string)$aluno->genero);
					
					$dadosDosAlunosDaTurma[$i] =  $alunoDados;
					$i++;
				}
				
				return $dadosDosAlunosDaTurma;
				
			}
		}
		
		return NULL;
	}
	
	function getTestes($turma, $aluno){
		$turmas = $this->doc;
		foreach($turmas->xpath('//turma') as $turmaXML){
			//Condição para encontrar a turma passada por parâmetro
			if((strcmp( (string)$turmaXML->nome, $turma["nome_turma"]) == 0)  && (strcmp( (string)$turmaXML->ano,$turma["ano"])== 0)){
				
				//Procura aluno dentro da turma;
				foreach($turmaXML->aluno as $alunoXML){
					
					//Condição para encontrar o aluno passado por parâmetro
					if((strcmp( (string)$alunoXML->nome, $aluno["nome"]) == 0)  && (strcmp( (string)$alunoXML->nomeMae,$aluno["nome_mae"])== 0 ) 
					&& ( strcmp((string)$alunoXML->dataNascimento,$aluno["dt_nascimento"])== 0)){
						$i = 0;
						
						foreach($alunoXML->teste as $teste){
							$tAS = $teste->testeAptidaoSaude;
							$tAE = $teste->testeAptidaoEsportivo;
							$mC = $teste->medidaCrescimento;
							
							$umTesteDoAluno = array("nome_teste" => (string)$teste->nome ,"dt_ocorrencia" => (string)$teste->data ,"num_abdominais" => (string)$tAS->numAbdominais, "imc" => (string)$tAS->imc,
							 "sentar_alcancar_com_banco" => (string)$tAS->sentarAlcancarComBanco , "sentar_alcancar_sem_banco" => (string)$tAS->sentarAlcancar,
							 "nove_minutos_saude" => (string)$tAS->noveMinutos, "seis_minutos_saude" => (string)$tAS->seisMinutos, "arremesso_medicineball" => 
							 (string)$tAE->arremessoDeMedicineball, "corrida_vinte_metros" => (string)$tAE->corridaVinteMetros, 
							"quadrado" => (string)$tAE->quadrado , "salto_em_distancia" => (string)$tAE->saltoEmDistancia, 
							"nove_minutos_esportivo" => (string)$tAE->noveMinutos, "seis_minutos_esportivo" => (string)$tAE->seisMinutos, "massa_corporal" => 
							(string)$mC->massaCorporal, "estatura" => (string)$mC->estatura, "envergadura" => (string)$mC->envergadura );
							
							$todosTestesDoAlunoDaTurma[$i] = $umTesteDoAluno;
							$i++;
						}
						return $todosTestesDoAlunoDaTurma;
					}
				}
				return NULL;
			}
		}
		return NULL;
	}
	
	
}

?>

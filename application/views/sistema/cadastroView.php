<?=headerGlobalView();?>
<div id="total" class="container">
    <div id="menuGeral" data-spy="affix-top" data-offset-top="0"  class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse" >
                    <ul class="nav">
                        <li class=""><a href="<?=BASE_URL?>" ><i class="icon-home"></i>Início</a></li>
                        <li class="disabled"><a href="#" >Central de Cadastro</a></li>
                        <li class=""><a target="_blank" href="<?=BASE_URL.'site/help'?>" >Central de Ajuda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 50px">
        <div class="span12">
            <h1><?=lang('proDown')?> <small>Cadastro</small></h1>
        </div>
    </div>
    <div class="row">
        <div class="span12">
            <div class="row">
                <div class="span12">
                    <p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
                        O sistema <strong>ProDown</strong> é apenas útil quando você faz parte dele. Cadastre-se e faça parte do projeto ProDown 
                        contribuindo para a comunidade down. Para que seja efetuado seu cadastro, você precisa informa usas informações e sobre a 
                        instituição na qual deseja realizar os testes ou que já tenha feito as avaliações. Precisa de ajuda para se cadastrar? 
                        Clique em "Central de Ajuda" no menu de ajuda.
                    </p>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px">
                <div class="span4">
                    <h5>Cadastro Pessoal</h5>
                    <p style="text-align: justify">
                        No cadastro pessoal estaremos coletando uma série de informações sobre você educador. É importante que lembre-se que o <strong>CPF</strong> deve ser válido e não seu.
                    </p>
                </div>
                <div class="span4">
                    <h5>Cadastro da Instituição</h5>
                    <p style="text-align: justify">
                        No cadastro da Instituição precisamos de informações que identifique-a. É importante lembrar que o <strong>CNPJ</strong> deve ser da instituição para a qual deseja criar o vínculo.
                    </p>
                </div>
                <div class="span4">
                    <h5>Confirmação e Observação</h5>
                    <p style="text-align: justify">
                        Deixe uma observação e por exemplo o porque da sua inscrição, isso ajuda na agilização da aprovação de seu pedido pelo administrador do sistema.
                    </p>
                </div>
            </div>
            <form id="form_cadastro" class="form-horizontal" novalidate="novalidate">
                    <fieldset>
                        <div class="row">
                            <div class="span6">
                                <h3>Cadastro Pessoal</h3>
                                <div id="controle_nome"  class="control-group">
                                    <label class="control-label" for="input01">Seu Nome</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" name="nome" id="nome">
                                        <p class="help-block" id="controle_nome_mensagem"></p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input01">Sobrenome</label>
                                    <div class="controls">
                                        <input type="text" class="input-xlarge" id="sobrenome" name="sobrenome">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="select01">Sexo</label>
                                    <div class="controls">
                                        <select id="genero" name="genero" class="valid">
                                            <option>Feminino</option>
                                            <option>Masculino</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="controle_cpf" class="control-group">
                                    <label class="control-label" for="input01">CPF</label>
                                    <div class="controls">
                                        <input type="text" id="cpf" name="cpf" class="input-xlarge"   data-toggle="tooltip" data-placement="right" title="" data-original-title="Oi! Digite o seu CPF e confira-o antes de clicar em cadastrar">
                                        <p class="help-block" id="controle_cpf_mensagem"></p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input01">Registro Geral</label>
                                    <div class="controls">
                                        <input type="text" name="rg"  id="rg">
                                    </div>
                                </div>
                                <div id="controle_email" class="control-group">
                                    <label class="control-label" for="input01">Email</label>
                                    <div class="controls">
                                        <input type="text" id="email"  name="email" class="input-xlarge" >
                                        <p class="help-block" id="controle_email_mensagem"></p>
                                    </div>
                                </div>
                                <div id="control_senha" class="control-group">
                                    <label class="control-label" for="input01">Senha</label>
                                    <div class="controls">
                                        <input type="password"  name="senha1" class="input-small" id="senha1">
                                        <input type="password" name="senha2" class="input-small" id="senha2">
                                        <p class="help-block" id="control_senha_mensagem"></p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input01">Cidade</label>
                                    <div class="controls">
                                        <?=form_file_select('cidadeEducador',7754, @$cidades,50,'cidadeEducador')?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input01">Rua/Avenida</label>
                                    <div class="controls">
                                        <input type="text" name="enderecoEducador"  class="input-xlarge" id="enderecoEducador">
                                    </div>
                                </div>
                                <div class="control-group" id="controle_dtnascimento" >
                                    <label class="control-label" for="input01">Dt. Nascimento</label>
                                    <div class="controls">
                                        <?=getCalendario('dt_nascimento','DD/MM/AAAA')?>
                                        <p class="help-block" id="controle_dtnascimento_mensagem"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <h3>Cadastro da Instituição</h3>
                                <div id="controle_cnpj"  class="control-group">
                                    <label class="control-label" for="input01" >CNPJ</label>
                                    <div class="controls">
                                        <input id="cnpj" name="cnpj" type="text" class="input-xlarge"  data-toggle="tooltip" data-placement="right" title="" data-original-title="Toda Instituição tem um CNPJ pela qual se comunica, tenha em mãos o CNPJ da instituição que você deseja-se cadastrar." >
                                        <p class="help-block" id="controle_cnpj_mensagem"></p>
                                    </div>
                                </div>
                                <div id="controle_nome_inst" class="control-group">
                                    <label class="control-label" for="input01">Nome</label>
                                    <div class="controls">
                                        <input type="text" name="nomeInstituicao" class="input-xlarge" id="nomeInstituicao">
                                        <p class="help-block" id="controle_nome_inst_mensagem"></p>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"  for="select01">Ensino</label>
                                    <div class="controls">
                                        <select id="tipo" name="tipo" class="valid">
                                            <option>Especial</option>
                                            <option>Regular</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input01">Cidade</label>
                                    <div class="controls">
                                        <?=form_file_select('cidadeInstituicao',7754, @$cidades,50,'cidadeInstituicao')?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="input01">Rua/Avenida</label>
                                    <div class="controls">
                                        <input type="text" name="enderecoInstituicao" class="input-xlarge" id="instiEndereco">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="span6">
                                <h3>Confirmação e Observação</h3>
                                <div id="controle_observacao"  class="control-group">
                                    <label class="control-label" for="textarea">Observ.</label>
                                    <div class="controls">
                                        <textarea class="input-xlarge" id="observacao" name="observacao" rows="4"></textarea>
                                        <p class="help-block" id="controle_observacao_mensagem"></p>
                                    </div>
                                </div> 
                            </div>
                            <div class='span6'>
                                <div class="control-group" style='margin-top: 55px'>
                                    <label class="control-label" for="textarea">reCaptcha</label>
                                    <div class="controls">
                                        <script type="text/javascript"
                                                src="http://www.google.com/recaptcha/api/challenge?k=6LcKXN8SAAAAANwLjlwa_iYAISOBWqBY0DQxPK2d">
                                                Recaptcha.create("6LcKXN8SAAAAANwLjlwa_iYAISOBWqBY0DQxPK2d",
                                                    "element_id",
                                                    {
                                                        lang : 'pt',
                                                        theme : 'white',
                                                        callback: Recaptcha.focus_response_field
                                                    }
                                                  );
                                                var RecaptchaOptions = {

                                                 };
                                        </script>
                                        <noscript>
                                        <iframe src="http://www.google.com/recaptcha/api/noscript?k=your_public_key"
                                                height="300" width="500" frameborder="0"></iframe><br>
                                        <textarea name="recaptcha_challenge_field" rows="3" cols="40">
                                        </textarea>
                                        <input type="hidden" name="recaptcha_response_field"
                                               value="manual_challenge">
                                        </noscript>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="span12">
                                <div class="form-actions">
                                    <a id="cadastra" onClick="validar()" class="btn btn-success"  data-loading-text="Cadastrando...">Cadastrar</a>
                                    <a class="btn btn-primary" onClick="limparDados()">Limpar Campos</a>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
        </div>
    </div>
    
</div>
<script>

     $('#total').css('height',$(document).height()-208);
     $('#cpf').tooltip('toggle');
     $('#cpf').tooltip('hide');
     $('#cnpj').tooltip('toggle');
     $('#cpf').tooltip('hide');
     $tudoOk = true;
     function validar(){
         /**
          * Valida o CPF
          * 
          */
         var cpf=$('#cpf').val();
         cpf=cpf.replace(/\./g,'');
         cpf=cpf.replace(/-/g,'');
         
         if(validarCPF(cpf)){
             $('#controle_cpf').removeClass('error');
             $('#controle_cpf_mensagem').hide();
             $.ajax({
                method: "post",
                data: { cpf: cpf },
                url: BASE_URL+"site/existeEducador",
                success : function(data) {
                    var resposta = JSON.parse(data);
                    if(resposta){
                        $('#controle_cpf').addClass('error');
                        $('#controle_cpf_mensagem').text('Atenção! O CPF já está cadastrado');
                        $('#controle_cpf_mensagem').show();
                        $tudoOk = false;
                    }
                }
              });
         }else{
            $('#controle_cpf').addClass('error');
            $('#controle_cpf_mensagem').text('Atenção! CPF não é válido!');
            $('#controle_cpf_mensagem').show();
            $tudoOk = false;
         }
          
          /**
           * Valida email, senha, nome do educador, observacao e dt.nascimento e nome da instituicao
           */
          
          if( $('#email').val().indexOf('@') == -1){
                $('#controle_email').addClass('error');
                $('#controle_email_mensagem').text('Atenção! Esse email não é válido');
                $('#controle_email_mensagem').show();
                $tudoOk = false;
          }else{
                $('#controle_email').removeClass('error');
                $('#controle_email_mensagem').hide();  
          }
          
          if( $('#senha1').val() !=  $('#senha2').val() ){
                $('#control_senha').addClass('error');
                $('#control_senha_mensagem').text('Senhas não são iguais, tente novamente');
                $('#control_senha_mensagem').show();
                $tudoOk = false;
          }else{
                $('#control_senha').removeClass('error');
                $('#control_senha_mensagem').hide();  
          }
          
          if($('#dt_nascimento').val() == 'DD/MM/AAAA'){
                $('#controle_dtnascimento').addClass('error');
                $('#controle_dtnascimento_mensagem').text('Quando vocẽ faz aniversário? O formato correto DD/MM/AAAA');
                $('#controle_dtnascimento_mensagem').show();
                $tudoOk = false;
          }else{
                $('#controle_dtnascimento').removeClass('error');
                $('#controle_dtnascimento_mensagem').hide();  
          }
          var n= $('#dt_nascimento').val().split("/");
          if(n[2] > 2000){
                $('#controle_dtnascimento').addClass('error');
                $('#controle_dtnascimento_mensagem').text('Atenção! Ano de nascimento insuficiente');
                $('#controle_dtnascimento_mensagem').show();
                $tudoOk = false;
          }else{
                $('#controle_dtnascimento').removeClass('error');
                $('#controle_dtnascimento_mensagem').hide();  
          }
          
          if( $('#observacao').val() == ''){
                $('#controle_observacao').addClass('warning');
                $('#controle_observacao_mensagem').text('Você tem algo a nos dizer?');
                $('#controle_observacao_mensagem').show();
          }else{
                $('#controle_observacao').removeClass('warning');
                $('#controle_observacao_mensagem').hide();  
          }
          
          if( $('#nome').val() == ''){
                $('#controle_nome').addClass('error');
                $('#controle_nome_mensagem').text('Atenção! É necessário digitar todo seu nome');
                $('#controle_nome_mensagem').show();
                $tudoOk = false;
          }else{
                $('#controle_nome').removeClass('error');
                $('#controle_nome_mensagem').hide();  
          }
          
          if( $('#nomeInstituicao').val() == ''){
                $('#controle_nome_inst').addClass('error');
                $('#controle_nome_inst_mensagem').text('Atenção! Nome da instituição');
                $('#controle_nome_inst_mensagem').show();
                
                $tudoOk = false;
          }else{
                $('#controle_nome_inst').removeClass('error');
                $('#controle_nome_inst_mensagem').hide();  
          }
          
          /**
           * Valida CNPJ
           */          
          var cnpj=$('#cnpj').val();
          cnpj=cnpj.replace(/\./g,'');
          cnpj=cnpj.replace(/\//g,'');
          
          if(validarCNPJ(cnpj)){
             $('#controle_cnpj').removeClass('warning');
             $('#controle_cnpj').removeClass('error');
             $('#controle_cnpj_mensagem').hide();
             $.ajax({
                method: "post",
                data: { cnpj: cnpj },
                url: BASE_URL+"site/existeInstituicao",
                success : function(data) {
                    var resposta = JSON.parse(data);
                    if(resposta){
                        $('#controle_cnpj').addClass('warning');
                        $('#controle_cnpj_mensagem').text('A instituição já faz parte do banco de dados. Iremos cadastrá-lo nela.');
                        $('#controle_cnpj_mensagem').show();
                    }
                }
              });
         }else{
            $('#controle_cnpj').addClass('error');
            $('#controle_cnpj_mensagem').text('CNPJ não é válido');
            $('#controle_cnpj_mensagem').show();
            $tudoOk = false;
         }
         
         $.ajax({
            method: "post",
            data: { email: $('#email').val() },
            url: BASE_URL+"site/existeEducadorEmail",
            success : function(data) {
                var resposta = JSON.parse(data);
                if(resposta){
                    $('#controle_email').addClass('error');
                    $('#controle_email_mensagem').text('O email já está cadastrado no nosso sistema.');
                    $('#controle_email_mensagem').show();
                    console.log('email já existe');
                    $tudoOk = false;
                }
            }
          });
         
         if($tudoOk != false){
            $('#aviso_titulo').text('Aguarde...');
            $('#aviso_mensagem').html(
                       "<img src=\"<?=IMG?>loading.gif\"><p>Cadastrando...</p>");
            $('#aviso').modal('show');
            $.ajax({
                method: "post",
                data: $("#form_cadastro").serialize(),
                url: BASE_URL+"site/cadastrar",
                error: function(){
                    $('#aviso_titulo').text('Ooops!');
                    $('#aviso_mensagem').text('Houve um erro ao cadastrar');
                    $('#aviso').modal('show');
                },
                success : function(data) {
                    var resposta = JSON.parse(data);
                    $('#aviso_titulo').text(resposta.titulo);
                    $('#aviso_mensagem').text(resposta.mensagem);
                    $('#aviso').modal('show');
                    
                }
              });
         }
     }
     function limparDados(){
        $('#nome').val('');$('#genero').val('');
        $('#nome').val('');$('#sobrenome').val('');
        $('#cpf').val('');$('#rg').val('');
        $('#email').val('');$('#senha1').val('');
        $('#senha2').val('');$('#senha1').val('');
        $('#cidadeEducador').val('');$('#cidadeEducadorId').val('');
        $('#enderecoEducador').val('');$('#dt_nascimento').val('');
        $('#cnpj').val('');$('#nomeInstituicao').val('');
        $('#cidadeInstituicao').val('');$('#cidadeInstituicaoId').val('');
        $('#instiEndereco').val('');$('#observacao').val('');
     }
     
     function cidadeEducador($id,$nome){
         return true;
     }
     
     function cidadeInstituicao($id,$nome){
         return true;
     }
     
     function validarCPF(cpf) {

        cpf = cpf.replace(/[^\d]+/g,'');

        if(cpf == '') return false;

        // Elimina CPFs invalidos conhecidos
        if (cpf.length != 11 || 
            cpf == "00000000000" || 
            cpf == "11111111111" || 
            cpf == "22222222222" || 
            cpf == "33333333333" || 
            cpf == "44444444444" || 
            cpf == "55555555555" || 
            cpf == "66666666666" || 
            cpf == "77777777777" || 
            cpf == "88888888888" || 
            cpf == "99999999999")
            return false;

        // Valida 1o digito
        add = 0;
        for (i=0; i < 9; i ++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;

        // Valida 2o digito
        add = 0;
        for (i = 0; i < 10; i ++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return false;

        return true;

    }
    
     function validarCNPJ(cnpj) {

        cnpj = cnpj.replace(/[^\d]+/g,'');

        if(cnpj == '') return false;

        if (cnpj.length != 14)
            return false;

        // Elimina CNPJs invalidos conhecidos
        if (
            cnpj == "00000000000000" ||
            cnpj == "11111111111111" || 
            cnpj == "22222222222222" || 
            cnpj == "33333333333333" || 
            cnpj == "44444444444444" || 
            cnpj == "55555555555555" || 
            cnpj == "66666666666666" || 
            cnpj == "77777777777777" || 
            cnpj == "88888888888888" || 
            cnpj == "99999999999999")
            return false;

        // Valida DVs
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0,tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
          soma += numeros.charAt(tamanho - i) * pos--;
          if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;

        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
          soma += numeros.charAt(tamanho - i) * pos--;
          if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
              return false;

        return true;

    }
    
    $('#cpf').maskInput("999.999.999-99");
    $('#rg').maskInput("9999999999");
    $('#cnpj').maskInput("99.999.999/9999-99");
</script>
<?=footerView(); ?>

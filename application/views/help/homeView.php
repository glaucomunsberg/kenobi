<div class="hero-unit">    
    
    <p>
        <a href="#" class="thumbnail">
            <img src="<?=IMG.'ajuda/ajuda_home.jpg'?>" alt="">
        </a>
    </p>
    <h3>Olá Colaborador</h3>
    <p style="text-align: justify; text-indent: 20px"> Aqui você poderá obter ajuda a respeito do sistema <strong>ProDown</strong>, como por exemplo: como realizar o cadastro, quanto a realização dos testes, ao envio de avaliações, sobre as normas,
    suas dúvidas frequentes e outras informações que possam ser úteis.
    </p>
    <p style="text-align: justify; text-indent: 20px"> 
        Através do menu lateral você terá contato com uma série de informações úteis selecionados segundo as maiores dúvidas de nossos colaboradores.
    </p>
</div>    
   

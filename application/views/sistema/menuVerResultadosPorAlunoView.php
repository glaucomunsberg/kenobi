<?=headerGlobalView();?>
<div class="container">
    <div class="row">
        <div class="page-header">
            <h1><?= lang('proDown') ?> <small><?= lang('kenobiSistema') ?></small></h1>
        </div>
    </div>
    <div class="row">
        <div class="span3">
            <div class="well" style="padding: 8px 0;">
                <ul class="nav nav-list">
                    <li class="disabled"><a><i class="icon-book"></i> <?= lang('menuPrivadoVerResultado') ?></a></li>
                    <li><a href="<?= BASE_URL . 'sistema/areaRestrita#resultados' ?>" ><i class="icon-chevron-left"></i>Voltar</a></li>
                </ul>
            </div>
        </div>
        <div id="tabs" class="span5">
            <h2>Aluno<small><?=' '.$testes[0]['nome']. '.'?></small></h2>
            <form action="<?=BASE_URL?>resultados/resultadoPorAluno/" method="post" id="byAluno">
               <input type="hidden" name="cpf" value="soemthing" id="cpf_a" />
               <input type="hidden" name="cnpj" value="soemthing" id="cnpj_a" />
               <input type="hidden" name="nome" value="soemthing" id="nome" />
               <input type="hidden" name="nome_mae" value="soemthing" id="nome_mae" />
               <input type="hidden" name="dt_nascimento" value="soemthing" id="dt_nascimento" />
            </form>
        </div>
        <div class="span4">
            <p>
            Deseja ver resultados de outros alunos?</p>
            <p>
            <div class="btn-group">
              <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Selecine outro aluno<span class="caret"></span></button>
              <ul class="dropdown-menu">
                  <?php
                  foreach ($alunos as $aluno){
                       echo '<li><a href="#" onClick="resultadosAluno(\''.$aluno->cnpj.'\',\''.$aluno->cpf.'\',\''.$aluno->nome.'\',\''.$aluno->nome_mae.'\',\''.$aluno->dt_nascimento.'\')">'.$aluno->nome.'</a></li>';
                   }
                  ?>
              </ul>
            </div>
        </div>
    </div>
    <div class='row'>
        <? if($testes[0]['dt_ocorrencia']==0){
    ?>
     <div class="row-fluid" >
                    <div class="span4">
                    </div>
                         <div class="span8">
                        <div>
                            <h3>Este Aluno não possui teste</h3>
                        </div>
                         </div>
</div><? }else{
    ?>

        <div class='span12'>  
            <div id="chart_div" style="width: 900px; height: 500px;"></div>
        </div>
        <div class="row-fluid" >
                    <div class="span4">
                    </div>
                         <div class="span8">
                        <div>
                            <h3>Resultado Individual em Tabela</h3>
                        </div>
                         </div>
</div>
        <div id='table_div'></div>
    </div> 
    <div style="width: 200px; height: 100px;">  
    </div>
        
    <p>
    <h1> </h1>
    </p>
</div>
<? } ?>




 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
      <?
      if(!($testes[0]['dt_ocorrencia']==0)){
      ?>
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Data', 'Força Abdominal','Arremeço MedicineBall','Corrida Vinte metros','Salto Horizontal','Teste do Quadrado'],
         
           <?
          
               foreach ($testes as $avaliacao){
               echo "['".$avaliacao['dt_ocorrencia']."',".$avaliacao['forcaAbdominal'].",".$avaliacao['membroSuperior'].",".$avaliacao['corrida_vinte_metros']. ","
                      .$avaliacao['salto_horizontal'].",".$avaliacao['quadrado']. "],";
          }
           
?>        ]);

        var options = {
          title: '<?echo $testes[0]['nome'] ?>'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      <? } ?>
      function resultadosAluno($cnpj,$cpf,$nome,$nome_mae,$dt_nascimento){
        $('#cnpj_a').val($cnpj);
        $('#cpf_a').val($cpf);
        $('#nome').val($nome);
        $('#nome_mae').val($nome_mae);
        $('#dt_nascimento').val($dt_nascimento);
        document.getElementById('byAluno').submit();
     }
    </script>
   <head>
    <script type='text/javascript' src='https://www.google.com/jsapi'></script>
    <script type='text/javascript'>
      google.load('visualization', '1', {packages:['table']});
      google.setOnLoadCallback(drawTable);
      function drawTable() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Data');
        data.addColumn('string', 'Força Abdominal');
        data.addColumn('string', 'Membro Superior');
        data.addColumn('string', 'Corrida vite metros');
        data.addColumn('string', 'Salto Horizontal');
        data.addColumn('string', 'Teste do Quadrado');
       
       
        data.addRows([
          <?  foreach ($testes as $avaliacao){
            echo"['".$avaliacao['dt_ocorrencia']."',";
            switch ($avaliacao['forcaAbdominal']) {
                case 0:
                    echo "'Não fez'";
                    break;
                case 1;
                    echo "'Fraco'";
                    break;
                case 2;
                    echo "'Razoavel'";
                    break;
                case 3;
                    echo "'Bom'";
                    break;
                case 4;
                    echo "'Muito bom'";
                    break;
                case 5;
                    echo "'Otimo'";
                    break;
                default:
                    break;
            }
            echo ",";
             switch ($avaliacao['membroSuperior']) {
                case 0:
                    echo "'Não fez'";
                    break;
                case 1;
                    echo "'Fraco'";
                    break;
                case 2;
                    echo "'Razoavel'";
                    break;
                case 3;
                    echo "'Bom'";
                    break;
                case 4;
                    echo "'Muito bom'";
                    break;
                case 5;
                    echo "'Otimo'";
                    break;
                default:
                    break;
            }
             echo ",";
             switch ($avaliacao['corrida_vinte_metros']) {
                case 0:
                    echo "'Não fez'";
                    break;
                case 1;
                    echo "'Fraco'";
                    break;
                case 2;
                    echo "'Razoavel'";
                    break;
                case 3;
                    echo "'Bom'";
                    break;
                case 4;
                    echo "'Muito bom'";
                    break;
                case 5;
                    echo "'Otimo'";
                    break;
                default:
                    break;
            }
             echo ",";
             switch ($avaliacao['salto_horizontal']) {
                case 0:
                    echo "'Não fez'";
                    break;
                case 1;
                    echo "'Fraco'";
                    break;
                case 2;
                    echo "'Razoavel'";
                    break;
                case 3;
                    echo "'Bom'";
                    break;
                case 4;
                    echo "'Muito bom'";
                    break;
                case 5;
                    echo "'Otimo'";
                    break;
                default:
                    break;
            }
            echo ",";
             switch ($avaliacao['quadrado']) {
                case 0:
                    echo "'Não fez'";
                    break;
                case 1;
                    echo "'Fraco'";
                    break;
                case 2;
                    echo "'Razoavel'";
                    break;
                case 3;
                    echo "'Bom'";
                    break;
                case 4;
                    echo "'Muito bom'";
                    break;
                case 5;
                    echo "'Otimo'";
                    break;
                default:
                    break;
            }
         
             echo    "],"; 
              
          }
          echo "]);";
       ?>

        var table = new google.visualization.Table(document.getElementById('table_div'));
        table.draw(data, {showRowNumber: true});
      }
    </script>
  </head>

  
<?=footerView(); ?>
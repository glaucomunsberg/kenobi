<div class="hero-unit">
    <h2>Bem Vindo! <small></small></h2>
    <p style="text-align: justify; text-indent: 40px; margin-top: 15px">
        ProDown é uma associação sem fins lucrativos para atendimentos especializados de crianças com necessidades especiais, tendo um Centro de Equoterapia e profissionais da área de Psicologia, Fisioterapia, Educação Especial e Pedagogia.
    </p>  
    <p class ="row" style="text-align: center">    
            <a type="button" class="btn btn-info btn-large" href="#programaDesktop"><strong>Programa Desktop</strong></a>
    </p>
    <p>
    </p>
</div>
<div class="row">
    <div class="span12">
        <ul class="thumbnails">
            <li class="span3">
                <div class="thumbnail">
                    <img src="<?=IMG.'home/exercicio1.jpg'?>" alt="">
                    <div class="caption">
                        <h5>Hipismo</h5>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    </div>
                </div>
            </li>
            <li class="span3">
                <div class="thumbnail">
                    <img src="<?=IMG.'home/exercicio2.jpg'?>" alt="">
                    <div class="caption">
                        <h5>Recreação</h5>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    </div>
                </div>
            </li>
            <li class="span3">
                <div class="thumbnail">
                    <img src="<?=IMG.'home/exercicio3.jpg'?>" alt="">
                    <div class="caption">
                        <h5>Ginástica</h5>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    </div>
                </div>
            </li>
            <li class="span3">
                <div class="thumbnail">
                    <img src="<?=IMG.'home/exercicio4.jpg'?>" alt="">
                    <div class="caption">
                        <h5>Inclusão</h5>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
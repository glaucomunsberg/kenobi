<h2><?=lang('menuPrivadoConfig')?><small> de conta</small></h2>
<div class="row">
    <div class="span12" style="margin-left: 40px">
        <p>
            Aqui nesse painel você poderá modificar suas informações de identificação. Isso ajuda na sua indentificação, bem como
                agiliza a troca de informações com o administrador do sistema.
        </p>
        <p>
        <form class="form-horizontal" novalidate="novalidate" id="atualizarUsuario">
                <fieldset>
                  <div class="control-group">
                    <label class="control-label" for="disabledInput">CPF</label>
                    <div class="controls">
                      <input class="input-xlarge disabled" id="u_cpf" type="text" placeholder="<?=@$usuario->cpf?>" disabled="">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="focusedInput">Nome</label>
                    <div class="controls">
                      <input class="input-xlarge focused valid" name="u_name" id="u_nome" type="text" value="<?=@$usuario->nome?>">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="focusedInput">Sobrenome</label>
                    <div class="controls">
                      <input class="input-xlarge focused valid" name="u_sobrenome" id="u_sobrenome" type="text" value="<?=@$usuario->sobrenome?>">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="focusedInput">Email</label>
                    <div class="controls">
                      <input class="input-xlarge focused valid" name="u_email" id="u_email" type="text" value="<?=@$usuario->email?>">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="focusedInput">Dt. Nascimento</label>
                    <div class="controls">
                      <input class="input-xlarge focused valid" name="u_dt_nascimento" id="u_dt_nascimento" type="text" value="<?php
                        $parts = explode('-', @$usuario->dt_nascimento);
                        $date  = "$parts[2]/$parts[1]/$parts[0]";
                        echo $date?>">
                    </div>
                  </div>
                  <div class="control-group">
                    <label class="control-label" for="focusedInput">Senha</label>
                    <div class="controls">
                      <input type="password" id="u_senha" name="u_senha" class="input-small" placeholder="nova senha"><input id="u_contra_senha"type="password" class="input-small" placeholder="novamente">
                      <p class="help-block">Para permanecer a mesma senha basta não inserir nada no campo</p>
                    </div>
                  </div>
                </fieldset>
              </form>
              <div class="controls">
                <button class="btn btn-primary" onClick="enviarDados()" href="#">Salvar Modificações</button>
                </div>
        </p>
    </div>
</div>
<script>
    function enviarDados(){
        if($('#u_senha').val() != $('#u_contra_senha').val() ){
            alert('A senha não é igual a confirmação');
            $('#u_senha').val('');
            $('#u_contra_senha').val('');
            exit;
        }else{
           if( $('#u_senha').val().length < 5 && $('#u_senha').val().length > 0){
                alert('Sua senha tem que ter no mínimo 8 Caracteres');
                exit;
            } 
        }
        $.post("<?=BASE_URL.'sistema/login/atualizarUsuario'?>", 
            {
                u_nome:$('#u_nome').val(),
                u_sobrenome:$('#u_sobrenome').val(),
                u_email:$('#u_email').val(),
                u_dt_nascimento:$('#u_dt_nascimento').val(),
                u_senha:$('#u_senha').val()
            },
            function(data) {
                if(data == 1 || data == true){
                    $('#aviso_titulo').text('Conta Salva');
                    $('#aviso_mensagem').text('Sua conta foi salva com sucesso!');
                    $('#aviso').modal('show');
                    
                }else{
                    $('#aviso_titulo').text('Oops!');
                    $('#aviso_mensagem').text('Algo de muito errado aconteceu com nosso servidor :/ Atualize a página ou tente mais tarde.');
                    $('#aviso').modal('show');
                }
            });
        }
    
    
    

</script>
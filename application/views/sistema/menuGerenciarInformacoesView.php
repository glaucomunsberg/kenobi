<?=headerGlobalView();?>
<div class="container">
    <div class="row-fluid">
        <div class="page-header">
            <h1><?=lang('proDown')?> <small><?=lang('kenobiSistema')?></small></h1>
            
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
            <div class="well" style="padding: 8px 0;">
                <ul class="nav nav-list">
                   <li class="disabled"><a><i class="icon-book"></i> Gerenciar Informações</a></li>
                  <li><a href="<?=BASE_URL.'sistema/areaRestrita'?>" ><i class="icon-chevron-left"></i>Voltar</a></li>
                </ul>
              </div>
        </div>
        <div id="tabs" class="span9">
            <h2> Gerenciador<small> de Informações</small></h2>
        </div>
    </div>
    <div class="row">
        <div class="span12" style="margin-left: 40px">
            <p>
                Abaixo você como administrador poderá fazer modificações nas informações dos educadores, para isso você necessita escolher o educador e logo a abaixo aparecerá as opções do que é possível fazer.
            </p>
        </div>
    </div>
    <div class="row" style="margin-left: 20px;margin-top: 20px">
        <div class="input-prepend">
          <span class="add-on">Educador </span>
          <?=form_file_select('pessoa','', @$educadores,200,'cpfEscolhido')?>
        </div>
        <div>
    </div>
    <div id="nomeEducador" class="row"  style="display: none;margin-left: 0px">

    </div>
    <div id="ComoEducador" class="row" style="display: none;margin-left: 40px">
        <div class="span12" >
            <ul id="menu" class="nav nav-list nav-stacked">
              <li id="senha" class=""><a href="#gerenciarInfo" onClick="mudarMenu('menuSenha')"><i class="icon-chevron-right"></i> Gerar uma nova senha</a></li>
              <li id="revogar" class=""><a href="#gerenciarInfo" onClick="mudarMenu('menuRevogar')"><i class="icon-chevron-right"></i> Revogar Administração de Instituição</a></li>
              <li id="exportacao" class=""><a href="#gerenciarInfo" onClick="mudarMenu('menuExportacao')"><i class="icon-chevron-right"></i> Recuperar Informações</a></li>
              <li id="alunos" class=""><a href="#gerenciarInfo" onClick="mudarMenu('menuAlunos')"><i class="icon-chevron-right"></i> Mudar informação de Alunos</a></li>
              <li id="testes" class=""><a href="#gerenciarInfo" onClick="mudarMenu('menuTestes')"><i class="icon-chevron-right"></i> Mudar informação de Testes</a></li>     
            </ul>
        </div>
    </div>
    <div id="divisor" class="row" style="display: none">
        <hr>
    </div>
    <div id="menuSenha" class="row" style="display: none; margin-left: 40px">
        <h4>
           Gerenciar Senha
        </h4>
        <div class="span12">
            <p>Você aqui modificará a senha do educador, fazendo com o que a senha atual seja inválida na próximo login do usuário. Sendo ela criptografada e apenas de acesso ao educador.</p>
            <div class="alert alert-error">
                <p><strong>Atenção!</strong></p>
                <p>Será gerado uma senha de 8 dígitos e será enviada via email para o email do educador.</p>
            </div>
            <h2></h2>
            <a id="btn-gerar"type="button" onClick="gerarSenha()" class="btn btn-danger" data-loading-text="Gerando...">Gerar uma nova senha</a>
            <script>
                $('#btn-gerar').click(function () {
                    var btn = $(this)
                    btn.button('loading')
                    setTimeout(function () {
                        btn.button('reset')
                    }, 5000)
                });
            </script>
        </div>
    </div>
    <div id="menuExportacao" class="row" style="display: none; margin-left: 40px">
        <h4>
           Gerenciar Exportação
        </h4>
        <div class="span12">
            <p>
                Escolha uma instiuição e o formato. Lembre-se: O arquivo apenas será gerado com as turmas a qual o educador ainda se encontra vinculado e alunos vinculadas a essas turmas.
            </p>
            <div class="btn-toolbar">
                <div class="btn-group">

                    <a id="botaoInsti" class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                        Instituições
                        <span class="caret"></span>
                    </a>
                    <ul  id="instituicoes_do_menu"class="dropdown-menu">
                        <?php
                        if ($instituicoes == '') {
                            echo '<li><a>Você não tem Instituições Vinculada</a></li>';
                        } else {
                            foreach ($instituicoes as $instituicao) {
                                echo '<li><a onClick="selecionaCNPJ(\'' . $instituicao->cnpj . '\',\'' . $instituicao->nome . '\')">' . $instituicao->nome . '</a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
                <div class="btn-group">

                    <a id="botaoFormato" class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                        Formato
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a onClick="selecionaFormato('CODY')">Programa Desktop Cody</a></li>
                        <li><a onClick="selecionaFormato('PDF')">PDF</a></li>
                        <li><a onClick="selecionaFormato('CVS')">CSV</a></li>
                    </ul>
                </div>
                <button id="btn-gerencInf" class="btn btn-success" style="margin-left:20px" onClick="exportarDados()" data-loading-text="Gerando..." href="#">Gerar</button>
            </div>
        </div>
    </div> 
    <div id="menuRevogar" class="row" style="display: none; margin-left: 40px">
        <h4>
           Gerenciar Administração de Instituições
        </h4>
        <p>Note que você <strong>revogando</strong> está tirando possibilidade do educador gerenciar as informações que pertence a instituição em questão. A qualquer momento você pode tornar válido a gerência das informações dele que há junto a Instituição</p>
        <div class="row">
            <div class="span12"  style="margin-left: 20px">
                <?= jTableStart('GerenciadorAdmin', 'GerenciadorAdmin', 'educadores/listaEducadores', '', 'educadores/atualizarRelacaoEducadorInstituicao', '', array('selecting')) ?>
                <?= jPanelAddID(true, true, true) ?>
                <?= jPanelAddCampo('edu_nome', 'Educador', '', '22%', false, false, false) ?>
                <?= jPanelAddCampo('cpf', 'Educador Nome', '', '22%', false, false, true) ?>
                <?= jPanelAddCampo('cnpj', 'Instituicao CPNJ', '', '22%', false, true, true) ?>
                <?= jPanelAddCampo('inst_nome', 'Instituição Nome', 'textarea', '26%', true, false, true) ?>
                <?= jPanelAddCampoValMulti('relacao_ativa', 'Relação', '', array('1' => 'Validado', '0' => 'Revogado'), '20%', true) ?>
                <?= jPanelAddData('dt_cadastro', 'Dt. Cadastro', '15%', 'dd-mm-yy', true, false, false) ?>
                <?= jTableEnd() ?>
            </div>
        </div>
    </div>
    <div id="menuTestes" class="row" style="display: none; margin-left: 40px">
        <h4>
           Gerenciar Testes
        </h4>
        <p>Note que você pode <strong>modificar</strong> informações dos tests, porém o <strong>CNPJ</strong>, <strong>CPF</strong>, <strong>Nome do Aluno</strong>, <strong>Nome da Mãe do Aluno</strong>, <strong>Data de Nascimento</strong> e nem <strong>nome </strong>e <strong>ano</strong> da turma não se é permitido modificar bem como a <strong>Data de ocorrência</strong>dos testes </p>
        <div class="span12">
            <div class="input-prepend">
              <span class="add-on">Aluno </span>
              <input class="span9" id="aluno_nome" type="text" placeholder="Aluno do Nome">
            </div>
            <br/>
            <div class="input-prepend">
              <span class="add-on">Turma </span>
              <input class="span9" id="turma_nome" type="text" placeholder="Nome da Turma">
            </div>
            <br/>
            <div class="input-prepend">
                <button class="btn" id="filtrar_testes" type="button">Filtrar</button>
                <button class="btn" id="limpar_testes" type="button">Limpar</button>
            </div>
        </div>
        <div class="row">
            <div class="span12"  style="margin-left: 20px">
                <?= jTableStart('GerenciadorTestes', 'GerenciadorTestes', 'administrar/listaTestes', '', 'administrar/atualizarTeste', '', array('selecting')) ?>
                <?= jPanelAddID(true, true, true) ?>
                <?= jPanelAddCampo('cnpj', 'CNPJ (Instituição)', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('cpf', 'CPF (Educador)', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('nome', 'Nome', '', '22%', true, false, true) ?>
                <?= jPanelAddCampo('nome_mae', 'Nom. Mãe', '', '22%', true, false, true) ?>
                <?= jPanelAddData('dt_nascimento','Dt.Nasc','10%','dd-mm-yy',true,false,true)?>
                <?= jPanelAddCampo('nome_turma', 'Turma', '', '22%', true, false, true) ?>
                <?= jPanelAddCampo('ano', 'Ano', '', '22%', true, false, true) ?>
                <?= jPanelAddData('dt_ocorrencia', 'Dt. Ocorrência', '22%', 'dd-mm-yy',true, false, true) ?>

                <?= jPanelAddCampo('arremesso_medicineball', 'Ano', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('corrida_vinte_metros', 'Ano', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('salto_em_distancia', 'Salto Distância', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('quadrado', 'Quadrado', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('minutos_esportivo', 'Min. Esportivo', '', '0%', false, false, true) ?>
                <?= jPanelAddCampoValMulti('minutos_esportivos_nove', 'Salt. Esportivo 9 Min.', '', array('1' => 'Sim', '0' => 'Não'), '0%', false,false,true) ?>
                <?= jPanelAddCampo('num_abdominais', 'Num. de Abdominais', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('imc', 'IMC', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('sentar_alcancar', 'Ano', '', '0%', false, false, true) ?>
                <?= jPanelAddCampoValMulti('sentar_alcancar_com_banco', 'Com banco', '', array('1' => 'Sim', '0' => 'Não'), '0%', false,false,true) ?>
                <?= jPanelAddCampo('minutos_saude', 'Min. Saúde', '', '0%', false, false, true) ?>
                <?= jPanelAddCampoValMulti('minutos_saude_nove', 'Min. Saúde 9 Min.', '', array('1' => 'Sim', '0' => 'Não'), '0%', false,false,true) ?>
                <?= jPanelAddCampo('massa_corporal', 'Massa Corporal', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('estatura', 'Estatura', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('envergadura', 'envergadura', '', '0%', false, false, true) ?>
                <?= jTableEnd() ?>
            </div>
        </div>
    </div>
    <div id="menuAlunos" class="row" style="display: none; margin-left: 40px">
        <h4>
           Gerenciar Alunos
        </h4>
        <p>Note que você pode <strong>modificar</strong> informações do alunos, porém o <strong>nome</strong>, <strong>nome da mãe</strong> e <strong>dt.nascimento</strong> não se é permitido modificar</p>
        <div class="row">
            <div class="span12"  style="margin-left: 20px">
                <?= jTableStart('GerenciadorAlunos', 'GerenciadorAlunos', 'administrar/listaAlunos', '', 'administrar/atualizarAluno', '', array('selecting')) ?>
                <?= jPanelAddID(true, true, true) ?>
                <?= jPanelAddCampo('nome', 'Nome', '', '22%', true, false, true) ?>
                <?= jPanelAddCampo('nome_mae', 'Nom. Mãe', '', '22%', true, false, true) ?>
                <?= jPanelAddData('dt_nascimento','Dt.Nasc','10%','dd-mm-yy',true,false,true)?>
                <?= jPanelAddCampo('cpf', 'CPF (Educador)', '', '0%', false, false, true) ?>
                <?= jPanelAddCampoValMulti('genero', 'Gênero', '', array('F' => 'Femenino', 'M' => 'Masculino'), '20%', true) ?>
                <?= jPanelAddCampo('endereco', 'Endereço', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('cpf_aluno', 'CPF', '', '0%', false, false, true) ?>
                <?= jPanelAddCampo('rg_aluno', 'RG', '', '0%', false, false, true) ?>
                <?= jTableEnd() ?>
            </div>
        </div>
    </div>
</div>

<script>
    
    $('#pessoa').attr('placeholder',"nome do educador");
    $('#pessoa').addClass('span6');
    $cpfEducador = '';
    $nomeEducador = '';
    $('#pessoa').val('');
    function cpfEscolhido($cpf,$nome){
        $('#nomeEducador').hide();
        $('#ComoEducador').hide();
        $('#menuSenha').css('display','none');
        $('#menuExportacao').css('display','none');
        $('#menuRevogar').css('display','none');
        $('#menuTestes').css('display','none');
        $('#menuAlunos').css('display','none');
        if( $cpf != '' || $nome != '' ){
            $cpfEducador=$cpf;
            cpf = $cpf;
            $nomeEducador = $nome;
            $('#nomeEducador').html('<h5>Como "'+$nome+'" você pode:<h5>');
            $('#nomeEducador').show();
            $('#ComoEducador').show();
            $('#divisor').show();
        }
     } 
     
    function mudarMenu($tabAtiva){
         $('#menu li').each(function(){
             $(this).removeClass('active');
         });
         $('#menuSenha').css('display','none');
         $('#menuExportacao').css('display','none');
         $('#menuRevogar').css('display','none');
         $('#menuTestes').css('display','none');
         $('#menuAlunos').css('display','none');
         switch($tabAtiva){
             case 'menuSenha':
                     $('#menuSenha').css('display','inline');
                     $('#senha').addClass('active');
                     break;
             case 'menuExportacao':
                     $('#menuExportacao').css('display','inline');
                     $('#exportacao').addClass('active');
                     $.ajax(
                    {
                        method: "post",
                        url: BASE_URL+"sistema/administrar/getInstituicoes/",
                        data: { cpf: $cpfEducador},
                        error: function(data){
                            $('#aviso_titulo').text('Ooops!');
                            $('#aviso_mensagem').text('Algo de muito errado aconteceu com nosso servidor :/ Atualize a página ou tente mais tarde.');
                            $('#aviso').modal('show');
                        },
                        success: function(data){
                            console.log(data);
                            resultado = JSON.parse(data);
                            $('#instituicoes_do_menu').html('');
                           
                            for(var $a=0; $a < resultado.length; $a++){
                                $('#instituicoes_do_menu').append("<li><a onClick=\"selecionaCNPJ('"+resultado[$a]['cnpj']+"','"+resultado[$a]['nome']+"')\">"+resultado[$a]['nome']+"</a></li>");
                            }
                        }
                    }); 
                     break;
             case 'menuRevogar':
                    $('#menuRevogar').css('display','inline');
                    $('#revogar').addClass('active');
                    $('#GerenciadorAdmin').jtable('load', {
                        cpf: $cpfEducador
                    });
                     break;
             case 'menuTestes':
                    $('#menuTestes').css('display','inline');
                    $('#testes').addClass('active');
                    $('#GerenciadorTestes').jtable('load', {
                        cpf: $cpfEducador
                    });
                     break;
             case 'menuAlunos':
                    $('#menuAlunos').css('display','inline');
                    $('#alunos').addClass('active');
                    $('#GerenciadorAlunos').jtable('load', {
                        cpf: $cpfEducador
                    });
                     break;
             default:
                 $('#home').css('display','inline');
         }
     }
     
    function gerarSenha(){
        $.ajax(
        {
            method: "post",
            url: BASE_URL+"sistema/administrar/gerarNovaSenha/",
            data: { cpf: $cpfEducador,nome:$nomeEducador},
            error: function(data){
                $('#aviso_titulo').text('Ooops!');
                $('#aviso_mensagem').text('Algo de muito errado aconteceu com nosso servidor :/ Atualize a página ou tente mais tarde.');
                $('#aviso').modal('show');
            },
            success: function(data){
                resultado = JSON.parse(data);
                $('#aviso_titulo').text(resultado.status);
                $('#aviso_mensagem').text(resultado.mensagem);
                $('#aviso').modal('show');
            }
        }); 
    }
    
    $('#filtrar_testes').click(function (e) {
        e.preventDefault();
        $('#GerenciadorTestes').jtable('load', {
            cpf: $cpfEducador,
            aluno_nome:$('#aluno_nome').val(),
            turma_nome:$('#turma_nome').val()
        });
    });
    
    $('#limpar_testes').click(function () {
         $('#turma_nome').val('');
         $('#aluno_nome').val('');
         $('#GerenciadorTestes').jtable('load', {
            cpf: $cpfEducador
        });
    });
    
   var cnpj = '';
   var formato = '';
   function selecionaCNPJ($cnpj,$nome){
       cnpj = $cnpj;
       $('#botaoInsti').html($nome+'<span class="caret"></span>');
   }
   
   function selecionaFormato($formato){
       formato = $formato;
       if($formato == 'CODY'){
           $('#msg').show('slide');
           $('#botaoFormato').html('Programa Desktop Cody<span class="caret"></span>');
       }else{
           $('#botaoFormato').html($formato+'<span class="caret"></span>');
           $('#msg').hide();
       }
   }
   
   function exportarDados(){
        if( cnpj == ''  || formato == '' ){
            $('#aviso_titulo').text('Atenção!');
            $('#aviso_mensagem').text('É necessário selecionar uma Instituição e um Formato para exportar');
            $('#aviso').modal('show');
        }else{
            var btn = $('#btn-gerencInf');
            btn.button('loading')
            setTimeout(function () {
                btn.button('reset');
            }, 3000);
            $.ajax(
            {
                method: "post",
                url: BASE_URL+"exportacao/exportarArquivo/",
                data: { cnpj: cnpj,formato:formato, cpf:$cpfEducador},
                error: function(data){
                    $('#mensagem').hide('slide');
                    $('#mensagem').removeClass('alert-success');
                    $('#mensagem').removeClass('alert-block');
                    $('#mensagem').addClass('alert-error');
                    $('#mensagem').html("<h4 class='alert-heading'>Ooops! Erro</h4>"+data);
                    $('#mensagem').show('slide');
                },
                success: function(data){
                    resultado = JSON.parse(data);
                    if(resultado.status == 'error'){
                        $('#mensagem').hide('slide');
                        $('#mensagem').removeClass('alert-success');
                        $('#mensagem').removeClass('alert-block');
                        $('#mensagem').addClass('alert-error');
                        $('#mensagem').html("<h4 class='alert-heading'>Ooops! Erro</h4>"+resultado.mensagem);
                        $('#mensagem').show('slide');
                    }else{
                        $('#mensagem').hide('slide');
                        window.open(resultado.mensagem,'Download');  
                    }
                }
            }); 
        }
   }
</script>
<?=footerView(); ?>
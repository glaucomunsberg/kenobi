<?php
class MensageiroModel extends CI_Model {

    /**
     * Retorna o número de pessoas
     *  que estão até então registrados
     * @return int 
     */
    function getTotalMensagens(){
        $retorno =  $this->db->get('mensagens');
        return $retorno->num_rows();
    }
    /**
     * Retorna todos os mensagens para a grid do jTable
     *  @param jtSorting - pelo que será ordenado ex: "descricao ASC"
     *  @param jtStartIndex - inicio do limit
     *  @param jtPageSize - fim da limit
     * @return type 
     */
    function listaMensagens($parametros,$get){
        $data = array();

        $data['TotalRecordCount'] = $this->getTotalMensagens();

        $this->db->trans_start();
            $this->db->select('origem_cpf, destino_cpf, mensagem, origem_tipo, destino_tipo,dt_cadastro',false);
            $this->db->from('mensagens');
            $this->db->where("origem_cpf = '".$this->session->userdata('cpf')."' or destino_cpf = '".$this->session->userdata('cpf')."'");
            if(!empty($parametros['name'])){
                $this->db->like('upper(event)', strtoupper($parametros['name']) );
            }

            if( !empty($get['jtSorting'])){
                $pieces = explode(" ", @$get['jtSorting']);
                $this->db->order_by($pieces[0],$pieces[1]);
            }

            if( @$get['jtStartIndex'] != ''  && @$get['jtPageSize'] != '' ){
                $this->db->limit($get['jtStartIndex']+','+$get['jtPageSize']);
            }

            $data['Records'] = $this->db->get()->result();
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }

        return $data;
    }
        /**
         * Insere uma nova history ao sistema
         * @param type $parametros
         * @return type 
         */
        function inserirMensagem($parametros){
            
            $this->db->trans_start();
                
                $this->db->set('mensagem', $parametros['mensagem']);
                $this->db->set('origem_cpf', $this->session->userdata('cpf'));
                $this->db->set('destino_cpf', '00000000000');
                $this->db->set('origem_tipo', 'Educador');
                $this->db->set('destino_tipo', 'Administrador');
                $this->db->insert('mensagens');
                
            $this->db->trans_complete();
            if($this->db->trans_status() === FALSE){
                $data['Result'] = "ERROR";
            }else{
                $data['Result'] = "OK";
            }
           
            $data['Record'] = $this->db->query('select * from mensagens ORDER BY dt_cadastro DESC limit 1')->result();
            return $data;
        }
}
?>
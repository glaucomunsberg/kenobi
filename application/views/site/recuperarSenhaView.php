<?=headerGlobalView();?> 
<div id="total" class="container">
    <div id="menuGeral" data-spy="affix-top" data-offset-top="0"  class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse" >
                    <ul class="nav">
                        <li class=""><a href="<?=BASE_URL?>" ><i class="icon-home"></i>Início</a></li>
                        <li class=""><a href="<?=BASE_URL.'login'?>" >Sistema de Login</a></li>
                        <li class=""><a target="_blank" href="<?=BASE_URL.'site/help'?>" >Central de Ajuda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 50px">
        <div class="span9">
            <h1><?=lang('proDown')?> <small><?=lang('kenobiSistema')?></small></h1>
        </div>
        <div class="span3 disabled">
            
        </div>
    </div>
    <div class="row">
        <div class="span12">
            <p>Para recuperar a sua senha você deve lembrar de seu <strong>email</strong> e <strong>CPF</strong> usado no sistema, automaticamente enviaremos uma nova senha para seu email.</p>
        </div>
    </div>
    <div class="row">
        <div class="span6">
            
            <div class="control-group">
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-list-alt"></i></span></span>
                    <input class="span3" id="cpf" name="cpf" type="text" placeholder="CPF">
                </div>
                <div class="input-prepend">
                    <span class="add-on"><i class="icon-envelope"></i></span>
                    <input class="span3" id="email" name="text" type="text" placeholder="E-mail">
                </div>
                <div>
                    <?php echo form_submit('submit', 'Recuperar','data-loading-text="Aguarde..." id="submit" class="btn btn-success" style="width: 88px" onClick="recuperarSenha()"');?>
                </div>
            </div>
        </div> 
        <div class="span6">
            <div id="mensagemCaixa" style="display: none" class="hero-unit">
                <h3 id="mensagemTitulo">Atenção</h3>
                <p id="mensagem"></p>
            </div>
        </div>
    </div>
</div>
<script>
     $('#total').css('height',$(document).height()-208);
     function recuperarSenha(){
        if($('#email').val() == '' || $('#cpf').val() == ''){
            $('#mensagemCaixa').hide('slow');
            $('#mensagemTitulo').html('Atenção');
            $('#mensagem').html('Digite seu email e seu CPF');
            $('#mensagemCaixa').show('slow');
        }else{
           $('#submit').button('loading');
           $.post("<?=BASE_URL.'sistema/login/recuperarSenha'?>", 
            {
                cpf:$('#cpf').val(),
                email:$('#email').val()
            },
            function(data) {
                $('#submit').button('reset');
                var resposta = JSON.parse(data);
                $('#mensagemCaixa').hide();
                $('#mensagemTitulo').html(resposta.titulo);
                $('#mensagem').html(resposta.mensagem);
                $('#mensagemCaixa').show('slow');
                if(resposta.titulo == 'sucesso'){
                    $('#email').val('');
                    $('#cpf').val('');
                }
            });
        }
        }
        
     
</script>
<?=footerView(); ?>

<h2>Normas de Uso do Sistema</h2>
<h4>Não é Permitido:</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    1) Utilizar o material contido no site e/ou sistema para fins de calúnia e difamação;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    2) Utilizar o material contido neste site e/ou sistema para fins lucrativos;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    3) Fazer mirrors ou vender copias do site em mídia eletrônica sem consentimento do autor do site;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    4) Utilizar-se do sistema e das informações aqui contidas para o mal;
</p >
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    5) Utilizar-se das publicações divulgadas neste site para fins de referências, monográfias, 
    teses e outros fins acadêmicos sem dar crédito aos referidos autores;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    6) Não é permitido ao usuário do sistema divulgar ou comercializar dados pessoais ou expor     
os indivíduos aos quais possui informações; 
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    7) Não é permitido ao usuário transferir seu usuário e senha de acesso
    a terceiros;
</p>
<h4>Privacidade das Informações:</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    1) Não distribuimos ou comercializamos nenhum tipo de dados pessoais
    de indivíduos cadastradas no sistema;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    2) O Administrador do sistema tem a total liberdade de quem pode ou não ter acesso ao sistema;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    3) O Administrador tem a liberdade de colocar ou retirar o sistema em funcionamento,
    bem como alterar as publicações divulgadas no site;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    4) Cada educador cadastrado tem acesso somente a dados de suas turmas,
não sendo possível obter informações alheias;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    5) Os dados aqui obtidos são utilizados apenas para fins de pesquisa,
    não serão vendidos ou comercializados;
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    6) O uso indevido ou inadequado do sistema inplicará na suspensão de acesso ao mesmo.
</p>
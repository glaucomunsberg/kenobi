<h2>Como se cadastrar no Sistema</h2>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Aqui você terá alguma ajuda a respeito do cadastro no sistema web para enviar suas avaliações e obter informações e relatórios sobre eles.
</p>
<div class ="row-fluid">
    <div class="span8">
        <p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
            Para pode usar o sistema você deve efetuar um pré-cadastro. Esse pré-cadastro será avaliado pelo Administrador do sistema.
        </p>
        <p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
            O Administrador irá decidir se você pode ou não ter acesso ao sistema. Você
            receberá um e-mail notificando se você pode ou não acessar o sistema.
        </p> 
    </div>
    <div class="span4">
        <a href="#" class="thumbnail" style='margin-bottom: 40px'>
            <img src="<?=IMG.'ajuda/tela_cadastro.png'?>">
        </a>
    </div>    
</div>
<h4>Efetuando o pré cadastramento</h4>
<div class ="row-fluid">
    <div class="span8">
        <p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
            Para efetuar o pré cadastramento você deve preencher o formulário de inscrição. 
            Para acessar o formulário basta clicar em "Inscreva-se Já".
        </p>
        <p>
            <div class="alert alert-info" style='margin-bottom: 40px'>
                <strong><i class="icon-info-sign"></i>Info</strong> Disponível na<a class="btn btn-link" href="<?=BASE_URL.'site/'?>">Início</a> do site.
            </div>
        </p>
    </div>
    <div class="span4">
        <a href="#" class="thumbnail" style='margin-bottom: 40px'>
            <img src="<?=IMG.'ajuda/cadastrar.png'?>">
        </a>
    </div>    
</div>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    Preencha os dados de acordo com o que se pede no formulário, envie
    ao Administrador e aguarde nossa resposta!
</p>
<h4>Acesso ao Sistema</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 120px'>
    Após receber o e-mail de confirmação de acesso ao sistema é só logar
    com seu usuário e senha e desfrutar do sistema! 
</p>
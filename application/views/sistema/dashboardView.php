<?=headerGlobalView();?>
<div id="total" class="container">
    <div class="row-fluid">
        <div class="page-header">
            <h1><?=lang('proDown')?> <small><?=lang('kenobiSistema')?></small></h1>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
            <div class="well" style="padding: 8px 0;">
                <ul class="nav nav-list">
                  <li><a href="<?=BASE_URL.'sistema/areaRestrita'?>" ><i class="icon-home"></i>Início</a></li>
                  <li class="nav-header"> <?=lang('menuPrivadoEducador')?></li>
                  <li><a href="#enviar" onClick="mudarTela('enviar')"><i class="icon-upload"></i> <?=lang('menuPrivadoEnviarAvaliacoes')?></a></li>
                  <li><a href="#resultados" onClick="mudarTela('resultados')"><i class="icon-list"></i> <?=lang('menuPrivadoVerResultado')?></a></li>
                  <li><a href="#exportar" onClick="mudarTela('exportar')"><i class=" icon-download"></i> <?=lang('menuPrivadoExportar')?></a></li>
                  <li><a href="#programa" onClick="mudarTela('programa')"><i class="icon-download-alt"></i> <?=lang('menuPrivadoProgramaDesktop')?></a></li>
                  <?php
                   if(@$usuario->tipo_usuario == 'A'){
                      ?>
                        <li class="nav-header"><?=lang('menuPrivadoAdministrador')?></li>
                        <li><a href="#gerenciarInfo" onClick="mudarTela('gerenciarInfo')"><i class="icon-edit"></i> <?=lang('menuPrivadoModificarInf')?></a></li>
                        <li><a href="#gerenciarEducadores" onClick="mudarTela('gerenciarEducadores')"><i class="icon-eye-close"></i> <?=lang('menuPrivadoGerenciarUser')?></a></li>
                        <li><a href="#configuracao" onClick="mudarTela('configuracao')"><i class="icon-wrench"></i> <?=lang('menuPrivadoConfig')?></a></li>
                      <?php
                   }
                  ?>
                  <li class="divider"></li>
                  <li><a href="#mensageiro" onClick="mudarTela('notificar')"><i class="icon-envelope"></i> <?=lang('menuPrivadoNotificarAdmin')?></a></li>
                  <li><a href="#conta" onClick="mudarTela('conta')"><i class="icon-user"></i> <?=lang('menuPrivadoConta')?></a></li>
                  <li><a href="<?=BASE_URL.'site/help'?>" target="_blank"><i class="icon-flag"></i> <?=lang('menuPrivadoAjuda')?></a></li>
                  <li><a href="<?=BASE_URL.'sistema/login/logoff'?>"><i class="icon-off"></i><?=lang('menuPrivadoSair')?></a></li>
                </ul>
              </div>
        </div>
        <div id="tabs" class="span9">
            <div id="home" style=" ">
                <?=$this->load->view('sistema/menuHomeView')?>
            </div>
            <div id="enviar" style="display: none">
                <?=$this->load->view('sistema/menuImportarArquivoView')?>
            </div>
            <div id="resultados" style="display: none">
                <?=$this->load->view('sistema/menuVerResultadosView')?>
            </div>
            <div id="programa" style="display: none">
                <?=$this->load->view('sistema/menuDownloadProgramaView')?>
            </div>
            <div id="exportar" style="display: none">
                <?=$this->load->view('sistema/menuExportarArquivoView')?>
            </div>
            <div id="conta" style="display: none">
                <?=$this->load->view('sistema/menuContaView')?>
            </div>
            <?php
               if(@$usuario->tipo_usuario == 'A'){
                  ?>
                    <div id="configuracao" style="display: none">
                        <?=$this->load->view('sistema/menuGerenciarConfiguracaoView')?>
                    </div>
                    <div id="notificar" style="display: none">
                        <?=$this->load->view('sistema/menuNotificarEducadorView')?>
                    </div>
                    <div id="gerenciarEducadores" style="display: none">
                        <?=$this->load->view('sistema/menuGerenciarUsuariosView')?>
                    </div>                    
                  <?php
               }
              ?>
            
            </div>
        </div>
    </div>
</div>
<script>
 $('#total').css('height',$(document).height()-158);
 function mudarTela($tabAtiva){
     $('#enviar').css('display','none');
     $('#programa').css('display','none');
     $('#home').css('display','none');
     $('#resultados').css('display','none');
     $('#exportar').css('display','none');
     $('#notificar').css('display','none');
     $('#conta').css('display','none');
     $('#configuracao').css('display','none');
     $('#gerenciarEducadores').css('display','none');
     $('#gerenciarInfo').css('display','none');
     switch($tabAtiva){
         case 'enviar':
                 $('#enviar').css('display','inline');
                 break;
         case 'programa':
                $('#programa').css('display','inline');
                 break;
         case 'resultados':
                $('#resultados').css('display','inline');
                 break;
         case 'exportar':
                $('#exportar').css('display','inline');
                 break;
         case 'notificar':
                $('#notificar').css('display','inline');
                 break;
         case 'conta':
                $('#conta').css('display','inline');
                 break;
         case 'gerenciarEducadores':
                $('#gerenciarEducadores').css('display','inline');
                 break;
         case 'configuracao':
                $('#configuracao').css('display','inline');
                 break;
         case 'gerenciarInfo':
                 window.location.href = "<?=BASE_URL.'sistema/administrar'?>";
                 break;
         default:
             $('#home').css('display','inline');
     }
 }
 
 if(window.location.hash) {
      var hash = window.location.hash.substring(1); //Retorna a primeira substring
      mudarTela(hash);
  }

</script>

<?=footerView(); ?>

<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Administrar extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('sistema/loginmodel', 'loginModel');
        $this->load->model('sistema/emailModel','emailModel');
        $this->load->model('sistema/configuracaoModel','configuracaoModel');
        $this->load->model('sistema/loginModel','loginModel');
        $this->load->model('educadorModel','educadoresModel');
        $this->load->model('turmaalunosModel','turmaAlunosModel');
        $this->load->model('alunosModel','alunosModel');
        $this->load->model('testesModel','testesModel');
        $this->load->model('instituicaoModel','instituicaoModel');
        
        $this->loginModel->logged();
    }
    function index(){
        $this->loginModel->adminLogged();
        $data['instituicoes'] = $this->instituicaoModel->getInstituicoesByEducador($this->session->userdata('cpf'));
        $data['educadores'] = $this->educadoresModel->getTodosEducadoresParaLista();
        $this->load->view('sistema/menuGerenciarInformacoesView',$data);
    }
    public function gerarNovaSenha(){
        $this->loginModel->adminLogged();
        if($this->educadoresModel->existeEducador($_POST['cpf'])){
            
            $educador = $this->educadoresModel->getEducador($_POST['cpf']);
            $educador = $educador[0];
            
            $novaSenha = random_string('alnum', 8);
            $prodown_email = $this->configuracaoModel->getEmail();
            $prodown_senha = $this->configuracaoModel->getSenha();

            if($this->loginModel->atualizarSenha($educador->cpf,$novaSenha)){
                $this->emailModel->enviarNovaSenha($prodown_email,$prodown_senha,$educador->nome,$educador->email,$novaSenha);
                print json_encode( array('status'=>'success','mensagem'=>"Nova senha enviada com sucesso para o educador ".$educador->nome.' para o email '.$educador->email));
            }else{
                print json_encode( array('status'=>'error',"Não foi possível trocar a senha do ".$educador->nome));
            }
            
        }else{
            print json_encode( array('status'=>'error','mensagem'=>"O CPF ".$_POST['cpf']." não existe"));
        }
        
    }
    
    public function enviarArquivo(){
        $data['usuario'] = $this->loginModel->getUsuarioLogged();
        $data['turmas'] = $this->turmasModel->getTurmaByProfessor($data['usuario']['cpf']);
        $this->load->view('sistema/enviarArquivoView',$data);
    }
    
    public function getInstituicoes(){
        log_message('ERROR', $_POST['cpf']);
        $retorno = $this->instituicaoModel->getInstituicoesByEducadorArray($_POST['cpf']);
        print json_encode($retorno);
    }
    
    public function listaUsuarios(){
        print json_encode($this->loginModel->listaUsuarios($_POST,$_GET));
    }
    
    public function atualizarUsuario(){
        $this->loginModel->adminLogged();
        $educador = $this->educadoresModel->getEducador($_POST['cpf']);
        $educador = $educador[0];
        print json_encode($this->loginModel->atualizarAtividoUsuario($_POST));
        if($_POST['usuario_ativo'] == 1){
            $prodown_email = $this->configuracaoModel->getEmail();
            $prodown_senha = $this->configuracaoModel->getSenha();
            $this->emailModel->enviarLiberacaoDeAcesso($prodown_email,$prodown_senha,$educador->nome,$educador->email);
        }
        
    }

    public function listaAlunos(){
        print json_encode($this->alunosModel->listaAlunosByEducador($_POST,$_GET));
    }
    
    public function atualizarAluno(){
        $this->loginModel->adminLogged();
        print json_encode($this->alunosModel->atualizarAlunoByAdmin($_POST));
    }
    public function excluirAluno(){
        print json_encode($this->alunosModel->excluirAluno($_POST));
    }
    public function listaTestes(){
        print json_encode($this->testesModel->listaTestesByEducador($_POST,$_GET));
    }
    
    public function atualizarTeste(){
        $this->loginModel->adminLogged();
        print json_encode($this->testesModel->atualizarTesteByAdmin($_POST)); 
    }
    
}
?>
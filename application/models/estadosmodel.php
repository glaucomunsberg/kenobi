<?php

class EstadosModel extends CI_Model{
    
    function getEstados(){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('estados');
        $this->db->trans_complete();
        
        return $this->db->get()->result();
        
    }
    
    function getEstado($uf , $estado){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('estados');
            $this->db->where('uf',$uf);
            $this->db->where('estado',$estado);
        $this->db->trans_complete();
        return $this->db->get()->result();
    }
    
}
?>

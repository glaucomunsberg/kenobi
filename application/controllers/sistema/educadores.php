<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Educadores extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('educadormodel', 'educadorModel');
        $this->load->model('sistema/loginmodel', 'loginModel');
        $this->load->model('sistema/configuracaomodel','configuracaoModel');
        $this->load->model('sistema/emailmodel','emailModel');
        $this->loginModel->logged();
    }
    
    
    public function listaEducadores(){
        print json_encode($this->educadorModel->listaEducadores($_POST,$_GET));
    }
    
    public function atualizarRelacaoEducadorInstituicao(){
        $educador = $this->educadorModel->getEducador($_POST['cpf']);
        $educador = $educador[0];
        $nome_escola =$_POST['inst_nome'];
        if($_POST['relacao_ativa'] == 1){
            $prodown_email = $this->configuracaoModel->getEmail();
            $prodown_senha = $this->configuracaoModel->getSenha();
            $this->emailModel->enviarLiberacaoDeAcessoInstituicao($prodown_email,$prodown_senha,$educador->nome,$educador->email,$nome_escola);
        }
        print json_encode($this->educadorModel->atualizarRelacao($_POST));
    }
}
?>
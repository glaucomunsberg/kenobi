<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
         $this->load->model('sistema/loginmodel', 'loginModel');
         $this->load->model('educadorModel','educadorModel');
         $this->load->model('cidadesmodel', 'cidadesModel');
         $this->load->model('sistema/emailModel','emailModel');
         $this->load->model('sistema/configuracaoModel','configuracaoModel');
    }

    function index() {

        // VALIDATION RULES
        $this->form_validation->set_rules('cpf', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_error_delimiters('<p class="error">', '</p>');


        // MODELO MEMBERSHIP
       
        $query = $this->loginModel->validate();

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('sistema/loginView');
        } else {
            if ($query) { // VERIFICA LOGIN E SENHA
                $pos = strrpos($this->input->post('cpf'), '@');
                if ($pos === false) { 
                    $cpf = $this->input->post('cpf');
                }else{
                    $cpf = @$this->educadorModel->getEducadorByEmail($this->input->post('cpf'),$this->input->post('password'))->cpf;
                }
                $data = array(
                    
                    'cpf' => $cpf,
                    'logged' => true
                );
                $this->session->set_userdata($data);
                header("Location: ".BASE_URL.'sistema/areaRestrita');
                die();
                
            } else {
                if($this->educadorModel->existeEducador($this->input->post('cpf'))){
                    $data['error']='Acesso a esse educador não é permitido';
                }else{
                    $data['error']='Senha ou usuário não cadastrado';
                }
                $this->load->view('sistema/loginView',$data);
            }
        }
    }
    
    function atualizarUsuario(){
        print $this->loginModel->atualizarUsuario($_POST);
    }
    
    function cadastro(){
        $data['cidades']=$this->cidadesModel->getTodasCidades();
        $this->load->view('sistema/cadastroView',$data);
    }
    
    function recuperarSenha(){
        
        if($this->loginModel->isUsuario($_POST)){
            
            $educador = $this->educadorModel->getEducador($_POST['cpf']);
            $educador = $educador[0];
            
            $novaSenha = random_string('alnum', 8);
            $prodown_email = $this->configuracaoModel->getEmail();
            $prodown_senha = $this->configuracaoModel->getSenha();

            if($this->loginModel->atualizarSenha($educador->cpf,$novaSenha)){
                $this->emailModel->enviarNovaSenha($prodown_email,$prodown_senha,$educador->nome,$educador->email,$novaSenha);
                print json_encode( array('titulo'=>'Sucesso','mensagem'=>"Nova senha enviada com sucesso para o educador ".$educador->nome.' para o email '.$educador->email));
            }else{
                print json_encode( array('titulo'=>'error',"Não foi possível trocar sua senha. Tente mais tarde"));
            }
            
        }else{
            print json_encode( array('titulo'=>'Error', 'mensagem'=>'A combinação de CPF e Email não são válidos no sistema, por favor tente novamente ou entre em contato com o administrador.') );
        }
    }
    function logoff(){
        $this->session->sess_destroy();
        header("Location: ".BASE_URL);
    }
}

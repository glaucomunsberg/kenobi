<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AreaRestrita extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('sistema/loginmodel', 'loginModel');
        $this->load->model('turmasModel','turmasModel');
        $this->load->model('alunosModel','alunosModel');
        $this->load->model('turmaAlunosModel','turmaAlunosModel');
        $this->load->model('instituicaoModel','instituicaoModel');
        $this->load->model('educadorModel','educadoresModel');
        $this->load->model('testesModel','testesModel');
        $this->loginModel->logged();
    }
    
    public function index() {
        $data['turmas'] = $this->turmasModel->getTurmaByProfessor($this->session->userdata('cpf'));
        $data['alunos'] = $this->alunosModel->getAlunosByEducador($this->session->userdata('cpf'));
        $data['num_alunos_femininos'] = $this->turmaAlunosModel->getNumAlunoGeneroByEducador($this->session->userdata('cpf'), 'F');
        $data['num_alunos_masculino'] = $this->turmaAlunosModel->getNumAlunoGeneroByEducador($this->session->userdata('cpf'), 'M');
        $data['melhor_corredor_feminino'] = $this->testesModel->getMelhorCorredorPorGenero($this->session->userdata('cpf'), 'F');
        $data['melhor_corredor_masculino'] = $this->testesModel->getMelhorCorredorPorGenero($this->session->userdata('cpf'), 'M');
        $data['melhor_desempenho_geral']= $this->testesModel->getMelhorAlunoDesempenhoGeral($this->session->userdata('cpf'));
        $data['media_por_turmas'] = $this->testesModel->getMediaTurmasByEducador($this->session->userdata('cpf'));
        
        $data['instituicoes'] = $this->instituicaoModel->getInstituicoesByEducador($this->session->userdata('cpf'));
        $data['usuario'] = $this->loginModel->getUsuarioLogged();
        $this->load->view('sistema/dashboardView',$data);
    }
    
    public function administrar(){
        $this->loginModel->adminLogged();
        $data['educadores'] = $this->educadoresModel->getTodosEducadoresParaLista();
        
    }
    
    public function enviarArquivo(){
        $data['usuario'] = $this->loginModel->getUsuarioLogged();
        $data['turmas'] = $this->turmasModel->getTurmaByProfessor($data['usuario']['cpf']);
        $this->load->view('sistema/enviarArquivoView',$data);
    }
    
}
?>
<?php
	
	class TurmasModel extends CI_Model{
		
		#retorna todas as turmas existentes no banco
		function getTurmas(){
			$this->db->trans_start();
				$this->db->select('*');
				$this->db->from('turmas');
			$this->db->trans_complete();
		
			return $this->db->get()->result();	
						
			}
			
			
		#retorna todas as turmas de uma instituicao especifica
		function getTurmaByInstituicao($cnpj){
			$this->db->trans_start();
				$this->db->select('nome_turma');
				$this->db->from('turmas');
				$this->db->where('cnpj',$cnpj);	
			$this->db->trans_complete();
			
			return $this->db->get()->result();
				
			}
		
		#retorna true, se a turma existe na instituicao
                        #antigo isTurma
		function existeTurmaNaInstituicao($cnpj, $nomeTurma){
			$this->db->trans_start();
				$this->db->select('*');
				$this->db->from('turmas');
				$this->db->where('cnpj', $cnpj);
                                $this->db->where('nome_turma', $nomeTurma);
                                $this->db->where('turma_ativa', 1);
			$this->db->trans_complete();
			
			if($this->db->get()->num_rows() > 0 ){
				return true;
			}else{
				return false;
			}
						
		}
		
		#retorna turmas ativas de um professor especificos 
		function getTurmaByProfessor($cpf){
			$this->db->trans_start();
				$this->db->select('i.cnpj, t.cpf, i.nome as nome_instituicao, t.nome_turma, t.ano, t.dt_cadastro');
				$this->db->from('turmas as t');
                                $this->db->join('instituicoes as i','i.cnpj = t.cnpj');
                                $this->db->where('t.cpf', $cpf);
                                $this->db->where('t.turma_ativa', 1);
			$this->db->trans_complete();

			return $this->db->get()->result();			
		}
		
		#retorna turmas de um professor especifico em um determinada instituicao
                #esse tipo de função deve conferir se a turma está em 'funcionamento' = ativa
                #Incidente #39
		function getTurmaByProfessorInstituicao($cnpj, $cpf){
			$this->db->trans_start();
				$this->db->select('*');
				$this->db->from('turmas');
				$this->db->where('cnpj', $cnpj);
                                $this->db->where('cpf', $cpf);
                                //$this->db->where('turma_ativa', 1);
			$this->db->trans_complete();
			
			return $this->db->get()->result_array();
			
		}
		
		#retorna todas as turmas de um determinado ano por instituição (quantas turmas
                #tem em determinada instituição)
		function getTurmasByInstituicaoANo($cnpj, $ano){
			$this->db->trans_start();
				$this->db->select('*');
				$this->db->from('turmas');
                                $this->db->where('cnpj',$cnpj);
                                $this->db->where('ano',$ano);
                                $this->db->where('turma_ativa', 1);
                                
			$this->db->trans_complete();
			
			return $this->db->get()->result();
			
		}
		
		
       #retorna  se a turma  tal existe
       function existeTurma($cnpj, $cpf, $turmaNome, $turmaAno) {
           $this->db->trans_start();
				$this->db->select('*');
				$this->db->from('turmas');
				$this->db->where('cnpj', $cnpj);
                                $this->db->where('cpf', $cpf);
                                $this->db->where('nome_turma', $turmaNome);
                                $this->db->where('ano', $turmaAno);                              
	  $this->db->trans_complete();
			
          
          if($this->db->get()->num_rows() > 0 ){
				return true;
			}else{
				return false;
			}
         
                }
                
     #retorna turmas da instituicao que nao estão mais ativas
	function turmasFechadas($cnpj){
          $this->db->trans_start();
                $this->db->select('*');
                $this->db->from('turmas');
                $this->db->where('cnpj', $cnpj);
                $this->db->where('turma_ativa', 0);
          $this->db->trans_complete();
         return $this->db->get()->result();  
                
                
	}
        
		
	#inserir turma nova
	function inserirTurma($cnpj,$cpf,$turma){
		$this->db->trans_start();
			$this->db->set('cnpj', $cnpj);
			$this->db->set('cpf', $cpf);
			$this->db->set('nome_turma', $turma['nome_turma']);
			$this->db->set('ano', $turma['ano']);
			$this->db->set('turma_ativa', 1);
			$this->db->insert('turmas');		
		$this->db->trans_complete();
		
                if($this->db->trans_status() === FALSE){
			return false;
		}else{
			return true;
		}		
	}
	
		
	
#setar turma como fechada
	function setTurmaFechada($cnpj, $cpf, $turmaNome, $turmaAno){
		$this->db->trans_start();
			$this->db->set('turma_ativa', 0);
			$this->db->where('cnpj', $cnpj);
			$this->db->where('cpf', $cpf);
			$this->db->where('nome_turma', $turmaNome);
				$this->db->where('ano', $turmaAno);	
			$this->db->update('turmas');
		$this->db->trans_complete();
		
		if($this->db->trans_status() === FALSE){
			return false;
		}else{
			return true;
		}
		
	}		
		
	
		}

?>

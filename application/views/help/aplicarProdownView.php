<h2>Como Aplicar o ProDown</h2>
<p style="text-align: justify; text-indent: 20px;margin-bottom: 20px">
    O Projeto ProDown pode ser aplicado por qualquer professor em qualquer parte do Brasil. Os professores recebem os relatórios
    de desempenho de seus alunos após submeterem os dados no sistema.
</p>
<div class="alert alert-info" style='margin-bottom: 20px'>
    <strong><i class="icon-info-sign"></i>Info</strong> Antes de aplicar os testes leia o Manual ProDown 2013. <a>Em construção :).</a>
</div>
<h4>Guia de aplicação do ProDown 2013:</h4>
<p  style="text-align: justify; text-indent: 20px;">
    1º)	Visualize e/ou salve os textos, exemplos de aquecimento e vídeos da na seção passo a passo;
</p>
<p  style="text-align: justify; text-indent: 20px;">
    2º) Salve a <a  href="<?=BASE_URL. 'doc/Ficha-Avaliacao-PRODOWN.doc'?>">ficha de avaliação </a> sugerida pelo ProDown;
</p>
<p  style="text-align: justify; text-indent: 20px;">
    3º)	Reúna o material necessário e monte seu cronograma de avaliação;
</p>
<p  style="text-align: justify; text-indent: 20px;">
    4º)	Organize os testes a serem realizados;
</p>
<p  style="text-align: justify; text-indent: 20px;">
    5º)     Aplique a bateria de testes, conforme a indicação na página bateria de testes, registrando os dados na ficha de avaliação de cada aluno;
</p>
<p  style="text-align: justify; text-indent: 20px;">
    6º)	Baixe o Programa de Digitação do ProDown. Salve também o guia do programa de digitação que simula passo a passo como você deve registrar e enviar os dados;
</p>
<p  style="text-align: justify; text-indent: 20px;">
    7º)	Envie o(s) arquivo(s) das avaliações para o ProDown;
</p>
<p  style="text-align: justify; text-indent: 20px;">
    8º)	Visualize seus relatórios e se preferir, imprima-os;
</p>
<p  style="text-align: justify; text-indent: 20px;margin-bottom: 20px">
    9º)	Utilize as informações para acompanhar o desenvolvimento de cada aluno, assim como adequar suas atividades às reais necessidades de suas turmas;
</p>
<p  style="text-align: justify; text-indent: 20px">
    <p>É possível visualizar vídeos explicativos sobre os testes:<a class="btn btn-primary btn-link" href="<?=BASE_URL .'site/help#videos'?>">Clique Aqui.</a></p>
</p>





   

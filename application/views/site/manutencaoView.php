<?=headerGlobalView();?> 
<div id="total" class="container">
    <div id="menuGeral" data-spy="affix-top" data-offset-top="0"  class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse" >
                    <ul class="nav">
                        <li class=""><a href="<?=BASE_URL?>" ><i class="icon-home"></i>Início</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 50px">
        <div class="span9">
            <h1><?=lang('proDown')?> <small><?=lang('kenobiSistema')?></small></h1>
        </div>
        <div class="span3 disabled">
            
        </div>
    </div>
    <div class="row">
        <div class="span12">
           <div class="hero-unit">
                <h2>Em Manutenção <small></small></h2>
                <p style="text-align: justify; text-indent: 40px;margin-top: 10px"> Lamentamos, mas estamos em manutenção. Enquanto os nossos técnicos solucionam o problema o login e o conteúdo não estarão disponíveis para o acesso. Aguarde até que nossos técnicos solucionem o problema encontrado</p>
           </div>
        </div>       
    </div>
</div>
<script>
     $('#total').css('height',$(document).height()-208);
</script>
<?=footerView(); ?>

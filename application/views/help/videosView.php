<h2>Vídeos</h2>

<div class ="row-fluid">
    <div class="span5">
        <p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
            Aqui você pode ver vídeos que ensinam como realizar os testes!  
        </p>
        <p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
            Antes de realizar os testes faça um aquecimento com seus alunos, como por exemplo:
        </p> 
    </div>
    <div class="span7">
        <a href="#" class="thumbnail" style='margin-bottom: 40px'>
            <img src="<?=IMG.'ajuda/exercicios-de-Aquecimento.jpg'?>">
        </a>
    </div>    
</div>
<h3>Testes da Aptidão Física para a Saúde</h3>
<h4>Estatura</h4>
    <iframe width="500" height="375" src="//www.youtube.com/embed/YuE9_PfZ9B0" frameborder="0" allowfullscreen></iframe>
<h4>Índice de massa corporal <small>IMC</small></h4>
    <iframe width="500" height="375" src="//www.youtube.com/embed/fT1t464u-ZE" frameborder="0" allowfullscreen></iframe>
<h4>Envergadura</h4>
    <iframe width="500" height="375" src="//www.youtube.com/embed/MER-1tpJqAA" frameborder="0" allowfullscreen></iframe>
<h4>Flexibilidade <small>Sentar-e-Alcançar</small></h4>
    <iframe width="500" height="375" src="//www.youtube.com/embed/uZR4L9Kzhr4" frameborder="0" allowfullscreen></iframe>
<h4>Força-Resistência <small>Abdominal</small></h4>
    <iframe width="500" height="375" src="//www.youtube.com/embed/qKv2DBgtPHM" frameborder="0" allowfullscreen></iframe>

<h3>Testes De Desempenho Motor</h3> 
<h4>Força explosiva de membros inferiores <small>Salto horizotal</small></h4>
        <iframe width="500" height="375" src="//www.youtube.com/embed/yVYzwEq7Vss" frameborder="0" allowfullscreen></iframe>
<h4>Força explosiva de membros superiores <small>Arremesso Medicinebal</small></h4>
        <iframe width="500" height="375" src="//www.youtube.com/embed/NI-ad2Y_qSM" frameborder="0" allowfullscreen></iframe>
<h4>Agilidade <small>Teste do quadrado</small></h4>
        <iframe width="500" height="375" src="//www.youtube.com/embed/HcfvJxrM-xY" frameborder="0" allowfullscreen></iframe>
<h4>Velocidade de deslocamento <small>Corrida de 20m</small></h4>
        <iframe width="500" height="375" src="//www.youtube.com/embed/-_wA3GE1lPY" frameborder="0" allowfullscreen></iframe>
<h4>Resistência Geral <small>9min</small></h4>
        <iframe width="500" height="375" src="//www.youtube.com/embed/B9uRtwADDyQ" frameborder="0" allowfullscreen></iframe>
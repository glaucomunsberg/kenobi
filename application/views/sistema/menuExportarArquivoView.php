<h2><?=lang('exportarArquivo')?><small><?=lang('exportarArquivoDef')?></small></h2>
<h2></h2>
<p style="text-align: justify"><?=lang('exportarArquivoExplanacao')?></p>
<h2></h2>
<ul class="thumbnails">
    <li class="span12">
      <a href="#" class="thumbnail">
        <img src="<?=IMG.'cody/importando.png'?>" alt="">
      </a>
    </li>   
</ul>
<h1></h1>
<p>
    <h4><?=lang('exportarFormato')?></h4>
    <?=lang('exportarArquivoComoFazer')?>
</p>       
<p>
    <div class="btn-toolbar">
        <div class="btn-group">
         
          <a id="botaoInsti" class="btn dropdown-toggle" data-toggle="dropdown" href="#">
              Instituições
              <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
              <?php
                  if($instituicoes == ''){
                      echo '<li><a>Você não tem Instituições Vinculada</a></li>';
                  }else{
                    foreach ($instituicoes as $instituicao){
                        echo '<li><a onClick="selecionaCNPJ(\''.$instituicao->cnpj.'\',\''.$instituicao->nome.'\')">'.$instituicao->nome.'</a></li>';
                    }  
                  }
              ?>
          </ul>
        </div>
        <div class="btn-group">
            <a id="botaoFormato" class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                Formatos
                <span class="caret"></span>
            </a>
          <ul class="dropdown-menu">
            <li><a onClick="selecionaFormato('CODY')">Programa Desktop Cody</a></li>
            <li><a onClick="selecionaFormato('PDF')">PDF</a></li>
          </ul>
        </div>
        <button id="btn-gerarEdu" class="btn btn-success" style="margin-left:20px" onClick="exportarDados()" href="#">Gerar</button>
    </div>
    <div id="msg" class="alert alert-block"  style="display: none">
      <h4 class="alert-heading">Atenção!</h4>
       Os testes realizados não serão exportados para o arquivo no formato do programa desktop, pois eles não podem ser editados.
    </div>
</p>
<script type="text/javascript" src="<?=JS.'ajaxfileupload.js'?>"></script>
<script>
    
   var cnpj = '';
   var cpf = "<?php echo $this->session->userdata('cpf') ?>";
   var formato = '';
   function selecionaCNPJ($cnpj,$nome){
       cnpj = $cnpj;
       $('#botaoInsti').html($nome+'<span class="caret"></span>');
   }
   function selecionaFormato($formato){
       formato = $formato;
       if($formato == 'CODY'){
           $('#msg').show('slide');
           $('#botaoFormato').html('Programa Desktop Cody<span class="caret"></span>');
       }else{
           $('#botaoFormato').html($formato+'<span class="caret"></span>');
           $('#msg').hide();
       }
   }
   
   function exportarDados(){
        if( cnpj == ''  || formato == '' ){
            $('#aviso_titulo').text('Atenção!');
            $('#aviso_mensagem').text('É necessário selecionar uma Instituição e um Formato para exportar');
            $('#aviso').modal('show');
        }else{
            var btn = $('#btn-gerarEdu');
            btn.button('loading')
            setTimeout(function () {
                btn.button('reset');
            }, 3000);
            $.ajax(
            {
                method: "post",
                url: BASE_URL+"exportacao/exportarArquivo/",
                data: { cnpj: cnpj,formato:formato,cpf:cpf},
                error: function(data){
                    $('#mensagem').hide('slide');
                    $('#mensagem').removeClass('alert-success');
                    $('#mensagem').removeClass('alert-block');
                    $('#mensagem').addClass('alert-error');
                    $('#mensagem').html("<h4 class='alert-heading'>Ooops! Erro</h4>"+data);
                    $('#mensagem').show('slide');
                },
                success: function(data){
                    resultado = JSON.parse(data);
                    if(resultado.status == 'error'){
                        $('#mensagem').hide('slide');
                        $('#mensagem').removeClass('alert-success');
                        $('#mensagem').removeClass('alert-block');
                        $('#mensagem').addClass('alert-error');
                        $('#mensagem').html("<h4 class='alert-heading'>Ooops! Erro</h4>"+resultado.mensagem);
                        $('#mensagem').show('slide');
                    }else{
                        $('#mensagem').hide('slide');
                        window.open(resultado.mensagem,'Download');  
                    }
                }
            }); 
        }
        
        
   }
    
</script>

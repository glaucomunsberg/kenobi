<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Resultados extends CI_Controller{
	function __construct() {
		parent::__construct();
		$this->load->model('sistema/loginmodel', 'loginModel');
		$this->load->model('turmasmodel','turmasModel');
		$this->load->model('alunosmodel','alunosModel');
                $this->load->model('turmaalunosmodel','turmaAlunosModel');
		$this->load->model('testesmodel','testesModel');
                $this->load->model('instituicaoModel','instituicaomodel');
                $this->load->model('quintilmodel',  'quintilModel');
		$this->loginModel->logged();
	}
        
        /**
         * Método que retorna os valores dos testes da turma
         */
        function resultadoPorTurma(){
            
            $data['testes'] = array();
            $linhas = array();
            $avalicoes = $this->testesModel->getAvaliacoesByTurma($_POST['cnpj'],$_POST['cpf'],$_POST['nome_turma'],$_POST['turma_ano']);
            $data['turmas']=  $this->turmasModel->getTurmaByProfessor($_POST['cpf']);
            foreach ($avalicoes as $avalicao){
                $testes = $this->testesModel->getTestesAlunosByAvaliacao($avalicao['cnpj'],$avalicao['cpf'],$avalicao['nome_turma'], $avalicao['ano'],$avalicao['dt_ocorrencia']);
  //              log_message('info', array_);
                foreach ($testes as $teste){
                    
                    $idade = $this->quintilModel->calc_idade($teste['dt_nascimento'],$avalicao['dt_ocorrencia']);
                  //  echo $idade;
                    $aux= $this->quintilModel-> getQuintilAbdominaisByGenero($teste['genero'], $idade ,$teste['num_abdominais']);
                //    log_message('info', $aux);
                    $valoresLinha= array('nome'=>$teste['nome'], 'genero'=>  $this->alunosModel->converteGenero($teste['genero']),'quitilabdminal' => $aux, 'imc' => $teste['imc'], 'corrida_vinte_metros' => $this->quintilModel->getQuintilVelocidadeDeslocamentoByGenero($teste['genero'], $idade ,$teste['corrida_vinte_metros']),'salto_horizontal'=> $this->quintilModel->getQuintilForcaExplosivaInferiorByGenero($teste['genero'], $idade ,$teste['salto_em_distancia']),
                            'medicinebal' => $this->quintilModel->getQuintilForcaExplosivaSuperiorByGenero($teste['genero'], $idade ,$teste['arremesso_medicineball']),
                        'quadrado'=>  $this->quintilModel->getQuintilAgilidadeByGenero($teste['genero'], $idade ,$teste['quadrado']),
                        'sentar_alcancar'=> $this->quintilModel-> getQuintilFlexibilidadeByGenero($teste['genero'], $idade ,$teste['sentar_alcancar']));
                    array_push($linhas, $valoresLinha);
                }
                array_push($data['testes'], array('ocorrencia'=>$avalicao['dt_ocorrencia'], 'testes'=> $linhas));
                $linhas = array();
            }
            
            $data['turma'] = array('nome_turma'=>$_POST['nome_turma'], 'instituicao_nome'=>$_POST['cnpj']);
            
            $this->load->view('sistema/menuVerResultadosPorTurmaView',$data);
        }
        
        /**
         * função da tela de resultado por aluno, os dados do aluno é passado por POST.
         */
	function resultadoPorAluno(){
            // por post : cnpj, cpf, nome, nome_mae, dt_nascimento
           // $data['quintil'] = $this->getQuintilByAluno($_POST['cnpj'],$_POST['cpf'],$_POST['nome_turma'],$_POST['turma_ano']);
           // $data['alunos'] = $this->turmaAlunosModel->getAlunosByTurma($_POST['cnpj'],$_POST['cpf'],$_POST['turma_nome'],$_POST['turma_ano']);
           $testes = $this->testesModel->getTestesByAluno($_POST['cpf'], $_POST['nome'], $_POST['nome_mae'], $_POST['dt_nascimento']);
           $resultados = array();
           $aluno = array();
           
            $data['testes'] = array() ;
            if(!empty($testes)){
           foreach ($testes as $teste){
             
              $aluno = $this->alunosModel->getAluno($teste['nome'], $teste['nome_mae'], $teste['dt_nascimento']);
              log_message('ERROR','Nome'.$aluno[0]['nome']);
              log_message('ERROR','nome_mae'.$aluno[0]['nome_mae']);
              log_message('ERROR','dt_nascimento'.$aluno[0]['dt_nascimento']);
              $idade = $this->quintilModel->calc_idade($teste['dt_nascimento'],$teste['dt_ocorrencia']);
              $quintil['forcaAbdominal']= $this->quintilModel->mudaQuintil($this->quintilModel->getQuintilAbdominaisByGenero($aluno[0]['genero'],$idade,$teste['num_abdominais']));
             // array_push($resultados, $quintil);
              $quintil['mebroSuperior']= $this->quintilModel->mudaQuintil($this->quintilModel->getQuintilForcaExplosivaSuperiorByGenero($aluno[0]['genero'], $idade ,$teste['arremesso_medicineball']));
              $quintil['salto_horizontal']= $this->quintilModel->mudaQuintil($this->quintilModel->getQuintilForcaExplosivaInferiorByGenero($aluno[0]['genero'], $idade ,$teste['salto_em_distancia']));
              $quintil['quadrado']= $this->quintilModel->mudaQuintil($this->quintilModel->getQuintilAgilidadeByGenero($aluno[0]['genero'], $idade ,$teste['quadrado']));
              $quintil['corrida_vinte_metros']=$this->quintilModel->mudaQuintil($this->quintilModel->getQuintilVelocidadeDeslocamentoByGenero($aluno[0]['genero'], $idade ,$teste['corrida_vinte_metros']));
              $quintil['sentar_alcancar']= $this->quintilModel->mudaQuintil($this->quintilModel-> getQuintilFlexibilidadeByGenero($aluno[0]['genero'], $idade ,$teste['sentar_alcancar']));
              $quintil['dt_ocorrencia']= $teste['dt_ocorrencia'];
              array_push($data['testes'],array('nome'=>$teste['nome'],'dt_nascimento'=>$teste['dt_nascimento'] ,'nome_mae'=>$teste['nome_mae'] , 'dt_ocorrencia'=>$quintil['dt_ocorrencia'], 'forcaAbdominal'=>$quintil['forcaAbdominal'],
                  'membroSuperior'=>$quintil['mebroSuperior'],'corrida_vinte_metros'=>$quintil['corrida_vinte_metros'],
                  'salto_horizontal'=>$quintil['salto_horizontal'],
                  'quadrado'=>$quintil['quadrado'],
                  'sentar_alcancar'=>$quintil['sentar_alcancar']));
           }
            }else{
                $quintil['forcaAbdominal']=0;
                $quintil['dt_ocorrencia']= 'Teste não realizado';
                $quintil['sentar_alcancar']=0;
                $quintil['corrida_vinte_metros']=0;
                $quintil['salto_horizontal']=0;
                $quintil['quadrado']= 0;
                $quintil['mebroSuperior']= 0;
                
                 array_push($data['testes'],array('nome'=>$_POST['nome'],'dt_nascimento'=>$_POST['dt_nascimento'] ,'nome_mae'=>$_POST['nome_mae'] , 'dt_ocorrencia'=>$quintil['dt_ocorrencia'], 'forcaAbdominal'=>$quintil['forcaAbdominal'],
                  'membroSuperior'=>$quintil['mebroSuperior'],'corrida_vinte_metros'=>$quintil['corrida_vinte_metros'],
                  'salto_horizontal'=>$quintil['salto_horizontal'],
                  'quadrado'=>$quintil['quadrado'],
                  'sentar_alcancar'=>$quintil['sentar_alcancar']));
           }
            
            $data['nome']=$_POST['nome'];
            $data['alunos']= $this->alunosModel->getAlunosByEducador($_POST['cpf']);
            $this->load->view('sistema/menuVerResultadosPorAlunoView',$data);
	}
        
        
        function getQuintilTurma($cnpj,$cpf,$turma_nome,$turma_ano){
            //pegar todos os aluno de uma turma    
            $alunos = $this->turmaAlunosModel->getAlunosByTurma($cnpj,$cpf, $turma_nome,$turma_ano);
            $quintilByTurmas = array();
            
            foreach ($alunos as $aluno) {	
	        //pegar todos os testes que o aluno fez e calcular o seu quintil        
                $testes = $this->testesModel->getTeste($cnpj,$cpf, $turma_nome, $aluno['nome'], $aluno['nome_mae'], $aluno['dt_nascimento']);
                foreach ($testes as $teste) {	
                    $nroAbdominais = $teste['num_abdominais'];
                    $testeQuintil = $this->getResultadoAbdominal($aluno['genero'],$this->quintilModel->calc_idade( $aluno['dt_nascimento'], $teste['dt_ocorrencia']),$nroAbdominais);
                    array_push($quintilByTurmas, array('teste'=>$teste['nome'], 'nome'=> $aluno['nome'], 'forcaAbdominal'=>$testeQuintil));
                }
            }
            return $quintilByTurmas;
        }
        
 
        //função que retorna todos os quintis de um aluno
	function getQuintilByAluno($cnpj,$cpf, $turma_nome,$aluno){
            $quintilByaluno = array(); 
            $testes = $this->testesModel->getTeste($cnpj,$cpf, $turma_nome, $aluno['nome'], $aluno['nome_mae'], $aluno['dt_nascimento']);
            foreach ($testes as $teste) {	
                $nroAbdominais = $teste['num_abdominais'];
                $testeQuintil = $this->testesModel->getResultadoAbdominal($aluno['genero'],$this->quintilModel->calc_idade( $aluno['dt_nascimento'], $teste['dt_ocorrencia']),$nroAbdominais);
                array_push($quintilByAluno, array('teste'=>$teste['nome'], 'nome'=> $aluno['nome'], 'forcaAbdominal'=>$testeQuintil));
            }
            return $quintilByAluno;
        }
	
    
}


?>

DROP TABLE IF EXISTS DEFAULT_SCHEMA.alunos;

CREATE TABLE DEFAULT_SCHEMA.alunos (
       nome VARCHAR(255) NOT NULL DEFAULT 'NOT NULL'
     , nome_mae VARCHAR(255) NOT NULL
     , dt_nascimento DATE NOT NULL
     , genero CHAR(1)
     , nome_pai VARCHAR(255)
     , cpf VARCHAR(11)
     , rg VARCHAR(25)
     , endereco VARCHAR(300)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (nome, nome_mae, dt_nascimento)
);


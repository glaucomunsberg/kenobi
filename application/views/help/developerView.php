<?=headerGlobalView();?>
<div id="total" class="container">
    <div id="menuGeral" data-spy="affix-top" data-offset-top="0"  class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse" >
                    <ul class="nav">
                        <li class=""><a href="<?=BASE_URL?>" ><i class="icon-home"></i>Início</a></li>
                        <li class=""><a href="<?=BASE_URL?>kenobi" >Kenobi </a></li>
                        <li class=""><a href="<?=BASE_URL?>cody" >Cody </a></li>
                        <li class=""><a href="<?=BASE_URL.'ajuda'?>"  target="_blank">Central de Ajuda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 50px;text-align: center">

    </div>
    <div class="row" style="margin-top: 20px">
        <div class="span12">
            <p>
                <h1><strong>Olá desenvolvedor!</strong></h1>
            </p>
            <p>
                <h6>Programa de desenvolvimento </h6>
            </p>
            <p style="text-indent: 40px; text-align: justify; margin-top: 10px;margin-bottom: 25px">
                Atualmente o Projeto ProDown conta com uma equipe de voluntários para dar manuteção aos programas que o constitue. O Prodown conta com dois programas: O <strong>Kenobi</strong> me PHP que atua como servidor e o programa desktop <strong>Cody</strong>
                que formata as informações. Visite as páginas destinas a cada um dos programa para conhecer mais ou entre em contato conosco.
            </p>
            <p>
                <a class="btn btn-large btn-block btn-primary"  href="<?=BASE_URL.'kenobi'?>" >Kenobi</a>
                <a class="btn btn-large btn-block btn-primary"  href="<?=BASE_URL.'cody'?>">Cody</a>
            </p>
        </div>
    </div>
    <div class="row">
        
    </div>
</div>

<script>
      $('#total').css('height',$(document).height()-180);
</script>
<?=footerView(); ?>
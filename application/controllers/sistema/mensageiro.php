<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mensageiro extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('sistema/mensageiromodel', 'mensageiroModel');
        $this->load->model('sistema/loginmodel', 'loginModel');
        $this->loginModel->logged();
    }
    
    
    public function listaMensagens(){
        print json_encode($this->mensageiroModel->listaMensagens($_POST,$_GET));
    }
    
    public function criarMensagem(){
         print json_encode($this->mensageiroModel->inserirMensagem($_POST));
    }
}
?>
Cenário Principal

Esta é um caso de uso apenas para compreensão da necessidade de estar logado. Quando for solicitado o validar Usuário, o sistema verificará se o usuário está logado e se pode operar o caso de uso que ele está tentando executar. Caso possa o sistema retorna com uma mensagem de sucesso.

Cenário Alternativo 1 (Usuario Não logado):
Se o usuário não estiver logado, deve então ser redirecionado a página de login para que ele efetue o login.

Cenário Alternativo 2(Usuario Não tem permissão):
Se o usuário não tem permissão, deve então ser redirecionado para página que informa que aquela ação deve ser executada apenas por  usuários que tem permissão.

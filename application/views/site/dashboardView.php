<?=headerGlobalView();?> 
<div id="total" class="container">
     <div id="menuGeral" data-spy="affix-top" data-offset-top="0"  class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <div class="nav-collapse collapse" >
                    <ul class="nav">
                        <li class=""><a href="#home" ><i class="icon-home"></i>Início</a></li>
                        <li class=""><a href="#projeto" >O Projeto</a></li>
                        <li class=""><a href="#programaDesktop">Programa Desktop</a></li>
                        <li class=""><a href="#publicacoes" >Publicações</a></li>
                        <li class=""><a href="<?=BASE_URL.'ajuda'?>"  target="_blank">Central de Ajuda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <section id="home" style="margin-top: 50px">
        <div class="row" >
            <div class="span9">
                <h1><?=lang('proDown')?> <small><?=lang('kenobiSistema')?></small></h1>
            </div>
            <div class="span3">

                <?php
                    echo form_open(BASE_URL.'sistema/login/');
                ?>
                <div class="control-group">
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-envelope"></i></span>
                        <input class="span2" id="cpf" name="cpf" style="width: 180px" type="text" placeholder="cpf ou email">
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-lock"></i></span>
                        <input class="span2" id="password" style="width: 180px" name="password" type="password" placeholder="senha">
                    </div>
                    <div style="margin-bottom: 5px">
                        <?php echo form_submit('submit', 'Entrar','class="btn btn-success btn-small" style="width: 88px"');?>
                        <a type="button" class="btn btn-link btn-small" href="<?=BASE_URL.'cadastro'?>"><strong>Inscreva-se já</strong></a>
                        <?php form_close(); ?>
                    </div>
                    <div>
                        <?php echo validation_errors(); ?>
                    </div>
                </div>


            </div>
        </div>
        <div class="row">
            <div class="span12">
                <div id="myCarousel" class="carousel slide" style="height: 260px">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item" style="text-align: center;">
                            <img src="<?=IMG.'home/carrossel1.jpg'?>" alt="">
                            <div class="carousel-caption">
                              <h4>Bem-vindo ao ProDown</h4>
                              <p><strong>Sistema de Gestão de Informações</strong> com foco em aptidões esportivas de crianças de 10 a 20 anos de idade com Síndrome de Down.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?=IMG.'home/carrossel2.jpg'?>" alt="">
                            <div class="carousel-caption">
                              <h4>Você sabia?!</h4>
                              <p>Programa de Avaliação da Aptidão Física de Crianças e Jovens com Síndrome de Down é um programa brasileiro que está atrás de jovens<strong>talentos esportivos</strong>. </p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="<?=IMG.'home/carrossel3.jpg'?>" alt="">
                            <div class="carousel-caption">
                              <h4>Esporte é Importante</h4>
                              <p> A inclusão através do esporte traz maior integração entre o corpo e a mente. <strong>"A maior dificuldade do meu filho era falar, ele falava pouco. Agora, praticando o esporte, ele está mais espontâneo"</strong>, relatou Maria Ribeiro de Carvalho, mãe de praticante de Karatê-Do</p>
                            </div>
                        </div>
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="span12">
                <?= $this->load->view('site/homeView') ?>
            </div>
        </div>
    </section>
    <div class="row">
        <div class="span12">
            <section id="projeto" style="margin-top: 60px">
                <?= $this->load->view('site/projetoView') ?>
            </section>
            <section id="programaDesktop" style="margin-top: 60px">
                <?= $this->load->view('site/programaDesktopView') ?>
            </section>
            <section id="publicacoes" style="margin-top: 60px">
                <?= $this->load->view('site/publicacoesView') ?>
            </section>
        </div>
    </div>
</div>
<script>
    $('.carousel').carousel({
      interval: 6000
    });
    $('#menuGeral').affix({
        offset: $('#menuGeral').position()
    });
    $('#total').css('height',$(document).height()-208);
</script>
<?=footerView(); ?>

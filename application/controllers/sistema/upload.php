<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

        /**
	 * @author Glauco Roberto Munsberg dos Santos
         * 
         *  Controller responsável pela administração de
         *      arquivos dentro do AtHome
         * 
         * @see www.glaucomunsberg.com/projetoAtHome
         * 
	 */
class Upload extends CI_Controller 
{
   public function __construct()
   {
      parent::__construct();
      $this->load->model('sistema/UploadModel','uploadModel');
      $this->load->database();
      $this->load->helper('url');
   }
 
   public function index()
   {
      $this->load->view('upload');
   }
   
   public function upload_file()
    {
       $status = "";
       $msg = "";
       $file_element_name = 'userfile';
       $data[''] = '';

       if (empty($_POST['title']))
       {
          $status = "error";
          $msg = "Por favor, selecione um arquivo.";
          $file_name = '';
       }

       if ($status != "error")
       {
          $config['upload_path'] = 'archives/';
          $config['allowed_types'] = '*';
          $config['max_size']  = 1024 * 8;
          $config['encrypt_name'] = TRUE;

          $this->load->library('upload', $config);

          if (!$this->upload->do_upload($file_element_name))
          {
             $status = 'error';
             $msg = $this->upload->display_errors();
             $file_name = '';
          }
          else
          {
             $data = $this->upload->data();
             $file_id = $this->uploadModel->insert_file($data['file_name'], $_POST['title']);
             if($file_id)
             {
                $status = "success";
                $msg = "Arquivo Enviado com sucesso!";
                $file_name = $data['file_name'];
                $file_id = $file_id;
             }
             else
             {
                unlink($data['full_path']);
                $status = "error";
                $msg = "Algo de errado aconteceu :/ Tente novamente.";
                $file_name = '';
             }
          }
          @unlink($_FILES[$file_element_name]);
       }
       echo json_encode(array('status' => $status, 'msg' => $msg,'file_name'=>$file_name,'file_id'=>$file_id));
    }
}
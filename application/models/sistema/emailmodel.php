<?php
class EmailModel extends CI_Model {
    
    
    
   function enviarEmailBoasVindas($email_prodwn,$senha_prodwon,$EducadorNome,$educadorEmail){
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = 7;
        $config['smtp_user']    = $email_prodwn;
        $config['smtp_pass']    = $senha_prodwon;
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not 
        
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from($email_prodwn, lang('proDown'));
        $this->email->to($educadorEmail); 

        $this->email->subject('Prodown - Bem Vindo Colaborador!');
        $this->email->message(
                "<html>
                    <body>
                        <img src=\"http://kenobi.glaucomunsberg.com/static/img/email_head.png\">
                        <p>
                            Bem vindo ao Prodown <b>$EducadorNome!</b>
                        </p>
                        <p>
                            Estamos muito felizes por você ter se tornado mais um colaborador para o projeto. Para a segurança ainda há a necessidade de validar os dados enviados pro você. Esse processo demora entre 1 a 7 dias no máximo. Para maiores informações visite nosso site. Você receberá um email assim que sua conta for liberada liberado.
                        </p>
                        <p>
                            Acesse o sistema <a href=\"".BASE_URL."\">Prodown</a>.
                        </p>
                    </body>
                </html>");
        $this->email->send();
        log_message('INFO', $this->email->print_debugger());
    }
    
    function notificaAdministradorDoSistema($email_prodwn,$senha_prodwon,$email_administrador,$titulo,$conteudo){
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = 7;
        $config['smtp_user']    = $email_prodwn;
        $config['smtp_pass']    = $senha_prodwon;
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not 
        
        $this->email->initialize($config);

        $this->email->from($email_prodwn, lang('proDown'));
        $this->email->to($email_administrador); 

        $this->email->subject($titulo);
        $this->email->message($conteudo);  

        $this->email->send();
        log_message('INFO', $this->email->print_debugger());
    }
    
    function enviarLiberacaoDeAcesso($email_prodwn,$senha_prodwon,$EducadorNome,$educadorEmail){
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = 7;
        $config['smtp_user']    = $email_prodwn;
        $config['smtp_pass']    = $senha_prodwon;
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not 
        
        $this->email->initialize($config);

        $this->email->from($email_prodwn, lang('proDown'));
        $this->email->to($educadorEmail); 

        $this->email->subject('Prodown - Liberação de Conta');
        $this->email->message(
                "<html>
                    <body>
                        <img src=\"http://kenobi.glaucomunsberg.com/static/img/email_head.png\">
                        <p>
                            Olá <b>$EducadorNome</b>!\n
                                seu pedido de <b>Acesso ao Prodown</b> acaba de ser liberado.\n
                        </p>
                        <p>
                            Acesse o sistema <a href=\"".BASE_URL."\">Prodown</a>.
                        </p>
                    </body>
                </html>
                    ");  

        $this->email->send();
        log_message('INFO', $this->email->print_debugger());
    }
    
    function enviarLiberacaoDeAcessoInstituicao($email_prodwn,$senha_prodwon,$EducadorNome,$educadorEmail,$InstituicaoNome){
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.gmail.com',
            //'smtp_host' => 'ssl://smtp.gmail.com',  
            'smtp_port' => 465,
            //'smtp_timeout' => 7,
            'smtp_user' => $email_prodwn,
            'smtp_pass' => $senha_prodwon,
            'mailtype'  => 'html', 
            'charset'   => 'utf-8',
            'newline'   => '\r\n',
            'validation' => true
        );
        
        $this->email->initialize($config);

        $this->email->from($email_prodwn, lang('proDown'));
        $this->email->to($educadorEmail); 

        $this->email->subject('Prodown - Liberação de Conta');
        $this->email->message(
                "<html>
                    <body>
                        <img src=\"http://kenobi.glaucomunsberg.com/static/img/email_head.png\">
                        <p>
                            Olá <b>$EducadorNome</b>!\n
                                seu pedido de acesso para a instituição \"".$InstituicaoNome."\" foi liberado. A partir desse momento você poderá fazer upload de informações para essa Instituição.\n
                            Acesse <a href=\"".BASE_URL."\">Prodown</a>.
                        </p>
                        <p>
                            Acesse o sistema <a href=\"".BASE_URL."\">Prodown</a>.
                        </p>
                    </body>
                </html>
                    ");  

        $this->email->send();
        log_message('INFO', $this->email->print_debugger());
    }
    
    function enviarNovaSenha($email_prodwn,$senha_prodwon,$educador_nome,$educador_email,$nova_senha){
        
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = 7;
        $config['smtp_user']    = $email_prodwn;
        $config['smtp_pass']    = $senha_prodwon;
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not 
        
        $this->email->initialize($config);

        $this->email->from($email_prodwn, lang('proDown'));
        $this->email->to($educador_email); 

        $this->email->subject('Prodown - Nova Senha requisitada');
        $this->email->message(
                "<html>
                    <body>
                        <img src=\"http://kenobi.glaucomunsberg.com/static/img/email_head.png\">
                        <p>
                            Olá <b>$educador_nome</b>!<br>
                                Uma nova senha foi gerada para você. <br>Para acessar a sua conta no Prodown você deve usar a senha abaixo:<br>Senha:<b> $nova_senha</b>\n\n
                        </p>
                        <p>
                            Obs.: Após acessar sua conta você poderá modificá-la a qualquer momento;
                        </p>
                        <p>
                            Acesse o sistema <a href=\"".BASE_URL."\">Prodown</a>.
                        </p>
                    </body>
                </html>
                    ");  

        $this->email->send();
        log_message('INFO', $this->email->print_debugger());
    }
}
?>
<h2> Configurador<small> do sistema.</small></h2>
<p>
    Abaixo você poderá ter acesso a algumas informações do sistema e modificar algumas delas:
</p>
<div class="row">
    <div class="span12"  style="margin-left: 40px">
        <?=jTableStart('configuracao','Configuração','configuracao/listaPerfil','','configuracao/atualizarPerfil','',array())?>
            <?=jPanelAddID(true,true,true)?>
            <?=jPanelAddCampo('nome_perfil', 'Educador', '', '10%',true,false,true)?>
            <?=jPanelAddCampoValMulti('sistema_online', 'Online', '', array('1'=>'Sim','0'=>'Não'), '10%', true)?>
            <?=jPanelAddCampoValMulti('linguagem_padrao', 'Linguagem', '', array('pt_br'=>'Português','en'=>'Inglês'), '15%', true)?>
            <?=jPanelAddCampo('email','Sis. Email','textarea','25%',true,false,true)?>
            <?=jPanelAddCampo('senha','Sis. Ema. Senha','textarea','15%',true,false,true)?>
            <?=jPanelAddCampo('email_administrador','Email do Admin','textarea','25%',true,false,true)?>
        <?=jTableEnd()?>
    </div>
</div>
<h2>Como usar o Sistema Web (Kenobi)</h2>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    Aqui você encontrará toda ajuda para o sistema web do ProDown! Ele foi feito complemente pensando em ser fácil, útil e organico as necessidades
    do projeto e de nossos colaboradores. Conheça um pouco mais dele abaixo como realizar algumas atividades.
</p>
<h4>Acessando o sistema</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Para acessar o sistema você deve estar cadastrado! Dúvidas em como se cadastrar
    <a class="btn btn-primary btn-link" href="<?= BASE_URL . 'site/help#sistema' ?>">clique Aqui</a> para obter maiores informações do processo.
</p>
<div class ="row-fluid">
    <div class="span8">
        <p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
            Se seu acesso ao sistema já foi liberado pelo Administrador do sistema,
            basta fazer o login no site. Caso não tenhas recebido o email de ativação da conta,
            aguarde o mesmo ou entre em contato com o administrador.
        </p>
        <p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
            O login no sistema deve ser feito através do Email ou CPF e com a senha que você criou no momento do seu cadastro.
        </p>
    </div>
    <div class="span4">
        <a href="#" class="thumbnail" style='margin-bottom: 40px'>
            <img src="<?=IMG.'ajuda/cadastrar.png'?>" alt="">
        </a>
    </div>    
</div>
<h4>Ínicio</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Após fazer o login está será sua tela inicial. Através deste menu
    localizado na sua esquerda, será possível acessar todas as funcionalidades do sistema!
</p>
<a href="#" class="thumbnail" style='margin-bottom: 40px'>
    <img src="<?=IMG.'ajuda/inicio.png'?>" alt="">
</a>
<h4>Enviar Avaliações</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    É neste menu que você enviará os dados coletados nos testes e organizados através 
    do programa desktop para o ProDown!
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Para escolher o arquivo a ser envidado clique em "Escolher Arquivo". 
    Após feito a escolha clique em "Enviar". O arquivo a ser enviado deve ter sido criado pelo programa desktop do projeto e ter a extensão ".cody".
    Não são aceitos outros tipos de arquivos para envio das avaliações!
</p>
<a href="#" class="thumbnail" style='margin-bottom: 20px'>
    <img src="<?=IMG.'ajuda/enviar_testes.png'?>" alt="">
</a>
<p  style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    Pronto! Seu arquivo foi enviado!
</p>
<h4>Recuperar Turmas</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 10px'>
    Se por algum motivo você perdeu o seu arquivo de turmas criado no
    programa desktop, saiba que é possível recuperá-lo neste menu!
</p>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Você escolhe a instituição que você pertence, a turma desejada e o formato
    do arquivo para exportar, clique em "Gerar" e um arquivo com sua turma será gerado
    para seu backup
<p>
<a href="#" class="thumbnail" style='margin-bottom: 20px'>
    <img src="<?=IMG.'ajuda/exportar.png'?>" alt="">
</a>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    Os testes desta turma não seram enviados por motivos de segurança
    do sistema!
</p>
<h4>Ver Resultados</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Neste menu você pode ver os resultados dos testes já com os quintis de avaliação e outras informações gerais.
    Para ver os resultados de uma turma específica selecione uma turma no botão "Turmas Vinculadas".
    Se você quiser ver os resultados para um aluno específico selecione ele no botão "Alunos Vinculados".
</p>
<a href="#" class="thumbnail" style='margin-bottom: 40px'>
    <img src="<?=IMG.'ajuda/resultados.png'?>" alt="">
</a>
<h4>Programa Desktop</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
   Aqui neste menu você pode fazer o download do programa desktop do ProDown.
    É através deste programa que você irá organizar as suas avaliações para serem 
    enviadas ao Sistema Web do Prodown.
</p>
<div class="alert alert-info" style='margin-bottom: 20px'>
    <strong><i class="icon-info-sign"></i>Info</strong> Escolha o download de acordo com o sistema operacional de seu computador.
</div>
<a href="#" class="thumbnail" style='margin-bottom: 20px'>
    <img src="<?=IMG.'ajuda/programa_desktop.png'?>" alt="">
</a>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    Para saber como usar o Programa Desktop do ProDown acesse 
    <a class="btn btn-primary btn-link" href="<?= BASE_URL .'site/help#desktop' ?>">Como usar o programa Desktop</a>
</p>
<h4>Notificar Administrador</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Aqui você poderá entrar em contato com o Administrador do sistema através
    do sistema de comunição feito pra você! Basta clicar em "Adicionar" e você poderá
    enviar uma mensagem ao Adminstrador. O Administrador também pode entrar em contato 
    com você através deste sistema. Além disso é possivel ver o histórico das trocas de mensagens!
</p>
<a href="#" class="thumbnail" style='margin-bottom: 40px'>
    <img src="<?=IMG.'ajuda/notificar_adm.png'?>" alt="">
</a>
<h4>Conta</h4>
<p  style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Neste menu você poderá fazer alterações nas informações da sua conta como seu nome, email e senhas.
</p>
<a href="#" class="thumbnail" style='margin-bottom: 40px'>
    <img src="<?=IMG.'ajuda/conta.png'?>" alt="">
</a>
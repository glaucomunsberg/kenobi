<?php

class UsuariosModel extends CI_Model{

    function getUsuarios(){
            $this->db->trans_start();
                    $this->db->select('*');
                    $this->db->from('usuarios');
            $this->db->trans_complete();

            return $this->db->get()->result();

    }

    #get dados de um usuario especifico
    function getUsuario($cpf){
            $this->db->trans_start();
                    $this->db->select('*');
                    $this->db->from('usuarios');
                    $this->db->where('cpf', $cpf);
            $this->db->trans_complete();

            return $this->db->get()->result();
    }


    #get de todos os usuarios segundo o tipo passado
    # A é adm    ,E é educador
    function getUsuarioByTipoUsuario($tipo_usuario){
            $this->db->trans_start();
                    $this->db->select('*');
                    $this->db->from('usuarios');
                    $this->db->where('tipo_usuario', $tipo_usuario);
            $this->db->trans_complete();

            return $this->db->get()->result();
    }

    #inserir usuario
    function inserirUsuario($usuario){
            $cpf = str_replace(".", "", $usuario['cpf']);
            $cpf = str_replace("-", "", $cpf);
            $cpf = str_replace("/", "", $cpf);
            $this->db->trans_start();
                    $this->db->set('cpf', $cpf);
                    $this->db->set('password', md5( $usuario['senha1'] ));
                    if(!empty($usuario['tipo_usuario']) ){
                        $this->db->set('tipo_usuario', $usuario['tipo_usuario']);
                    }else{
                        $this->db->set('tipo_usuario', 'E');
                    }

                    $this->db->insert('usuarios');
            $this->db->trans_complete();

            if($this->db->trans_status() === FALSE){
                    return false;
            }else{
                    return true;
            }	
    }


    #editar login e senha do um determinado usuario
    function editarLoginSenha($usuario){
            $this->db->trans_start();
                    $this->db->set('password', md5( $usuario['password'] ));
                    $this->db->where('cpf', $usuario['cpf']);
                    $this->db->update('usuarios');
            $this->db->trans_complete();

            if($this->db->trans_status() === FALSE){
                    return false;
            }else{
                    return true;
            }

    }

    #existe usuario
    function existeUsuario($cpf){
            $this->db->trans_start();
                    $this->db->selec('*');
                    $this->db->from('usuarios');
                    $this->db->where('cpf',$cpf);
            $this->db->trans->complete();

            if($this->db->get()->num_rows() > 0 ){
                    return true;
            }else{
                    return false;
            }
    }


    #editar tipo de usuario
    function editarTipoUsuario($usuario){
            $this->db->trans_start();
                    $this->db->set('tipo_usuario',$usuario['tipo_usuario']);
                    $this->db->where('cpf',$usuario['cpf']);
                    $this->db->update('usuarios');
            $this->db->trans_complete();

            if($this->db->trans_status() === FALSE){
                    return false;
            }else{
                    return true;
            }

    }
}


?>
<!doctype html>
<html lang="pt-BR" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="shortcut icon" href="<?=IMG?>/favicon.ico">
    <script src='<?=JS."jquery.js"?>'></script>
    <script src='<?=JS."jquery-ui-1.9.2.custom.min.js"?>'></script>
    <script src='<?=JS."bootstrap.min.js"?>'></script>
    <script src='<?=JS."jquery.mask.min.js"?>'></script>
    
    <link href= '<?=CSS?>smoothness/jquery-ui-1.9.2.custom.min.css' rel="stylesheet">
    <link href= '<?=CSS?>bootstrap.min.css' rel="stylesheet">
    <meta name="description" content="<?=lang('tituloPagina');?>">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title><?=lang('tituloPagina');?></title>
    <script>
        var BASE_URL = '<?=BASE_URL?>';
        var CSS = '<?=CSS?>';
        var IMG = '<?=IMG?>';
        var JS = '<?=JS?>';
        var ARQUIVO = '<?=ARQUIVO?>';
    </script>
  </head>
  <body>
  <div id="aviso" class="modal hide fade">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h3 id="aviso_titulo">Modal header</h3>
      </div>
      <div class="modal-body">
          <p id="aviso_mensagem">One fine body…</p>
      </div>
      <div class="modal-footer">
          <a type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</a>
      </div>
  </div>

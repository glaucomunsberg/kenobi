<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Importacao extends CI_Controller{
	function __construct() {
            parent::__construct();
            $this->load->model('sistema/loginmodel', 'loginModel');
            $this->load->model('importacaomodel','importacaoModel');
            $this->load->model('instituicaomodel','instituicaoModel');
            $this->load->model('turmasmodel','turmaModel');
            $this->load->model('alunosmodel','alunoModel');
            $this->load->model('turmaalunosmodel','turmaAlunoModel');
            $this->load->model('testesmodel','testesModel');
            $this->load->model('versaoenviadamodel','versaoEnviadaModel');
            $this->load->model('sistema/uploadmodel','uploadModel');
            $this->loginModel->logged();
	}
	
	function index(){
		
	}
	
	function importarArquivo(){
            $instituicao = '';
            $turmas = '';
            $alunos = '';
            $testes = '';
            $date = '';
            $parts = '';
            try{
                if( !$this->importacaoModel->abrirArquivo($_POST['url']) ){
                    print json_encode(array('status'=>'error','msg'=>"Oops! Erro ao abrir o arquivo. Tente novamente"));
                    exit;
                }else{

                    $instituicao = $this->importacaoModel->getInstituicao();
                    if(!$this->instituicaoModel->existeInstituicao($instituicao['cnpj'])){
                            $this->uploadModel->deletarFiles($_POST['file_id']);
                            print json_encode(array('status'=>'error','msg'=>"A instituição ".$instituicao['nome']." não existe, entre em contato com o administrador"));
                            exit;
                    }
                    if($this->versaoEnviadaModel->existeHash($instituicao['hash'],$this->session->userdata('cpf'))){
                        $this->uploadModel->deletarFiles($_POST['file_id']);
                        print json_encode(array('status'=>'error','msg'=>"O arquivo com estes mesmos dados já foi enviado anteriormente"));
                            exit;
                    }else{

                        $this->versaoEnviadaModel->inserirHash($this->session->userdata('cpf'),$instituicao['hash'],$_POST['file_id'],$instituicao['versao']);
                    }

                    if(!$this->instituicaoModel->existeEducadorInstituicao($instituicao['cnpj'],$this->session->userdata('cpf'))){
                        print json_encode(array('status'=>'error','msg'=>"Você não tem vínculo com esta instituição, entre em contato com o administrador"));
                        exit;
                    }

                   $turmas = $this->importacaoModel->getTurmas();

                   foreach($turmas as $turma){
                        if(!$this->turmaModel->existeTurma($instituicao['cnpj'],$this->session->userdata('cpf'),$turma['nome_turma'],$turma['ano'])){
                         $this->turmaModel->inserirTurma($instituicao['cnpj'],$this->session->userdata('cpf'),$turma);
                        }

                        $alunos = $this->importacaoModel->getAlunosTurma($turma);
                        if($alunos){
                            foreach ($alunos as $aluno){
                                $parts = explode('/', $aluno['dt_nascimento']);
                                $date  = "$parts[2]-$parts[1]-$parts[0]";
                                if(!$this->alunoModel->existeAluno($aluno['nome'],$aluno['nome_mae'],$date)){
                                    if(strcmp( $aluno['genero'], "Masculino") == 0){
                                        $aluno['genero'] = "M";
                                    }
                                    else if (strcmp( $aluno['genero'], "Feminino") == 0){
                                        $aluno['genero'] = "F";
                                    }
                                    else{
                                        $aluno['genero'] = "N";
                                    }
                                    
                                    $this->alunoModel->inserirAluno($aluno);
                                    }
                                    if(!$this->turmaAlunoModel->alunoPertenceTurma($instituicao['cnpj'],$this->session->userdata('cpf'),$turma['nome_turma'],$turma['ano'],$aluno['nome'],$aluno['nome_mae'],$date)){
                                        $this->turmaAlunoModel->insereAlunoTurma($instituicao['cnpj'],$this->session->userdata('cpf'),$turma,$aluno);
                                    }else{
                                        $this->turmaAlunoModel->atualizaAlunoTurma($instituicao['cnpj'],$this->session->userdata('cpf'),$turma,$aluno);
                                    }
                                    $testes = $this->importacaoModel->getTestes($turma,$aluno);
                                    foreach($testes as $teste){
                                        $parts2 = explode('/', $teste['dt_ocorrencia']);
                                        $date2 = "$parts2[2]-$parts2[1]-$parts2[0]";
                                 		
                                        if(strcmp( $teste['nove_minutos_saude'], "true") == 0){
                                       		 $teste['nove_minutos_saude'] = (boolean)1;
                                        }
                                    	else{
                                        	 $teste['nove_minutos_saude'] = (boolean)0;
                                    	}
                                    	
                              			if(strcmp( $teste['nove_minutos_esportivo'], "true") == 0){
                                       		 $teste['nove_minutos_esportivo'] = (boolean)1;
                                         }
                                    	else{
                                        	 $teste['nove_minutos_esportivo'] = (boolean)0;
                                    	}
                                    	
                                    	if(strcmp( $teste['sentar_alcancar_com_banco'], "true") == 0){
                                       		 $teste['sentar_alcancar_com_banco'] = (boolean)1;
                                         }
                                    	else{
                                        	 $teste['sentar_alcancar_com_banco'] = (boolean)0;
                                    	}
                                    	
                                        if(!$this->testesModel->existeTeste($instituicao['cnpj'],$this->session->userdata('cpf'),$turma['nome_turma'],$turma['ano'],$aluno['nome'],$aluno['nome_mae'],$date,$date2)){
                                            
                                            $this->testesModel->inserirTeste($instituicao['cnpj'],$this->session->userdata('cpf'),$turma,$aluno,$teste);
                                        }
                                    }
                                }
                            }
                        }
                        print json_encode(array('status'=>'sucess','msg'=>"Dados inseridos com sucesso!"));
                    }
            }catch(Exception $e){
                
                $this->versaoEnviadaModel->removerHash($this->session->userdata('cpf'),$instituicao['hash']);
                $this->uploadModel->deletarFiles($_POST['file_id']);
                print json_encode(array('status'=>'error','mensagem'=>"Algo de errado aconteceu no nosso servidor, tente novamente"));
            }
        }
}


<h2><?=lang('menuPrivadoEnviar')?><small><?=lang('enviarArquivoDef')?></small></h2>
<h2></h2>
<p style="text-align: justify"><?=lang('enviarArquivoExplanacao')?></p>
<h2></h2>
<ul class="thumbnails">
    <li class="span12">
      <a href="#" class="thumbnail">
        <img src="<?=IMG.'cody/salvarcomo.png'?>" alt="">
      </a>
    </li>   
</ul>
<h1></h1>
<p>
    <h4><?=lang('menuPrivadoEnviarAvaliacoes')?></h4>
    <?=lang('enviarArquivoComoFazer')?>
</p>
<p>
    
    <form method="post" id="upload_file">
      <input type="hidden" name="title" id="title" value="" />
 
      <input type="file" name="userfile" id="userfile" size="20" />
      <input type="submit" data-loading-text="Gerando..." name="gerar" class="btn btn-primary" id="submit" />
   </form>
</p>
<div id="mensagem" class="alert alert-block">
  <h4 class="alert-heading">Atenção!</h4>
   Se você modificou algum teste submetido antes, entre em contato com o administrador para fazer a correção, não será sobrescrito os valores automaticamente.
</div>
<script type="text/javascript" src="<?=JS.'ajaxfileupload.js'?>"></script>
<script>
    var arquivoRecebidoNoServidor = '';
    var arquivoId = '';
    $('#submit').click(function () {
        var btn = $(this)
        btn.button('loading')
        setTimeout(function () {
            btn.button('reset');
            btn.val('Gerar')
        }, 3000)
    });
    //Aqui ele enviar o arquivo para o servidor
    $('#upload_file').submit(function(e) {
      e.preventDefault();
      $('#title').val( $('#userfile').val());
      
        if( $('#title').val() ==''){
            $('#aviso_titulo').text('Atenção');
            $('#aviso_mensagem').text('Para enviar é necessário selecionar um arquivo antes');
            $('#aviso').modal('show');
            return false;
        }
      
      $.ajaxFileUpload({
         url         :'upload/upload_file/', 
         secureuri      :false,
         fileElementId  :'userfile',
         dataType    : 'json',
         data        : {
            'title'           : $('#title').val()
         },
         beforeSend: function(){
             $('#mensagem').hide('slide');
         },
         success  : function (data, status)
         {
            
            if(data.status == 'error'){
                $('#mensagem').hide('slide');
                $('#mensagem').removeClass('alert-success');
                $('#mensagem').removeClass('alert-block');
                $('#mensagem').addClass('alert-error');
                $('#mensagem').html(data.msg);
                $('#mensagem').show('slide');
            }else{
                arquivoRecebidoNoServidor = ARQUIVO+data.file_name;
                arquivoId = data.file_id;
                importarDados();
            }
         },
         error: function(data){
            resultado = JSON.parse(data);
            $('#mensagem').hide('slide');
            $('#mensagem').removeClass('alert-success');
            $('#mensagem').removeClass('alert-block');
            $('#mensagem').addClass('alert-error');
            $('#mensagem').html("<h4 class='alert-heading'>Ooops! Erro</h4>"+resultado.msg);
            $('#mensagem').show('slide');
         }
      });
      return false;
   });
   
   //Aqui ele enviar os dados
   function importarDados(){
        $.ajax(
        {
            method: "post",
            url: BASE_URL+"importacao/importarArquivo/",
            data: { url: arquivoRecebidoNoServidor,file_id:arquivoId},
            error: function(data){
                resultado = JSON.parse(data);
                console.log('error:'+resultado);
                $('#mensagem').hide('slide');
                $('#mensagem').removeClass('alert-success');
                $('#mensagem').removeClass('alert-block');
                $('#mensagem').addClass('alert-error');
                $('#mensagem').html("<h4 class='alert-heading'>Ooops! Erro</h4>"+resultado.msg);
                $('#mensagem').show('slide');
            },
            success: function(data){
                resultado = JSON.parse(data);
                console.log(resultado);
                if(resultado.status == 'error'){
                    $('#mensagem').hide('slide');
                    $('#mensagem').removeClass('alert-success');
                    $('#mensagem').removeClass('alert-block');
                    $('#mensagem').addClass('alert-error');
                    $('#mensagem').html("<h4 class='alert-heading'>Ooops! Erro</h4>"+resultado.msg);
                    $('#mensagem').show('slide');
                }else{
                    $('#mensagem').hide('slide');
                    $('#mensagem').removeClass('alert-error');
                    $('#mensagem').removeClass('alert-block');
                    $('#mensagem').addClass('alert-success');
                    $('#mensagem').html("<h4 class='alert-heading'>Enviado com sucesso</h4>"
                                        +"Agora você já pode ver os resultados computados a partir dos dados recebidos agora");
                    $('#mensagem').show('slide');
                }
            }
        }
        );
        
   }
    
</script>

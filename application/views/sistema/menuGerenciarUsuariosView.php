<h2>Gerenciador<small> de usuários.</small></h2>
<p>
   Veja abaixo a lista dos usuários cadastrados no sistema e se ele tem ou não permissão para acessar o sistema.
</p>
<div class="row">
    <div class="span12" style="margin-left: 40px">
        <br/>
        <div class="input-prepend">
          <span class="add-on">Usuário </span>
          <input class="span8" id="usuario" type="text" placeholder="nome do usuario">
        </div>
        <br/>
        <div class="input-prepend">
            <button class="btn" id="filtrar" type="button">Filtrar</button>
            <button class="btn" id="limpar" type="button">Limpar</button>
        </div>
    </div>
</div>
<div class="row">
    <div class="span12"  style="margin-left: 40px">
        <?=jTableStart('usuarios','usuarios','administrar/listaUsuarios','','administrar/atualizarUsuario','',array('selecting'))?>
            <?=jPanelAddID(true,true,true)?>
            <?=jPanelAddCampo('cpf', 'CPF', '', '17%',true,false,true)?>
            <?=jPanelAddCampo('nome', 'Nome', '', '53%',true,false,false)?>
            <?=jPanelAddCampoValMulti('usuario_ativo', 'Acesso', '', array('1'=>'Ativo','0'=>'Não Ativo'), '15%', true)?>
            <?=jPanelAddData('dt_cadastro', 'Dt. Cadastro', '15%', 'dd-mm-yy',true,false,false)?>
        <?=jTableEnd()?>
    </div>
</div>

<script>
    $('#filtrar').click(function (e) {
        e.preventDefault();
        $('#usuarios').jtable('load', {
            usuario_nome: $('#usuario').val()
        });
    });
    $('#limpar').click(function () {
         $('#educador').val('');
    });
</script>
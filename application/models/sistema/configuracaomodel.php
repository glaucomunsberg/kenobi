<?php
class ConfiguracaoModel extends CI_Model {
    
    
    
    function getTotalPerfies(){
        $retorno =  $this->db->get('configuracao_perfil');
        return $retorno->num_rows();
    }
    
    function getPerfeis(){
	$this->db->trans_start();
		$this->db->select('nome_perfil');
		$this->db->from('configuracao_perfil');
	$this->db->trans_complete();	
	return $this->db->get()->result();
    }
    
    function getLinguagem(){
	$this->db->trans_start();
		$this->db->select('linguagem_padrao');
		$this->db->from('configuracao_perfil');
                $this->db->limit(1);
	$this->db->trans_complete();	
	return $this->db->get()->row()->linguagem_padrao;
    }
    
    function getSistemaOnline(){
	$this->db->trans_start();
		$this->db->select('sistema_online');
		$this->db->from('configuracao_perfil');
                $this->db->limit(1);
	$this->db->trans_complete();	
	return $this->db->get()->row()->sistema_online;
    }
    
    function getEmailAdministrador(){
	$this->db->trans_start();
		$this->db->select('email_administrador');
		$this->db->from('configuracao_perfil');
                $this->db->limit(1);
	$this->db->trans_complete();	
	return $this->db->get()->row()->email_administrador;
    }
    function getEmail(){
	$this->db->trans_start();
		$this->db->select('email');
		$this->db->from('configuracao_perfil');
                $this->db->limit(1);
	$this->db->trans_complete();	
	return $this->db->get()->row()->email;
    }
    
    function getSenha(){
	$this->db->trans_start();
		$this->db->select('senha');
		$this->db->from('configuracao_perfil');
                $this->db->limit(1);
	$this->db->trans_complete();	
	return $this->db->get()->row()->senha;
    }
    
    function getPerfil($nome_perfil){
	$this->db->trans_start();
		$this->db->select('*');
		$this->db->from('configuracao_perfil');
		$this->db->where('nome_perfil', $nome_perfil);
	$this->db->trans_complete();
	return $this->db->get()->result();
    }
    
    function listaPerfeis($parametros,$get){
        $data = array();

        $data['TotalRecordCount'] = $this->getTotalPerfies();

        $this->db->trans_start();
            $this->db->select('*',false);
            $this->db->from('configuracao_perfil');
            $this->db->order_by('nome_perfil','ASC');
            if( !empty($get['jtSorting'])){
                $pieces = explode(" ", @$get['jtSorting']);
                $this->db->order_by($pieces[0],$pieces[1]);
            }
            if( @$get['jtStartIndex'] != ''  && @$get['jtPageSize'] != '' ){
                $this->db->limit($get['jtStartIndex']+','+$get['jtPageSize']);
            }

            $data['Records'] = $this->db->get()->result();
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }

        return $data;
    }
    
    function atualizarPrefil($parametros){
        $this->db->trans_start();
            $this->db->where('nome_perfil', $parametros['nome_perfil']);
            $this->db->set('senha', $parametros['senha']);
            $this->db->set('email', $parametros['email']);
            $this->db->set('email_administrador', $parametros['email_administrador']);
            $this->db->set('linguagem_padrao', $parametros['linguagem_padrao']);
            $this->db->set('sistema_online', $parametros['sistema_online']);
            $this->db->update('configuracao_perfil');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }
        return $data;
    }
}
?>
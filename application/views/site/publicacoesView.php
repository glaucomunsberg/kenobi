<div class="page-header">
    <h1>Publicações<small> de artigos</small></h1>
</div>
<p style="text-align: justify;text-indent: 40px;margin-bottom: 10px">
    O projeto <strong>Prodown</strong> já publicou inúmeros artigos com base no trabalho que vem desenvolvendo, abaixo encontra-se os mais importantes e relevantes para o público que esteja interessado em conhecer um pouco mais do trabalho.
</p>
<p style="margin-left: 20px; margin-bottom: 10px">
    <div class ="row">
        <div class="span12">
            <p style="text-align: justify; text-indent: 10px">   <i class="icon-circle-arrow-down"> <a href="<?=BASE_URL.'site'?>" class="btn btn-link"></a></i>
                <strong>Artigo: Inclusão Escolar de uma criança com Síndrome de Down</strong>
            </p>
        </div>
    </div>
</p>
<p style="margin-left: 20px; margin-bottom: 10px">
    <div class ="row">
        <div class="span12">
            <p style="text-align: justify; text-indent: 10px"> <i class="icon-circle-arrow-down"> <a href="<?=BASE_URL.'site'?>" class="btn btn-link"></a></i>
                <strong>Artigo: Avaliação da Composição Corporal em Adultos com Síndrome de Down</strong>
            </p>
        </div>
    </div>
</p>

<h2>Programa Desktop</h2>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 40px'>
    Aqui você encontrará todas as informações necessárias de como
    usar o programa Desktop do Prodown!O Programa Desktop do Prodown nada mais é que um "formatador" de texto
    para sua melhor visualização e compreensão.
</p>
<h4>Ínicio</h4>
<p>Abra seu programa desktop e você verá está tela inicial.</p>
<a href="#" class="thumbnail" style='margin-bottom: 40px'>
    <img src="<?=IMG.'ajuda/cody-inicio.png'?>" alt="">
</a>
<h4>Cadastrando uma Instituição</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Para cadastrar uma instituição é fundamental você ter em mãos o CNPJ da mesma.
    Coloque  o CNPJ, o nome da Instituição e diga se é regular ou especial.
    Para cadastrar clique em "Arquivo"-> "Cria Nova Instituição".
</p>
<a href="#" class="thumbnail" style='margin-bottom: 40px'>
     <img src="<?=IMG.'ajuda/cody_criar_inst.png'?>" alt="">
</a>
<h4>Cadastrando Turmas</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Para cadastrar as turmas basta colocar seu nome e clicar em "salvar"!
</p>
<a href="#" class="thumbnail" style='margin-bottom: 40px'>
     <img src="<?=IMG.'ajuda/cody_criar_turma.png'?>" alt="">
</a> 
<h4>Cadastrando Alunos</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Aqui você irá preencher os dados de seus alunos basta clicar em "criar".
    Todas as informações com "*" são obrigatórias.
    Preencha com o máximo de informações possível.
</p>
<a href="#" class="thumbnail" style='margin-bottom: 40px'>
     <img src="<?=IMG.'ajuda/criar_alunos.png'?>" alt="">
</a> 
<h4>Criando e preenchendo os Testes</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Aqui você irá organizar os dados dos testes realizados. Tenha atenção ao 
    preencher para não errar as medidas! Para criar um novo teste para a turma
    basta clicar em "criar".
</p>
<a href="#" class="thumbnail" style='margin-bottom: 20px'>
     <img src="<?=IMG.'ajuda/cody_criar_avaliacao.png'?>" alt="">
</a> 
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Ao digitar um de seus testes para os alunos, certifique-se de dar um enter para registrar
    a informação.
</p>
<a href="#" class="thumbnail" style='margin-bottom: 20px'>
     <img src="<?=IMG.'ajuda/cody_testes.png'?>" alt="">
</a>
<div class="alert alert-info" style='margin-bottom: 40px'>
    <strong><i class="icon-info-sign"></i>Info</strong> Preencha todos os três tipos de testes realizados, mantendo atenção nos valores.</a>
</div> 
<h4>Ajuda</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Clicando no menu "Ajuda" você encontrará informações sobre 
    como usar o programa desktop.
</p>
<a href="#" class="thumbnail" style='margin-bottom: 40px'>
     <img src="<?=IMG.'ajuda/cody_ajuda.png'?>" alt="">
</a>   
<h4>Abrindo um Arquivo</h4>
<p style='text-align: justify; text-indent: 20px;margin-bottom: 20px'>
    Para abrir um arquivo criado em outro computador
    no seu programa desktop basta clicar em "Arquivo"->"Abrir"
    e e selecionar o arquivo ponto cody para abrir o arquivo. A partir deste momento
    você já poderá editar as informações que há nele.
</p>

<?php

    class UploadModel extends CI_Model{
        
   /**
    * @author Glauco Roberto Munsberg dos Santos
    * @see www.glaucomunsberg.com/projetoAtHome
    * 
    */
        
    /**
     * Retorna o total de arquivos
     *  registrados pelo sistema
     * @return int 
     */    
    function getTotalFiles(){
            $retorno =  $this->db->get('files_upload');
            return $retorno->num_rows();
    }
    
    /**
     * Adiciona ao banco, o registro do arquivo
     * @param String $nomeArquivo
     * @param String $titulo
     * @return id 
     */
    public function insert_file($nomeArquivo, $titulo) {
        $data = array(
            'filename' => $nomeArquivo,
            'titulo' => $titulo
        );
        $this->db->insert('files_upload', $data);
        return $this->db->insert_id();
    }

    /**
    * Retorna todos os Posts para a grid do jTable
    *  @param jtSorting - pelo que será ordenado ex: "descricao ASC"
    *  @param jtStartIndex - inicio do limit
    *  @param jtPageSize - fim da limit
    * @return type 
    */
    function listaFiles($parametros,$get){
        $data = array();

        $data['TotalRecordCount'] = $this->getTotalFiles();

        $this->db->trans_start();
            $this->db->select('*',false);
            $this->db->from('files_upload');
            if(!empty($parametros['title'])){
                $this->db->like('upper(titulo)', strtoupper($parametros['title']) );
            }
            
            if(!empty($parametros['filename'])){
                $this->db->like('upper(filename)', strtoupper($parametros['filename']) );
            }
            
            if( !empty($get['jtSorting'])){
                $pieces = explode(" ", @$get['jtSorting']);
                $this->db->order_by($pieces[0],$pieces[1]);
            }

            if( @$get['jtStartIndex'] != ''  && @$get['jtPageSize'] != '' ){
                $this->db->limit($get['jtStartIndex']+','+$get['jtPageSize']);
            }

            $data['Records'] = $this->db->get()->result();
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }

        return $data;
    }
    
    /**
        * Deleta o arquivo segundo seu ID
        * @param type $parametros
        * @return string 
        */
    function deletarFiles($id){
        $file = $this->getFileToDelete($id);
        $this->db->trans_start();
            $this->db->where_in('id', $id);
            $this->db->delete('files_upload');
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $retorno = false;
        }else{
            $retorno = true;
        }
        unlink( ARQUIVO_LOG_URL.$file[0]['filename'] );
        return $retorno;
    }
    
    /**
     *Retorna ARRAY de todos
     * @return type 
     */
    function getTodosFiles(){
        $array= array();
        $this->db->trans_start();
            $this->db->select('id,titulo as descricao');
            $this->db->from('files_upload');
            $array = $this->db->get()->result_array();
        $this->db->trans_complete();
        return $array;
    }
    
    
    function getFile($id){
        $this->db->trans_start();
            $this->db->select('*',false);
            $this->db->from('files_upload');
            $this->db->where('id',$id);
        $this->db->trans_complete();
        $valor = $this->db->get()->result();
        return $valor;
    }
    function getFileToDelete($id){
        $this->db->trans_start();
            $this->db->select('*',false);
            $this->db->from('files_upload');
            $this->db->where('id',$id);
        $this->db->trans_complete();
        $valor = $this->db->get()->result_array();
        return $valor;
    }
}
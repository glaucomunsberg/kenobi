<div class="page-header">
    <h1>Programa Desktop<small> versões para download</small></h1>
</div>
<p style="text-align: justify;text-indent: 40px;margin-bottom: 10px">
    Aqui você encontrará a versão adequada para fazer download, do Programa de Digitação, de acordo com seu sistema operacional. Atualmente a equipe Skywalker está trabalhando para compatibilizar com um número maior de sistemas operacionais.
</p>
<ul class="thumbnails">
    <li class="span3">
        <div class="thumbnail">
            <img src="<?=IMG.'soWindows.png'?>" alt="">
            <div class="caption">
            <h4>Windows</h4>
            <p>
                É necessário ter <strong>Windows XP SP3 ou superior</strong>;
            </p>
            <p>
                Esteja instalado previamente o <strong>Java 7</strong>;
            </p>
            <hr>
            <p style="text-align: justify">
                Para a instalação e execução no seu computador basta ter permissão de administrador.
            </p>
            <p><a href="<?= BASE_URL . 'static/installers/ProDown_1.3.exe' ?>" class="btn btn-primary">Baixar</a></p>  
            </div>
        </div>
    </li>
    <li class="span3">
        <div class="thumbnail">
            <img src="<?= IMG . 'soLinux.png' ?>" alt="">
            <div class="caption">
                <h4>Linux</h4>
                <p>
                    Qualquer distribuição <strong>Linux</strong> e 64 MB de RAM livres;
                </p>
                <p>
                    Esteja instalado previamente o <strong>Java 7</strong> ou <strong>OpenJDK</strong>;
                </p>
                <hr>
                <p style="text-align: justify">
                    Para a execução descompacte a pasta Cody e execute o bash Cody.sh .
                </p>
                <p><a href="<?= BASE_URL . 'static/installers/ProDown_1.3.tar.gz' ?>" class="btn btn-primary">Baixar</a></p>
            </div>
        </div>
    </li>
    <li class="span3">
        <div class="thumbnail">
            <img src="<?= IMG . 'soUbuntu.png' ?>" alt="">
            <div class="caption">
                <h4>Ubuntu</h4>
                <p>
                    Ubuntu igual a <strong>10.04 ou superior</strong> e instalado previamente o <strong>Java 7</strong>;
                </p>
                <p><strong>Em breve!</strong></p>
                <hr>
                <p>A equipe está trabalhando para tornar o Cody instalável através de um pacote deb :)</p>
                <p><a class="btn btn-primary disabled">Baixar</a></p>
            </div>
        </div>
    </li>
    <li class="span3">
        <div class="thumbnail">
            <img src="<?= IMG . 'soOSX.png' ?>" alt="">
            <div class="caption">
                <h4>MAC OS X</h4>
                <p>
                    Para instalar basta ter a versão <strong>MAC OS X 10.6</strong> ou superior e esteja instalado previamente o <strong>Java 6</strong>;
                </p>
                <p>
                     
                </p>
                <hr>
                <p>Para a instalação basta que seja executado o instalador baixado</p>
                <p><a  class="btn btn-primary" href="<?= BASE_URL . 'static/installers/ProDown_1.3.pkg' ?>">Baixar</a></p>
            </div>
        </div>
    </li>   
</ul>
<p class="alert alert-info">
    <strong>Versão Beta</strong> do programa ou informações técnicas podem ser obtidas<a href="<?=BASE_URL.'cody'?>" class="btn btn-link">a aqui</a>.  
</p>

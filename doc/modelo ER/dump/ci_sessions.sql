DROP TABLE IF EXISTS ci_sessions;

CREATE TABLE ci_sessions (
       session_id VARCHAR(40) NOT NULL
     , ip_address VARCHAR(45) NOT NULL
     , user_agent VARCHAR(120) NOT NULL
     , last_activity INT(10) UNSIGNED
     , user_data TEXT
     , PRIMARY KEY (session_id)
);


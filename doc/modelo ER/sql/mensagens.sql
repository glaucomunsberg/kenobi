DROP TABLE IF EXISTS mensagens;

CREATE TABLE mensagens (
       origem_cpf VARCHAR(11) NOT NULL
     , destino_cpf VARCHAR(11) NOT NULL
     , mensagem VARCHAR(300) NOT NULL
     , origem_tipo VARCHAR(30)
     , destino_tipo VARCHAR(30)
     , dt_cadastro CHAR(10) DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (origem_cpf, destino_cpf, mensagem)
     , INDEX (origem_cpf)
     , CONSTRAINT FK_mensagens_1 FOREIGN KEY (origem_cpf)
                  REFERENCES educadores (cpf)
     , INDEX (destino_cpf)
     , CONSTRAINT FK_mensagens_2 FOREIGN KEY (destino_cpf)
                  REFERENCES educadores (cpf)
) ENGINE = MyISAM ;


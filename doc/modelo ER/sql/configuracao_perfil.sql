DROP TABLE IF configuracao_perfil;

CREATE TABLE configuracao_perfil (
       nome_perfil VARCHAR(255) NOT NULL
     , sistema_online BOOLEAN DEFAULT 1
     , linguagem_padrao CHAR(10)
     , email VARCHAR(255)
     , senha VARCHAR(30)
     , email_administrador VARCHAR(255)
     , dt_cadastro TIMESTAMP DEFAULT CURRENT_TIMESTAMP
     , PRIMARY KEY (nome_perfil)
);


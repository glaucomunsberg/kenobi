<?php

class AlunosModel extends CI_Model{
    
    function getAlunos(){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('alunos');
        $this->db->trans_complete();
        
        return $this->db->get()->result();
        
    }
    function converteGenero($genero){
        if($genero == 'M'){
            return 'Masculino';
        }else{
            return 'Feminino';
        }
    }
            
    function getAluno($nome,$nome_mae,$dt_nascimento){
        
        $this->db->trans_start();
            $this->db->select('*',false);
            $this->db->from('alunos');
            $this->db->where('nome',$nome);
            $this->db->where('nome_mae',$nome_mae);
            $this->db->where('dt_nascimento',$dt_nascimento);
        $this->db->trans_complete();
        return $this->db->get()->result_array();
    }
    
    function getAlunoByLinha($nome,$nome_mae,$dt_nascimento){
        
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('alunos');
            $this->db->where('nome',$nome);
            $this->db->where('nome_mae',$nome_mae);
            $this->db->where('dt_nascimento',$dt_nascimento);
            $this->db->limit(1);
        $this->db->trans_complete();
        return $this->db->get()->result();
    }
    
    function inserirAluno($parametros){
        $cpf = $parametros['cpf'];
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace("-", "", $cpf);
        $cpf = str_replace("/", "", $cpf);
        
        $this->db->trans_start();
            $this->db->set('nome', $parametros['nome']);
            $this->db->set('nome_mae', $parametros['nome_mae']);
            $parts = explode('/', $parametros['dt_nascimento']);
            $date  = "$parts[2]-$parts[1]-$parts[0]";
            $this->db->set('dt_nascimento', $date);
            $this->db->set('nome_pai', $parametros['nome_pai']);
            $this->db->set('cpf', $cpf);
            $this->db->set('rg', $parametros['rg']);
            $this->db->set('endereco', $parametros['endereco']);
            $this->db->set('genero', $parametros['genero']);
            $this->db->insert('alunos');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            return true;
        }
    }
    
    function atualizarAluno($parametros){
        
        $this->db->trans_start();
            $this->db->set('nome_pai', $parametros['nome_pai']);
            $this->db->set('cpf', $parametros['cpf']);
            $this->db->set('rg', $parametros['rg']);
            $this->db->set('endereco', $parametros['endereco']);
            $this->db->set('dt_cadastro', $parametros['dt_cadastro']);
            $this->db->where('nome', $parametros['nome']);
            $this->db->where('nome_mae', $parametros['nome_mae']);
            $this->db->where('dt_nascimento', $parametros['dt_nascimento']);
            $this->db->set('genero', $parametros['genero']);
            $this->db->update('alunos');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            return false;
        }else{
            return true;
        }
    }
    
    #retorna se o aluno existe
    function existeAluno($nome,$nome_mae,$dt_nascimento){
        $this->db->trans_start();
            $this->db->select('*');
            $this->db->from('alunos');
            $this->db->where('nome', $nome);
            $this->db->where('nome_mae', $nome_mae);
            $this->db->where('dt_nascimento', $dt_nascimento);
            
        $this->db->trans_complete();
        
        if($this->db->get()->num_rows() > 0 ){
            return true;
        }else{
            return false;
        }
    }
    
    #retorna todos os alunos de acordo com o genero
    function getAlunoByGenero($genero){
    	$this->db->trans_start();
    		$this->db->select('*');
    		$this->db->from('alunos');
    		$this->where('genero', $genero);
    	$this->db->trans_complete();

    	return $this->db->get()->result();
    }
    
    function getAlunosByEducador($cpf){
        $this->db->trans_start();
    		$this->db->select('a.nome, a.nome_mae, a.dt_nascimento,ta.cpf, ta.cnpj',false);
    		$this->db->from('alunos as a');
                $this->db->join('turmaAlunos as ta','ta.nome = a.nome and ta.nome_mae = a.nome_mae and ta.dt_nascimento = a.dt_nascimento');
    		$this->db->where('ta.esta_ativo', 1);
                $this->db->where('ta.cpf', $cpf);
    	$this->db->trans_complete();

    	return $this->db->get()->result();
    }
    
    function getTotalAlunosByEducador($cpf){
        if($cpf != '')
            $this->db->where('cpf',$cpf);
        $retorno =  $this->db->get('alunos');
        return $retorno->num_rows();
    }
    
    
    function listaAlunosByEducador($parametros,$get){
        $data = array();

        $data['TotalRecordCount'] = $this->getTotalAlunosByEducador(@$parametros['cpf']);

        $this->db->trans_start();
            $this->db->select('a.nome, a.nome_mae, a.dt_nascimento,ta.cpf, ta.cnpj,a.genero,a.endereco,a.cpf as cpf_aluno, a.rg as rg_aluno');
            $this->db->from('alunos as a');
            $this->db->join('turmaAlunos as ta','ta.nome = a.nome and ta.nome_mae = a.nome_mae and ta.dt_nascimento = a.dt_nascimento');
            $this->db->order_by('a.nome','ASC');
            if(!empty($parametros['aluno_nome'])){
                $this->db->like('upper(a.nome)', strtoupper($parametros['aluno_nome']) );
            }
            
            if(!empty($parametros['cpf'])){
                $this->db->where('ta.cpf', strtoupper($parametros['cpf']) );
            }

            if( !empty($get['jtSorting'])){
                $pieces = explode(" ", @$get['jtSorting']);
                $this->db->order_by($pieces[0],$pieces[1]);
            }

            if( @$get['jtStartIndex'] != ''  && @$get['jtPageSize'] != '' ){
                $this->db->limit($get['jtStartIndex']+','+$get['jtPageSize']);
            }

            $data['Records'] = $this->db->get()->result();
        $this->db->trans_complete();

        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }

        return $data;
    }
    
    function atualizarAlunoByAdmin($parametros){
        $this->db->trans_start();
            $this->db->set('cpf', $parametros['cpf_aluno']);
            $this->db->set('rg', $parametros['rg_aluno']);
            $this->db->set('genero', $parametros['genero']);
            $this->db->set('endereco', $parametros['endereco']);
            $this->db->where('nome', $parametros['nome']);
            $this->db->where('nome_mae', $parametros['nome_mae']);
            $this->db->where('dt_nascimento', $parametros['dt_nascimento']);
            $this->db->update('alunos');
        $this->db->trans_complete();
        
        if($this->db->trans_status() === FALSE){
            $data['Result'] = "ERROR";
        }else{
            $data['Result'] = "OK";
        }
        return $data;
    }
    
    function excluirAluno($parametros){
            $this->db->trans_start();
                $this->db->where('nome', $parametros['nome']);
                $this->db->where('nome_mae', $parametros['nome_mae']);
                $this->db->where('dt_nascimento', $parametros['dt_nascimento']);
                $this->db->delete('alunos');
            $this->db->trans_complete();

            if($this->db->trans_status() === FALSE){
                $data['Result'] = "ERROR";
            }else{
                $data['Result'] = "OK";
            }
            return $data;
        }
    
}



?>
